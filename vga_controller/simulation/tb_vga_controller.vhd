
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_vga_controller is
end tb_vga_controller;

architecture tb of tb_vga_controller is

    component vga_controller
        port (video_vga_controller_0_avalon_vga_sink_data          : in std_logic_vector (29 downto 0) := (others => '0');
              video_vga_controller_0_avalon_vga_sink_startofpacket : in std_logic;
              video_vga_controller_0_avalon_vga_sink_endofpacket   : in std_logic;
              video_vga_controller_0_avalon_vga_sink_valid         : in std_logic;
              video_vga_controller_0_avalon_vga_sink_ready         : out std_logic;
              video_vga_controller_0_clk_clk                       : in std_logic;
              video_vga_controller_0_external_interface_CLK        : out std_logic;
              video_vga_controller_0_external_interface_HS         : out std_logic;
              video_vga_controller_0_external_interface_VS         : out std_logic;
              video_vga_controller_0_external_interface_BLANK      : out std_logic;
              video_vga_controller_0_external_interface_SYNC       : out std_logic;
              video_vga_controller_0_external_interface_R          : out std_logic_vector (3 downto 0);
              video_vga_controller_0_external_interface_G          : out std_logic_vector (3 downto 0);
              video_vga_controller_0_external_interface_B          : out std_logic_vector (3 downto 0);
              video_vga_controller_0_reset_reset                   : in std_logic);
    end component;

    signal video_vga_controller_0_avalon_vga_sink_data          : std_logic_vector (29 downto 0) := (others => '0');
    signal video_vga_controller_0_avalon_vga_sink_startofpacket : std_logic:= '0';
    signal video_vga_controller_0_avalon_vga_sink_endofpacket   : std_logic:= '0';
    signal video_vga_controller_0_avalon_vga_sink_valid         : std_logic;
    signal video_vga_controller_0_avalon_vga_sink_ready         : std_logic;
    signal video_vga_controller_0_clk_clk                       : std_logic;
    signal video_vga_controller_0_external_interface_CLK        : std_logic;
    signal video_vga_controller_0_external_interface_HS         : std_logic;
    signal video_vga_controller_0_external_interface_VS         : std_logic;
    signal video_vga_controller_0_external_interface_BLANK      : std_logic;
    signal video_vga_controller_0_external_interface_SYNC       : std_logic;
    signal video_vga_controller_0_external_interface_R          : std_logic_vector (3 downto 0);
    signal video_vga_controller_0_external_interface_G          : std_logic_vector (3 downto 0);
    signal video_vga_controller_0_external_interface_B          : std_logic_vector (3 downto 0);
    signal video_vga_controller_0_reset_reset                   : std_logic;
    signal cnt : unsigned(29 downto 0) := (others => '0');        

    constant TbPeriod : time := 9.26 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : vga_controller
    port map (video_vga_controller_0_avalon_vga_sink_data          => video_vga_controller_0_avalon_vga_sink_data,
              video_vga_controller_0_avalon_vga_sink_startofpacket => video_vga_controller_0_avalon_vga_sink_startofpacket,
              video_vga_controller_0_avalon_vga_sink_endofpacket   => video_vga_controller_0_avalon_vga_sink_endofpacket,
              video_vga_controller_0_avalon_vga_sink_valid         => video_vga_controller_0_avalon_vga_sink_valid,
              video_vga_controller_0_avalon_vga_sink_ready         => video_vga_controller_0_avalon_vga_sink_ready,
              video_vga_controller_0_clk_clk                       => video_vga_controller_0_clk_clk,
              video_vga_controller_0_external_interface_CLK        => video_vga_controller_0_external_interface_CLK,
              video_vga_controller_0_external_interface_HS         => video_vga_controller_0_external_interface_HS,
              video_vga_controller_0_external_interface_VS         => video_vga_controller_0_external_interface_VS,
              video_vga_controller_0_external_interface_BLANK      => video_vga_controller_0_external_interface_BLANK,
              video_vga_controller_0_external_interface_SYNC       => video_vga_controller_0_external_interface_SYNC,
              video_vga_controller_0_external_interface_R          => video_vga_controller_0_external_interface_R,
              video_vga_controller_0_external_interface_G          => video_vga_controller_0_external_interface_G,
              video_vga_controller_0_external_interface_B          => video_vga_controller_0_external_interface_B,
              video_vga_controller_0_reset_reset                   => video_vga_controller_0_reset_reset);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    video_vga_controller_0_clk_clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        video_vga_controller_0_avalon_vga_sink_data <= (others => '0');
        video_vga_controller_0_avalon_vga_sink_endofpacket <= '0';
        video_vga_controller_0_avalon_vga_sink_valid <= '0';

        -- Reset generation
        -- EDIT: Check that video_vga_controller_0_reset_reset is really your reset signal
        video_vga_controller_0_reset_reset <= '1';
        wait for 100*TbPeriod;
        video_vga_controller_0_reset_reset <= '0';
        wait for 100*TbPeriod;
        video_vga_controller_0_avalon_vga_sink_startofpacket <= '1';
        video_vga_controller_0_avalon_vga_sink_valid <= '1';
        wait for 1*TbPeriod;
        video_vga_controller_0_avalon_vga_sink_startofpacket <= '0';
        for a in 0 to 2000 loop
          video_vga_controller_0_avalon_vga_sink_data <= std_logic_vector(cnt);
          cnt <= cnt + 1;
          wait for 1*TbPeriod;
        end loop;
        -- EDIT Add stimuli here
        wait for 10000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_vga_controller of tb_vga_controller is
    for tb
    end for;
end cfg_tb_vga_controller;