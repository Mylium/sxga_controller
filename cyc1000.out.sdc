## Generated SDC file "cyc1000.out.sdc"

## Copyright (C) 2020  Intel Corporation. All rights reserved.
## Your use of Intel Corporation's design tools, logic functions 
## and other software and tools, and any partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Intel Program License 
## Subscription Agreement, the Intel Quartus Prime License Agreement,
## the Intel FPGA IP License Agreement, or other applicable license
## agreement, including, without limitation, that your use is for
## the sole purpose of programming logic devices manufactured by
## Intel and sold by Intel or its authorized distributors.  Please
## refer to the applicable agreement for further details, at
## https://fpgasoftware.intel.com/eula.


## VENDOR  "Altera"
## PROGRAM "Quartus Prime"
## VERSION "Version 20.1.0 Build 711 06/05/2020 SJ Lite Edition"

## DATE    "Sat Oct 24 21:16:53 2020"

##
## DEVICE  "10CL025YU256C8G"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {clk12} -period 83.333 -waveform { 0.000 41.666 } [get_ports {clk12}]


#**************************************************************
# Create Generated Clock
#**************************************************************

create_generated_clock -name {i_pll|altpll_component|auto_generated|pll1|clk[0]} -source [get_pins {i_pll|altpll_component|auto_generated|pll1|inclk[0]}] -duty_cycle 50/1 -multiply_by 9 -master_clock {clk12} [get_pins {i_pll|altpll_component|auto_generated|pll1|clk[0]}] 


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -rise_from [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -rise_from [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}] -rise_to [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.020  
set_clock_uncertainty -fall_from [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}] -fall_to [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.020  


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {B[0]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {B[1]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {B[2]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {G[0]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {G[1]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {G[2]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {HS}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {R[0]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {R[1]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {R[2]}]
set_output_delay -add_delay  -clock [get_clocks {i_pll|altpll_component|auto_generated|pll1|clk[0]}]  0.800 [get_ports {VS}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

