// Copyright (C) 2020  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and any partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details, at
// https://fpgasoftware.intel.com/eula.

// VENDOR "Altera"
// PROGRAM "Quartus Prime"
// VERSION "Version 20.1.0 Build 711 06/05/2020 SJ Lite Edition"

// DATE "10/25/2020 21:02:09"

// 
// Device: Altera 10CL025YU256C8G Package UFBGA256
// 

// 
// This greybox netlist file is for third party Synthesis Tools
// for timing and resource estimation only.
// 


module sdram_controller (
	clk_clk,
	new_sdram_controller_0_s1_address,
	new_sdram_controller_0_s1_byteenable_n,
	new_sdram_controller_0_s1_chipselect,
	new_sdram_controller_0_s1_writedata,
	new_sdram_controller_0_s1_read_n,
	new_sdram_controller_0_s1_write_n,
	new_sdram_controller_0_s1_readdata,
	new_sdram_controller_0_s1_readdatavalid,
	new_sdram_controller_0_s1_waitrequest,
	new_sdram_controller_0_wire_addr,
	new_sdram_controller_0_wire_ba,
	new_sdram_controller_0_wire_cas_n,
	new_sdram_controller_0_wire_cke,
	new_sdram_controller_0_wire_cs_n,
	new_sdram_controller_0_wire_dq,
	new_sdram_controller_0_wire_dqm,
	new_sdram_controller_0_wire_ras_n,
	new_sdram_controller_0_wire_we_n,
	reset_reset_n)/* synthesis synthesis_greybox=0 */;
input 	clk_clk;
input 	[21:0] new_sdram_controller_0_s1_address;
input 	[1:0] new_sdram_controller_0_s1_byteenable_n;
input 	new_sdram_controller_0_s1_chipselect;
input 	[15:0] new_sdram_controller_0_s1_writedata;
input 	new_sdram_controller_0_s1_read_n;
input 	new_sdram_controller_0_s1_write_n;
output 	[15:0] new_sdram_controller_0_s1_readdata;
output 	new_sdram_controller_0_s1_readdatavalid;
output 	new_sdram_controller_0_s1_waitrequest;
output 	[11:0] new_sdram_controller_0_wire_addr;
output 	[1:0] new_sdram_controller_0_wire_ba;
output 	new_sdram_controller_0_wire_cas_n;
output 	new_sdram_controller_0_wire_cke;
output 	new_sdram_controller_0_wire_cs_n;
inout 	[15:0] new_sdram_controller_0_wire_dq;
output 	[1:0] new_sdram_controller_0_wire_dqm;
output 	new_sdram_controller_0_wire_ras_n;
output 	new_sdram_controller_0_wire_we_n;
input 	reset_reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \new_sdram_controller_0|m_addr[0]~q ;
wire \new_sdram_controller_0|m_addr[1]~q ;
wire \new_sdram_controller_0|m_addr[2]~q ;
wire \new_sdram_controller_0|m_addr[3]~q ;
wire \new_sdram_controller_0|m_addr[4]~q ;
wire \new_sdram_controller_0|m_addr[5]~q ;
wire \new_sdram_controller_0|m_addr[6]~q ;
wire \new_sdram_controller_0|m_addr[7]~q ;
wire \new_sdram_controller_0|oe~q ;
wire \new_sdram_controller_0|za_data[0]~q ;
wire \new_sdram_controller_0|za_data[1]~q ;
wire \new_sdram_controller_0|za_data[2]~q ;
wire \new_sdram_controller_0|za_data[3]~q ;
wire \new_sdram_controller_0|za_data[4]~q ;
wire \new_sdram_controller_0|za_data[5]~q ;
wire \new_sdram_controller_0|za_data[6]~q ;
wire \new_sdram_controller_0|za_data[7]~q ;
wire \new_sdram_controller_0|za_data[8]~q ;
wire \new_sdram_controller_0|za_data[9]~q ;
wire \new_sdram_controller_0|za_data[10]~q ;
wire \new_sdram_controller_0|za_data[11]~q ;
wire \new_sdram_controller_0|za_data[12]~q ;
wire \new_sdram_controller_0|za_data[13]~q ;
wire \new_sdram_controller_0|za_data[14]~q ;
wire \new_sdram_controller_0|za_data[15]~q ;
wire \new_sdram_controller_0|za_valid~q ;
wire \new_sdram_controller_0|the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal0~0_combout ;
wire \new_sdram_controller_0|m_addr[8]~q ;
wire \new_sdram_controller_0|m_addr[9]~q ;
wire \new_sdram_controller_0|m_addr[10]~q ;
wire \new_sdram_controller_0|m_addr[11]~q ;
wire \new_sdram_controller_0|m_bank[0]~q ;
wire \new_sdram_controller_0|m_bank[1]~q ;
wire \new_sdram_controller_0|m_cmd[1]~q ;
wire \new_sdram_controller_0|m_cmd[3]~q ;
wire \new_sdram_controller_0|m_dqm[0]~q ;
wire \new_sdram_controller_0|m_dqm[1]~q ;
wire \new_sdram_controller_0|m_cmd[2]~q ;
wire \new_sdram_controller_0|m_cmd[0]~q ;
wire \rst_controller|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out~q ;
wire \new_sdram_controller_0|m_data[0]~q ;
wire \new_sdram_controller_0|m_data[1]~q ;
wire \new_sdram_controller_0|m_data[2]~q ;
wire \new_sdram_controller_0|m_data[3]~q ;
wire \new_sdram_controller_0|m_data[4]~q ;
wire \new_sdram_controller_0|m_data[5]~q ;
wire \new_sdram_controller_0|m_data[6]~q ;
wire \new_sdram_controller_0|m_data[7]~q ;
wire \new_sdram_controller_0|m_data[8]~q ;
wire \new_sdram_controller_0|m_data[9]~q ;
wire \new_sdram_controller_0|m_data[10]~q ;
wire \new_sdram_controller_0|m_data[11]~q ;
wire \new_sdram_controller_0|m_data[12]~q ;
wire \new_sdram_controller_0|m_data[13]~q ;
wire \new_sdram_controller_0|m_data[14]~q ;
wire \new_sdram_controller_0|m_data[15]~q ;
wire \new_sdram_controller_0_s1_chipselect~input_o ;
wire \new_sdram_controller_0_wire_dq[0]~input_o ;
wire \new_sdram_controller_0_wire_dq[1]~input_o ;
wire \new_sdram_controller_0_wire_dq[2]~input_o ;
wire \new_sdram_controller_0_wire_dq[3]~input_o ;
wire \new_sdram_controller_0_wire_dq[4]~input_o ;
wire \new_sdram_controller_0_wire_dq[5]~input_o ;
wire \new_sdram_controller_0_wire_dq[6]~input_o ;
wire \new_sdram_controller_0_wire_dq[7]~input_o ;
wire \new_sdram_controller_0_wire_dq[8]~input_o ;
wire \new_sdram_controller_0_wire_dq[9]~input_o ;
wire \new_sdram_controller_0_wire_dq[10]~input_o ;
wire \new_sdram_controller_0_wire_dq[11]~input_o ;
wire \new_sdram_controller_0_wire_dq[12]~input_o ;
wire \new_sdram_controller_0_wire_dq[13]~input_o ;
wire \new_sdram_controller_0_wire_dq[14]~input_o ;
wire \new_sdram_controller_0_wire_dq[15]~input_o ;
wire \clk_clk~input_o ;
wire \new_sdram_controller_0_s1_read_n~input_o ;
wire \new_sdram_controller_0_s1_write_n~input_o ;
wire \reset_reset_n~input_o ;
wire \new_sdram_controller_0_s1_address[8]~input_o ;
wire \new_sdram_controller_0_s1_address[9]~input_o ;
wire \new_sdram_controller_0_s1_address[21]~input_o ;
wire \new_sdram_controller_0_s1_address[11]~input_o ;
wire \new_sdram_controller_0_s1_address[10]~input_o ;
wire \new_sdram_controller_0_s1_address[13]~input_o ;
wire \new_sdram_controller_0_s1_address[12]~input_o ;
wire \new_sdram_controller_0_s1_address[15]~input_o ;
wire \new_sdram_controller_0_s1_address[14]~input_o ;
wire \new_sdram_controller_0_s1_address[17]~input_o ;
wire \new_sdram_controller_0_s1_address[16]~input_o ;
wire \new_sdram_controller_0_s1_address[19]~input_o ;
wire \new_sdram_controller_0_s1_address[18]~input_o ;
wire \new_sdram_controller_0_s1_address[20]~input_o ;
wire \new_sdram_controller_0_s1_address[0]~input_o ;
wire \new_sdram_controller_0_s1_address[1]~input_o ;
wire \new_sdram_controller_0_s1_address[2]~input_o ;
wire \new_sdram_controller_0_s1_address[3]~input_o ;
wire \new_sdram_controller_0_s1_address[4]~input_o ;
wire \new_sdram_controller_0_s1_address[5]~input_o ;
wire \new_sdram_controller_0_s1_address[6]~input_o ;
wire \new_sdram_controller_0_s1_address[7]~input_o ;
wire \new_sdram_controller_0_s1_byteenable_n[0]~input_o ;
wire \new_sdram_controller_0_s1_byteenable_n[1]~input_o ;
wire \new_sdram_controller_0_s1_writedata[0]~input_o ;
wire \new_sdram_controller_0_s1_writedata[1]~input_o ;
wire \new_sdram_controller_0_s1_writedata[2]~input_o ;
wire \new_sdram_controller_0_s1_writedata[3]~input_o ;
wire \new_sdram_controller_0_s1_writedata[4]~input_o ;
wire \new_sdram_controller_0_s1_writedata[5]~input_o ;
wire \new_sdram_controller_0_s1_writedata[6]~input_o ;
wire \new_sdram_controller_0_s1_writedata[7]~input_o ;
wire \new_sdram_controller_0_s1_writedata[8]~input_o ;
wire \new_sdram_controller_0_s1_writedata[9]~input_o ;
wire \new_sdram_controller_0_s1_writedata[10]~input_o ;
wire \new_sdram_controller_0_s1_writedata[11]~input_o ;
wire \new_sdram_controller_0_s1_writedata[12]~input_o ;
wire \new_sdram_controller_0_s1_writedata[13]~input_o ;
wire \new_sdram_controller_0_s1_writedata[14]~input_o ;
wire \new_sdram_controller_0_s1_writedata[15]~input_o ;


sdram_controller_sdram_controller_new_sdram_controller_0 new_sdram_controller_0(
	.m_addr_0(\new_sdram_controller_0|m_addr[0]~q ),
	.m_addr_1(\new_sdram_controller_0|m_addr[1]~q ),
	.m_addr_2(\new_sdram_controller_0|m_addr[2]~q ),
	.m_addr_3(\new_sdram_controller_0|m_addr[3]~q ),
	.m_addr_4(\new_sdram_controller_0|m_addr[4]~q ),
	.m_addr_5(\new_sdram_controller_0|m_addr[5]~q ),
	.m_addr_6(\new_sdram_controller_0|m_addr[6]~q ),
	.m_addr_7(\new_sdram_controller_0|m_addr[7]~q ),
	.oe1(\new_sdram_controller_0|oe~q ),
	.za_data_0(\new_sdram_controller_0|za_data[0]~q ),
	.za_data_1(\new_sdram_controller_0|za_data[1]~q ),
	.za_data_2(\new_sdram_controller_0|za_data[2]~q ),
	.za_data_3(\new_sdram_controller_0|za_data[3]~q ),
	.za_data_4(\new_sdram_controller_0|za_data[4]~q ),
	.za_data_5(\new_sdram_controller_0|za_data[5]~q ),
	.za_data_6(\new_sdram_controller_0|za_data[6]~q ),
	.za_data_7(\new_sdram_controller_0|za_data[7]~q ),
	.za_data_8(\new_sdram_controller_0|za_data[8]~q ),
	.za_data_9(\new_sdram_controller_0|za_data[9]~q ),
	.za_data_10(\new_sdram_controller_0|za_data[10]~q ),
	.za_data_11(\new_sdram_controller_0|za_data[11]~q ),
	.za_data_12(\new_sdram_controller_0|za_data[12]~q ),
	.za_data_13(\new_sdram_controller_0|za_data[13]~q ),
	.za_data_14(\new_sdram_controller_0|za_data[14]~q ),
	.za_data_15(\new_sdram_controller_0|za_data[15]~q ),
	.za_valid1(\new_sdram_controller_0|za_valid~q ),
	.Equal0(\new_sdram_controller_0|the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal0~0_combout ),
	.m_addr_8(\new_sdram_controller_0|m_addr[8]~q ),
	.m_addr_9(\new_sdram_controller_0|m_addr[9]~q ),
	.m_addr_10(\new_sdram_controller_0|m_addr[10]~q ),
	.m_addr_11(\new_sdram_controller_0|m_addr[11]~q ),
	.m_bank_0(\new_sdram_controller_0|m_bank[0]~q ),
	.m_bank_1(\new_sdram_controller_0|m_bank[1]~q ),
	.m_cmd_1(\new_sdram_controller_0|m_cmd[1]~q ),
	.m_cmd_3(\new_sdram_controller_0|m_cmd[3]~q ),
	.m_dqm_0(\new_sdram_controller_0|m_dqm[0]~q ),
	.m_dqm_1(\new_sdram_controller_0|m_dqm[1]~q ),
	.m_cmd_2(\new_sdram_controller_0|m_cmd[2]~q ),
	.m_cmd_0(\new_sdram_controller_0|m_cmd[0]~q ),
	.altera_reset_synchronizer_int_chain_out(\rst_controller|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out~q ),
	.m_data_0(\new_sdram_controller_0|m_data[0]~q ),
	.m_data_1(\new_sdram_controller_0|m_data[1]~q ),
	.m_data_2(\new_sdram_controller_0|m_data[2]~q ),
	.m_data_3(\new_sdram_controller_0|m_data[3]~q ),
	.m_data_4(\new_sdram_controller_0|m_data[4]~q ),
	.m_data_5(\new_sdram_controller_0|m_data[5]~q ),
	.m_data_6(\new_sdram_controller_0|m_data[6]~q ),
	.m_data_7(\new_sdram_controller_0|m_data[7]~q ),
	.m_data_8(\new_sdram_controller_0|m_data[8]~q ),
	.m_data_9(\new_sdram_controller_0|m_data[9]~q ),
	.m_data_10(\new_sdram_controller_0|m_data[10]~q ),
	.m_data_11(\new_sdram_controller_0|m_data[11]~q ),
	.m_data_12(\new_sdram_controller_0|m_data[12]~q ),
	.m_data_13(\new_sdram_controller_0|m_data[13]~q ),
	.m_data_14(\new_sdram_controller_0|m_data[14]~q ),
	.m_data_15(\new_sdram_controller_0|m_data[15]~q ),
	.new_sdram_controller_0_wire_dq_0(\new_sdram_controller_0_wire_dq[0]~input_o ),
	.new_sdram_controller_0_wire_dq_1(\new_sdram_controller_0_wire_dq[1]~input_o ),
	.new_sdram_controller_0_wire_dq_2(\new_sdram_controller_0_wire_dq[2]~input_o ),
	.new_sdram_controller_0_wire_dq_3(\new_sdram_controller_0_wire_dq[3]~input_o ),
	.new_sdram_controller_0_wire_dq_4(\new_sdram_controller_0_wire_dq[4]~input_o ),
	.new_sdram_controller_0_wire_dq_5(\new_sdram_controller_0_wire_dq[5]~input_o ),
	.new_sdram_controller_0_wire_dq_6(\new_sdram_controller_0_wire_dq[6]~input_o ),
	.new_sdram_controller_0_wire_dq_7(\new_sdram_controller_0_wire_dq[7]~input_o ),
	.new_sdram_controller_0_wire_dq_8(\new_sdram_controller_0_wire_dq[8]~input_o ),
	.new_sdram_controller_0_wire_dq_9(\new_sdram_controller_0_wire_dq[9]~input_o ),
	.new_sdram_controller_0_wire_dq_10(\new_sdram_controller_0_wire_dq[10]~input_o ),
	.new_sdram_controller_0_wire_dq_11(\new_sdram_controller_0_wire_dq[11]~input_o ),
	.new_sdram_controller_0_wire_dq_12(\new_sdram_controller_0_wire_dq[12]~input_o ),
	.new_sdram_controller_0_wire_dq_13(\new_sdram_controller_0_wire_dq[13]~input_o ),
	.new_sdram_controller_0_wire_dq_14(\new_sdram_controller_0_wire_dq[14]~input_o ),
	.new_sdram_controller_0_wire_dq_15(\new_sdram_controller_0_wire_dq[15]~input_o ),
	.clk_clk(\clk_clk~input_o ),
	.new_sdram_controller_0_s1_read_n(\new_sdram_controller_0_s1_read_n~input_o ),
	.new_sdram_controller_0_s1_write_n(\new_sdram_controller_0_s1_write_n~input_o ),
	.new_sdram_controller_0_s1_address_8(\new_sdram_controller_0_s1_address[8]~input_o ),
	.new_sdram_controller_0_s1_address_9(\new_sdram_controller_0_s1_address[9]~input_o ),
	.new_sdram_controller_0_s1_address_21(\new_sdram_controller_0_s1_address[21]~input_o ),
	.new_sdram_controller_0_s1_address_11(\new_sdram_controller_0_s1_address[11]~input_o ),
	.new_sdram_controller_0_s1_address_10(\new_sdram_controller_0_s1_address[10]~input_o ),
	.new_sdram_controller_0_s1_address_13(\new_sdram_controller_0_s1_address[13]~input_o ),
	.new_sdram_controller_0_s1_address_12(\new_sdram_controller_0_s1_address[12]~input_o ),
	.new_sdram_controller_0_s1_address_15(\new_sdram_controller_0_s1_address[15]~input_o ),
	.new_sdram_controller_0_s1_address_14(\new_sdram_controller_0_s1_address[14]~input_o ),
	.new_sdram_controller_0_s1_address_17(\new_sdram_controller_0_s1_address[17]~input_o ),
	.new_sdram_controller_0_s1_address_16(\new_sdram_controller_0_s1_address[16]~input_o ),
	.new_sdram_controller_0_s1_address_19(\new_sdram_controller_0_s1_address[19]~input_o ),
	.new_sdram_controller_0_s1_address_18(\new_sdram_controller_0_s1_address[18]~input_o ),
	.new_sdram_controller_0_s1_address_20(\new_sdram_controller_0_s1_address[20]~input_o ),
	.new_sdram_controller_0_s1_address_0(\new_sdram_controller_0_s1_address[0]~input_o ),
	.new_sdram_controller_0_s1_address_1(\new_sdram_controller_0_s1_address[1]~input_o ),
	.new_sdram_controller_0_s1_address_2(\new_sdram_controller_0_s1_address[2]~input_o ),
	.new_sdram_controller_0_s1_address_3(\new_sdram_controller_0_s1_address[3]~input_o ),
	.new_sdram_controller_0_s1_address_4(\new_sdram_controller_0_s1_address[4]~input_o ),
	.new_sdram_controller_0_s1_address_5(\new_sdram_controller_0_s1_address[5]~input_o ),
	.new_sdram_controller_0_s1_address_6(\new_sdram_controller_0_s1_address[6]~input_o ),
	.new_sdram_controller_0_s1_address_7(\new_sdram_controller_0_s1_address[7]~input_o ),
	.new_sdram_controller_0_s1_byteenable_n_0(\new_sdram_controller_0_s1_byteenable_n[0]~input_o ),
	.new_sdram_controller_0_s1_byteenable_n_1(\new_sdram_controller_0_s1_byteenable_n[1]~input_o ),
	.new_sdram_controller_0_s1_writedata_0(\new_sdram_controller_0_s1_writedata[0]~input_o ),
	.new_sdram_controller_0_s1_writedata_1(\new_sdram_controller_0_s1_writedata[1]~input_o ),
	.new_sdram_controller_0_s1_writedata_2(\new_sdram_controller_0_s1_writedata[2]~input_o ),
	.new_sdram_controller_0_s1_writedata_3(\new_sdram_controller_0_s1_writedata[3]~input_o ),
	.new_sdram_controller_0_s1_writedata_4(\new_sdram_controller_0_s1_writedata[4]~input_o ),
	.new_sdram_controller_0_s1_writedata_5(\new_sdram_controller_0_s1_writedata[5]~input_o ),
	.new_sdram_controller_0_s1_writedata_6(\new_sdram_controller_0_s1_writedata[6]~input_o ),
	.new_sdram_controller_0_s1_writedata_7(\new_sdram_controller_0_s1_writedata[7]~input_o ),
	.new_sdram_controller_0_s1_writedata_8(\new_sdram_controller_0_s1_writedata[8]~input_o ),
	.new_sdram_controller_0_s1_writedata_9(\new_sdram_controller_0_s1_writedata[9]~input_o ),
	.new_sdram_controller_0_s1_writedata_10(\new_sdram_controller_0_s1_writedata[10]~input_o ),
	.new_sdram_controller_0_s1_writedata_11(\new_sdram_controller_0_s1_writedata[11]~input_o ),
	.new_sdram_controller_0_s1_writedata_12(\new_sdram_controller_0_s1_writedata[12]~input_o ),
	.new_sdram_controller_0_s1_writedata_13(\new_sdram_controller_0_s1_writedata[13]~input_o ),
	.new_sdram_controller_0_s1_writedata_14(\new_sdram_controller_0_s1_writedata[14]~input_o ),
	.new_sdram_controller_0_s1_writedata_15(\new_sdram_controller_0_s1_writedata[15]~input_o ));

sdram_controller_altera_reset_controller rst_controller(
	.altera_reset_synchronizer_int_chain_out(\rst_controller|alt_rst_sync_uq1|altera_reset_synchronizer_int_chain_out~q ),
	.clk_clk(\clk_clk~input_o ),
	.reset_reset_n(\reset_reset_n~input_o ));

assign \new_sdram_controller_0_wire_dq[0]~input_o  = new_sdram_controller_0_wire_dq[0];

assign \new_sdram_controller_0_wire_dq[1]~input_o  = new_sdram_controller_0_wire_dq[1];

assign \new_sdram_controller_0_wire_dq[2]~input_o  = new_sdram_controller_0_wire_dq[2];

assign \new_sdram_controller_0_wire_dq[3]~input_o  = new_sdram_controller_0_wire_dq[3];

assign \new_sdram_controller_0_wire_dq[4]~input_o  = new_sdram_controller_0_wire_dq[4];

assign \new_sdram_controller_0_wire_dq[5]~input_o  = new_sdram_controller_0_wire_dq[5];

assign \new_sdram_controller_0_wire_dq[6]~input_o  = new_sdram_controller_0_wire_dq[6];

assign \new_sdram_controller_0_wire_dq[7]~input_o  = new_sdram_controller_0_wire_dq[7];

assign \new_sdram_controller_0_wire_dq[8]~input_o  = new_sdram_controller_0_wire_dq[8];

assign \new_sdram_controller_0_wire_dq[9]~input_o  = new_sdram_controller_0_wire_dq[9];

assign \new_sdram_controller_0_wire_dq[10]~input_o  = new_sdram_controller_0_wire_dq[10];

assign \new_sdram_controller_0_wire_dq[11]~input_o  = new_sdram_controller_0_wire_dq[11];

assign \new_sdram_controller_0_wire_dq[12]~input_o  = new_sdram_controller_0_wire_dq[12];

assign \new_sdram_controller_0_wire_dq[13]~input_o  = new_sdram_controller_0_wire_dq[13];

assign \new_sdram_controller_0_wire_dq[14]~input_o  = new_sdram_controller_0_wire_dq[14];

assign \new_sdram_controller_0_wire_dq[15]~input_o  = new_sdram_controller_0_wire_dq[15];

assign \clk_clk~input_o  = clk_clk;

assign \new_sdram_controller_0_s1_read_n~input_o  = new_sdram_controller_0_s1_read_n;

assign \new_sdram_controller_0_s1_write_n~input_o  = new_sdram_controller_0_s1_write_n;

assign \reset_reset_n~input_o  = reset_reset_n;

assign \new_sdram_controller_0_s1_address[8]~input_o  = new_sdram_controller_0_s1_address[8];

assign \new_sdram_controller_0_s1_address[9]~input_o  = new_sdram_controller_0_s1_address[9];

assign \new_sdram_controller_0_s1_address[21]~input_o  = new_sdram_controller_0_s1_address[21];

assign \new_sdram_controller_0_s1_address[11]~input_o  = new_sdram_controller_0_s1_address[11];

assign \new_sdram_controller_0_s1_address[10]~input_o  = new_sdram_controller_0_s1_address[10];

assign \new_sdram_controller_0_s1_address[13]~input_o  = new_sdram_controller_0_s1_address[13];

assign \new_sdram_controller_0_s1_address[12]~input_o  = new_sdram_controller_0_s1_address[12];

assign \new_sdram_controller_0_s1_address[15]~input_o  = new_sdram_controller_0_s1_address[15];

assign \new_sdram_controller_0_s1_address[14]~input_o  = new_sdram_controller_0_s1_address[14];

assign \new_sdram_controller_0_s1_address[17]~input_o  = new_sdram_controller_0_s1_address[17];

assign \new_sdram_controller_0_s1_address[16]~input_o  = new_sdram_controller_0_s1_address[16];

assign \new_sdram_controller_0_s1_address[19]~input_o  = new_sdram_controller_0_s1_address[19];

assign \new_sdram_controller_0_s1_address[18]~input_o  = new_sdram_controller_0_s1_address[18];

assign \new_sdram_controller_0_s1_address[20]~input_o  = new_sdram_controller_0_s1_address[20];

assign \new_sdram_controller_0_s1_address[0]~input_o  = new_sdram_controller_0_s1_address[0];

assign \new_sdram_controller_0_s1_address[1]~input_o  = new_sdram_controller_0_s1_address[1];

assign \new_sdram_controller_0_s1_address[2]~input_o  = new_sdram_controller_0_s1_address[2];

assign \new_sdram_controller_0_s1_address[3]~input_o  = new_sdram_controller_0_s1_address[3];

assign \new_sdram_controller_0_s1_address[4]~input_o  = new_sdram_controller_0_s1_address[4];

assign \new_sdram_controller_0_s1_address[5]~input_o  = new_sdram_controller_0_s1_address[5];

assign \new_sdram_controller_0_s1_address[6]~input_o  = new_sdram_controller_0_s1_address[6];

assign \new_sdram_controller_0_s1_address[7]~input_o  = new_sdram_controller_0_s1_address[7];

assign \new_sdram_controller_0_s1_byteenable_n[0]~input_o  = new_sdram_controller_0_s1_byteenable_n[0];

assign \new_sdram_controller_0_s1_byteenable_n[1]~input_o  = new_sdram_controller_0_s1_byteenable_n[1];

assign \new_sdram_controller_0_s1_writedata[0]~input_o  = new_sdram_controller_0_s1_writedata[0];

assign \new_sdram_controller_0_s1_writedata[1]~input_o  = new_sdram_controller_0_s1_writedata[1];

assign \new_sdram_controller_0_s1_writedata[2]~input_o  = new_sdram_controller_0_s1_writedata[2];

assign \new_sdram_controller_0_s1_writedata[3]~input_o  = new_sdram_controller_0_s1_writedata[3];

assign \new_sdram_controller_0_s1_writedata[4]~input_o  = new_sdram_controller_0_s1_writedata[4];

assign \new_sdram_controller_0_s1_writedata[5]~input_o  = new_sdram_controller_0_s1_writedata[5];

assign \new_sdram_controller_0_s1_writedata[6]~input_o  = new_sdram_controller_0_s1_writedata[6];

assign \new_sdram_controller_0_s1_writedata[7]~input_o  = new_sdram_controller_0_s1_writedata[7];

assign \new_sdram_controller_0_s1_writedata[8]~input_o  = new_sdram_controller_0_s1_writedata[8];

assign \new_sdram_controller_0_s1_writedata[9]~input_o  = new_sdram_controller_0_s1_writedata[9];

assign \new_sdram_controller_0_s1_writedata[10]~input_o  = new_sdram_controller_0_s1_writedata[10];

assign \new_sdram_controller_0_s1_writedata[11]~input_o  = new_sdram_controller_0_s1_writedata[11];

assign \new_sdram_controller_0_s1_writedata[12]~input_o  = new_sdram_controller_0_s1_writedata[12];

assign \new_sdram_controller_0_s1_writedata[13]~input_o  = new_sdram_controller_0_s1_writedata[13];

assign \new_sdram_controller_0_s1_writedata[14]~input_o  = new_sdram_controller_0_s1_writedata[14];

assign \new_sdram_controller_0_s1_writedata[15]~input_o  = new_sdram_controller_0_s1_writedata[15];

assign new_sdram_controller_0_s1_readdata[0] = \new_sdram_controller_0|za_data[0]~q ;

assign new_sdram_controller_0_s1_readdata[1] = \new_sdram_controller_0|za_data[1]~q ;

assign new_sdram_controller_0_s1_readdata[2] = \new_sdram_controller_0|za_data[2]~q ;

assign new_sdram_controller_0_s1_readdata[3] = \new_sdram_controller_0|za_data[3]~q ;

assign new_sdram_controller_0_s1_readdata[4] = \new_sdram_controller_0|za_data[4]~q ;

assign new_sdram_controller_0_s1_readdata[5] = \new_sdram_controller_0|za_data[5]~q ;

assign new_sdram_controller_0_s1_readdata[6] = \new_sdram_controller_0|za_data[6]~q ;

assign new_sdram_controller_0_s1_readdata[7] = \new_sdram_controller_0|za_data[7]~q ;

assign new_sdram_controller_0_s1_readdata[8] = \new_sdram_controller_0|za_data[8]~q ;

assign new_sdram_controller_0_s1_readdata[9] = \new_sdram_controller_0|za_data[9]~q ;

assign new_sdram_controller_0_s1_readdata[10] = \new_sdram_controller_0|za_data[10]~q ;

assign new_sdram_controller_0_s1_readdata[11] = \new_sdram_controller_0|za_data[11]~q ;

assign new_sdram_controller_0_s1_readdata[12] = \new_sdram_controller_0|za_data[12]~q ;

assign new_sdram_controller_0_s1_readdata[13] = \new_sdram_controller_0|za_data[13]~q ;

assign new_sdram_controller_0_s1_readdata[14] = \new_sdram_controller_0|za_data[14]~q ;

assign new_sdram_controller_0_s1_readdata[15] = \new_sdram_controller_0|za_data[15]~q ;

assign new_sdram_controller_0_s1_readdatavalid = \new_sdram_controller_0|za_valid~q ;

assign new_sdram_controller_0_s1_waitrequest = \new_sdram_controller_0|the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal0~0_combout ;

assign new_sdram_controller_0_wire_addr[0] = \new_sdram_controller_0|m_addr[0]~q ;

assign new_sdram_controller_0_wire_addr[1] = \new_sdram_controller_0|m_addr[1]~q ;

assign new_sdram_controller_0_wire_addr[2] = \new_sdram_controller_0|m_addr[2]~q ;

assign new_sdram_controller_0_wire_addr[3] = \new_sdram_controller_0|m_addr[3]~q ;

assign new_sdram_controller_0_wire_addr[4] = \new_sdram_controller_0|m_addr[4]~q ;

assign new_sdram_controller_0_wire_addr[5] = \new_sdram_controller_0|m_addr[5]~q ;

assign new_sdram_controller_0_wire_addr[6] = \new_sdram_controller_0|m_addr[6]~q ;

assign new_sdram_controller_0_wire_addr[7] = \new_sdram_controller_0|m_addr[7]~q ;

assign new_sdram_controller_0_wire_addr[8] = \new_sdram_controller_0|m_addr[8]~q ;

assign new_sdram_controller_0_wire_addr[9] = \new_sdram_controller_0|m_addr[9]~q ;

assign new_sdram_controller_0_wire_addr[10] = \new_sdram_controller_0|m_addr[10]~q ;

assign new_sdram_controller_0_wire_addr[11] = \new_sdram_controller_0|m_addr[11]~q ;

assign new_sdram_controller_0_wire_ba[0] = \new_sdram_controller_0|m_bank[0]~q ;

assign new_sdram_controller_0_wire_ba[1] = \new_sdram_controller_0|m_bank[1]~q ;

assign new_sdram_controller_0_wire_cas_n = ~ \new_sdram_controller_0|m_cmd[1]~q ;

assign new_sdram_controller_0_wire_cke = vcc;

assign new_sdram_controller_0_wire_cs_n = ~ \new_sdram_controller_0|m_cmd[3]~q ;

assign new_sdram_controller_0_wire_dqm[0] = \new_sdram_controller_0|m_dqm[0]~q ;

assign new_sdram_controller_0_wire_dqm[1] = \new_sdram_controller_0|m_dqm[1]~q ;

assign new_sdram_controller_0_wire_ras_n = ~ \new_sdram_controller_0|m_cmd[2]~q ;

assign new_sdram_controller_0_wire_we_n = ~ \new_sdram_controller_0|m_cmd[0]~q ;

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[0]~output (
	.i(\new_sdram_controller_0|m_data[0]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[0]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[0]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[0]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[1]~output (
	.i(\new_sdram_controller_0|m_data[1]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[1]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[1]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[1]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[2]~output (
	.i(\new_sdram_controller_0|m_data[2]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[2]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[2]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[2]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[3]~output (
	.i(\new_sdram_controller_0|m_data[3]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[3]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[3]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[3]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[4]~output (
	.i(\new_sdram_controller_0|m_data[4]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[4]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[4]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[4]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[5]~output (
	.i(\new_sdram_controller_0|m_data[5]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[5]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[5]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[5]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[6]~output (
	.i(\new_sdram_controller_0|m_data[6]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[6]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[6]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[6]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[7]~output (
	.i(\new_sdram_controller_0|m_data[7]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[7]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[7]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[7]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[8]~output (
	.i(\new_sdram_controller_0|m_data[8]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[8]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[8]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[8]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[9]~output (
	.i(\new_sdram_controller_0|m_data[9]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[9]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[9]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[9]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[10]~output (
	.i(\new_sdram_controller_0|m_data[10]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[10]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[10]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[10]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[11]~output (
	.i(\new_sdram_controller_0|m_data[11]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[11]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[11]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[11]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[12]~output (
	.i(\new_sdram_controller_0|m_data[12]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[12]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[12]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[12]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[13]~output (
	.i(\new_sdram_controller_0|m_data[13]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[13]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[13]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[13]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[14]~output (
	.i(\new_sdram_controller_0|m_data[14]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[14]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[14]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[14]~output .open_drain_output = "false";

cyclone10lp_io_obuf \new_sdram_controller_0_wire_dq[15]~output (
	.i(\new_sdram_controller_0|m_data[15]~q ),
	.oe(\new_sdram_controller_0|oe~q ),
	.seriesterminationcontrol(16'b0000000000000000),
	.o(new_sdram_controller_0_wire_dq[15]),
	.obar());
defparam \new_sdram_controller_0_wire_dq[15]~output .bus_hold = "false";
defparam \new_sdram_controller_0_wire_dq[15]~output .open_drain_output = "false";

assign \new_sdram_controller_0_s1_chipselect~input_o  = new_sdram_controller_0_s1_chipselect;

endmodule

module sdram_controller_altera_reset_controller (
	altera_reset_synchronizer_int_chain_out,
	clk_clk,
	reset_reset_n)/* synthesis synthesis_greybox=0 */;
output 	altera_reset_synchronizer_int_chain_out;
input 	clk_clk;
input 	reset_reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;



sdram_controller_altera_reset_synchronizer_1 alt_rst_sync_uq1(
	.altera_reset_synchronizer_int_chain_out1(altera_reset_synchronizer_int_chain_out),
	.clk(clk_clk),
	.reset_reset_n(reset_reset_n));

endmodule

module sdram_controller_altera_reset_synchronizer_1 (
	altera_reset_synchronizer_int_chain_out1,
	clk,
	reset_reset_n)/* synthesis synthesis_greybox=0 */;
output 	altera_reset_synchronizer_int_chain_out1;
input 	clk;
input 	reset_reset_n;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \altera_reset_synchronizer_int_chain[1]~q ;
wire \altera_reset_synchronizer_int_chain[0]~q ;


dffeas altera_reset_synchronizer_int_chain_out(
	.clk(clk),
	.d(\altera_reset_synchronizer_int_chain[0]~q ),
	.asdata(vcc),
	.clrn(reset_reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(altera_reset_synchronizer_int_chain_out1),
	.prn(vcc));
defparam altera_reset_synchronizer_int_chain_out.is_wysiwyg = "true";
defparam altera_reset_synchronizer_int_chain_out.power_up = "low";

dffeas \altera_reset_synchronizer_int_chain[1] (
	.clk(clk),
	.d(vcc),
	.asdata(vcc),
	.clrn(reset_reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\altera_reset_synchronizer_int_chain[1]~q ),
	.prn(vcc));
defparam \altera_reset_synchronizer_int_chain[1] .is_wysiwyg = "true";
defparam \altera_reset_synchronizer_int_chain[1] .power_up = "low";

dffeas \altera_reset_synchronizer_int_chain[0] (
	.clk(clk),
	.d(\altera_reset_synchronizer_int_chain[1]~q ),
	.asdata(vcc),
	.clrn(reset_reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\altera_reset_synchronizer_int_chain[0]~q ),
	.prn(vcc));
defparam \altera_reset_synchronizer_int_chain[0] .is_wysiwyg = "true";
defparam \altera_reset_synchronizer_int_chain[0] .power_up = "low";

endmodule

module sdram_controller_sdram_controller_new_sdram_controller_0 (
	m_addr_0,
	m_addr_1,
	m_addr_2,
	m_addr_3,
	m_addr_4,
	m_addr_5,
	m_addr_6,
	m_addr_7,
	oe1,
	za_data_0,
	za_data_1,
	za_data_2,
	za_data_3,
	za_data_4,
	za_data_5,
	za_data_6,
	za_data_7,
	za_data_8,
	za_data_9,
	za_data_10,
	za_data_11,
	za_data_12,
	za_data_13,
	za_data_14,
	za_data_15,
	za_valid1,
	Equal0,
	m_addr_8,
	m_addr_9,
	m_addr_10,
	m_addr_11,
	m_bank_0,
	m_bank_1,
	m_cmd_1,
	m_cmd_3,
	m_dqm_0,
	m_dqm_1,
	m_cmd_2,
	m_cmd_0,
	altera_reset_synchronizer_int_chain_out,
	m_data_0,
	m_data_1,
	m_data_2,
	m_data_3,
	m_data_4,
	m_data_5,
	m_data_6,
	m_data_7,
	m_data_8,
	m_data_9,
	m_data_10,
	m_data_11,
	m_data_12,
	m_data_13,
	m_data_14,
	m_data_15,
	new_sdram_controller_0_wire_dq_0,
	new_sdram_controller_0_wire_dq_1,
	new_sdram_controller_0_wire_dq_2,
	new_sdram_controller_0_wire_dq_3,
	new_sdram_controller_0_wire_dq_4,
	new_sdram_controller_0_wire_dq_5,
	new_sdram_controller_0_wire_dq_6,
	new_sdram_controller_0_wire_dq_7,
	new_sdram_controller_0_wire_dq_8,
	new_sdram_controller_0_wire_dq_9,
	new_sdram_controller_0_wire_dq_10,
	new_sdram_controller_0_wire_dq_11,
	new_sdram_controller_0_wire_dq_12,
	new_sdram_controller_0_wire_dq_13,
	new_sdram_controller_0_wire_dq_14,
	new_sdram_controller_0_wire_dq_15,
	clk_clk,
	new_sdram_controller_0_s1_read_n,
	new_sdram_controller_0_s1_write_n,
	new_sdram_controller_0_s1_address_8,
	new_sdram_controller_0_s1_address_9,
	new_sdram_controller_0_s1_address_21,
	new_sdram_controller_0_s1_address_11,
	new_sdram_controller_0_s1_address_10,
	new_sdram_controller_0_s1_address_13,
	new_sdram_controller_0_s1_address_12,
	new_sdram_controller_0_s1_address_15,
	new_sdram_controller_0_s1_address_14,
	new_sdram_controller_0_s1_address_17,
	new_sdram_controller_0_s1_address_16,
	new_sdram_controller_0_s1_address_19,
	new_sdram_controller_0_s1_address_18,
	new_sdram_controller_0_s1_address_20,
	new_sdram_controller_0_s1_address_0,
	new_sdram_controller_0_s1_address_1,
	new_sdram_controller_0_s1_address_2,
	new_sdram_controller_0_s1_address_3,
	new_sdram_controller_0_s1_address_4,
	new_sdram_controller_0_s1_address_5,
	new_sdram_controller_0_s1_address_6,
	new_sdram_controller_0_s1_address_7,
	new_sdram_controller_0_s1_byteenable_n_0,
	new_sdram_controller_0_s1_byteenable_n_1,
	new_sdram_controller_0_s1_writedata_0,
	new_sdram_controller_0_s1_writedata_1,
	new_sdram_controller_0_s1_writedata_2,
	new_sdram_controller_0_s1_writedata_3,
	new_sdram_controller_0_s1_writedata_4,
	new_sdram_controller_0_s1_writedata_5,
	new_sdram_controller_0_s1_writedata_6,
	new_sdram_controller_0_s1_writedata_7,
	new_sdram_controller_0_s1_writedata_8,
	new_sdram_controller_0_s1_writedata_9,
	new_sdram_controller_0_s1_writedata_10,
	new_sdram_controller_0_s1_writedata_11,
	new_sdram_controller_0_s1_writedata_12,
	new_sdram_controller_0_s1_writedata_13,
	new_sdram_controller_0_s1_writedata_14,
	new_sdram_controller_0_s1_writedata_15)/* synthesis synthesis_greybox=0 */;
output 	m_addr_0;
output 	m_addr_1;
output 	m_addr_2;
output 	m_addr_3;
output 	m_addr_4;
output 	m_addr_5;
output 	m_addr_6;
output 	m_addr_7;
output 	oe1;
output 	za_data_0;
output 	za_data_1;
output 	za_data_2;
output 	za_data_3;
output 	za_data_4;
output 	za_data_5;
output 	za_data_6;
output 	za_data_7;
output 	za_data_8;
output 	za_data_9;
output 	za_data_10;
output 	za_data_11;
output 	za_data_12;
output 	za_data_13;
output 	za_data_14;
output 	za_data_15;
output 	za_valid1;
output 	Equal0;
output 	m_addr_8;
output 	m_addr_9;
output 	m_addr_10;
output 	m_addr_11;
output 	m_bank_0;
output 	m_bank_1;
output 	m_cmd_1;
output 	m_cmd_3;
output 	m_dqm_0;
output 	m_dqm_1;
output 	m_cmd_2;
output 	m_cmd_0;
input 	altera_reset_synchronizer_int_chain_out;
output 	m_data_0;
output 	m_data_1;
output 	m_data_2;
output 	m_data_3;
output 	m_data_4;
output 	m_data_5;
output 	m_data_6;
output 	m_data_7;
output 	m_data_8;
output 	m_data_9;
output 	m_data_10;
output 	m_data_11;
output 	m_data_12;
output 	m_data_13;
output 	m_data_14;
output 	m_data_15;
input 	new_sdram_controller_0_wire_dq_0;
input 	new_sdram_controller_0_wire_dq_1;
input 	new_sdram_controller_0_wire_dq_2;
input 	new_sdram_controller_0_wire_dq_3;
input 	new_sdram_controller_0_wire_dq_4;
input 	new_sdram_controller_0_wire_dq_5;
input 	new_sdram_controller_0_wire_dq_6;
input 	new_sdram_controller_0_wire_dq_7;
input 	new_sdram_controller_0_wire_dq_8;
input 	new_sdram_controller_0_wire_dq_9;
input 	new_sdram_controller_0_wire_dq_10;
input 	new_sdram_controller_0_wire_dq_11;
input 	new_sdram_controller_0_wire_dq_12;
input 	new_sdram_controller_0_wire_dq_13;
input 	new_sdram_controller_0_wire_dq_14;
input 	new_sdram_controller_0_wire_dq_15;
input 	clk_clk;
input 	new_sdram_controller_0_s1_read_n;
input 	new_sdram_controller_0_s1_write_n;
input 	new_sdram_controller_0_s1_address_8;
input 	new_sdram_controller_0_s1_address_9;
input 	new_sdram_controller_0_s1_address_21;
input 	new_sdram_controller_0_s1_address_11;
input 	new_sdram_controller_0_s1_address_10;
input 	new_sdram_controller_0_s1_address_13;
input 	new_sdram_controller_0_s1_address_12;
input 	new_sdram_controller_0_s1_address_15;
input 	new_sdram_controller_0_s1_address_14;
input 	new_sdram_controller_0_s1_address_17;
input 	new_sdram_controller_0_s1_address_16;
input 	new_sdram_controller_0_s1_address_19;
input 	new_sdram_controller_0_s1_address_18;
input 	new_sdram_controller_0_s1_address_20;
input 	new_sdram_controller_0_s1_address_0;
input 	new_sdram_controller_0_s1_address_1;
input 	new_sdram_controller_0_s1_address_2;
input 	new_sdram_controller_0_s1_address_3;
input 	new_sdram_controller_0_s1_address_4;
input 	new_sdram_controller_0_s1_address_5;
input 	new_sdram_controller_0_s1_address_6;
input 	new_sdram_controller_0_s1_address_7;
input 	new_sdram_controller_0_s1_byteenable_n_0;
input 	new_sdram_controller_0_s1_byteenable_n_1;
input 	new_sdram_controller_0_s1_writedata_0;
input 	new_sdram_controller_0_s1_writedata_1;
input 	new_sdram_controller_0_s1_writedata_2;
input 	new_sdram_controller_0_s1_writedata_3;
input 	new_sdram_controller_0_s1_writedata_4;
input 	new_sdram_controller_0_s1_writedata_5;
input 	new_sdram_controller_0_s1_writedata_6;
input 	new_sdram_controller_0_s1_writedata_7;
input 	new_sdram_controller_0_s1_writedata_8;
input 	new_sdram_controller_0_s1_writedata_9;
input 	new_sdram_controller_0_s1_writedata_10;
input 	new_sdram_controller_0_s1_writedata_11;
input 	new_sdram_controller_0_s1_writedata_12;
input 	new_sdram_controller_0_s1_writedata_13;
input 	new_sdram_controller_0_s1_writedata_14;
input 	new_sdram_controller_0_s1_writedata_15;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[26]~0_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[27]~2_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[39]~3_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[29]~4_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[28]~5_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[31]~6_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[30]~7_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[33]~8_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[32]~9_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[35]~10_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[34]~11_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[37]~12_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[36]~13_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[38]~14_combout ;
wire \comb~0_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[18]~15_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[19]~16_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[20]~17_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[21]~18_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[22]~19_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[23]~20_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[24]~21_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[25]~22_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[16]~23_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[17]~24_combout ;
wire \comb~1_combout ;
wire \comb~2_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[0]~25_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[1]~26_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[2]~27_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[3]~28_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[4]~29_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[5]~30_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[6]~31_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[7]~32_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[8]~33_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[9]~34_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[10]~35_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[11]~36_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[12]~37_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[13]~38_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[14]~39_combout ;
wire \the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[15]~40_combout ;
wire \Add0~0_combout ;
wire \refresh_counter[0]~q ;
wire \Add0~1 ;
wire \Add0~2_combout ;
wire \refresh_counter[1]~q ;
wire \Add0~3 ;
wire \Add0~4_combout ;
wire \refresh_counter[2]~q ;
wire \Add0~5 ;
wire \Add0~6_combout ;
wire \refresh_counter~7_combout ;
wire \refresh_counter[3]~q ;
wire \Add0~7 ;
wire \Add0~8_combout ;
wire \refresh_counter[4]~q ;
wire \Add0~9 ;
wire \Add0~10_combout ;
wire \refresh_counter~6_combout ;
wire \refresh_counter[5]~q ;
wire \Add0~11 ;
wire \Add0~12_combout ;
wire \refresh_counter~5_combout ;
wire \refresh_counter[6]~q ;
wire \Add0~13 ;
wire \Add0~14_combout ;
wire \refresh_counter[7]~q ;
wire \Add0~15 ;
wire \Add0~16_combout ;
wire \refresh_counter~4_combout ;
wire \refresh_counter[8]~q ;
wire \Add0~17 ;
wire \Add0~18_combout ;
wire \refresh_counter[9]~q ;
wire \Add0~19 ;
wire \Add0~20_combout ;
wire \refresh_counter[10]~10_combout ;
wire \refresh_counter[10]~q ;
wire \Add0~21 ;
wire \Add0~22_combout ;
wire \refresh_counter~3_combout ;
wire \refresh_counter[11]~q ;
wire \Add0~23 ;
wire \Add0~24_combout ;
wire \refresh_counter~1_combout ;
wire \refresh_counter[12]~q ;
wire \Add0~25 ;
wire \Add0~26_combout ;
wire \refresh_counter~2_combout ;
wire \refresh_counter[13]~q ;
wire \Add0~27 ;
wire \Add0~28_combout ;
wire \refresh_counter~0_combout ;
wire \refresh_counter[14]~q ;
wire \Equal0~0_combout ;
wire \Equal0~1_combout ;
wire \Equal0~2_combout ;
wire \Equal0~3_combout ;
wire \Equal0~4_combout ;
wire \i_next.000~0_combout ;
wire \i_next.000~q ;
wire \Selector7~0_combout ;
wire \i_state.000~q ;
wire \Selector8~0_combout ;
wire \i_state.001~q ;
wire \Selector6~0_combout ;
wire \i_refs[0]~q ;
wire \Selector5~0_combout ;
wire \i_refs[1]~q ;
wire \Selector4~0_combout ;
wire \Selector4~1_combout ;
wire \i_refs[2]~q ;
wire \Selector16~0_combout ;
wire \i_count[2]~2_combout ;
wire \Selector16~1_combout ;
wire \i_next.010~q ;
wire \i_count[0]~6_combout ;
wire \i_count[2]~3_combout ;
wire \i_count[0]~7_combout ;
wire \i_count[0]~q ;
wire \Selector14~1_combout ;
wire \Selector14~0_combout ;
wire \Selector14~2_combout ;
wire \i_count[1]~q ;
wire \i_count[2]~4_combout ;
wire \i_count[2]~5_combout ;
wire \i_count[2]~q ;
wire \Selector9~0_combout ;
wire \i_state.010~q ;
wire \Selector18~0_combout ;
wire \Selector18~1_combout ;
wire \i_next.111~q ;
wire \Selector12~0_combout ;
wire \i_state.111~q ;
wire \Selector10~2_combout ;
wire \Selector10~3_combout ;
wire \i_state.011~q ;
wire \i_count[2]~1_combout ;
wire \Selector17~0_combout ;
wire \i_next.101~q ;
wire \i_state.101~0_combout ;
wire \i_state.101~q ;
wire \init_done~0_combout ;
wire \init_done~q ;
wire \Selector24~1_combout ;
wire \active_rnw~q ;
wire \active_addr[8]~q ;
wire \pending~0_combout ;
wire \active_addr[21]~q ;
wire \pending~1_combout ;
wire \active_addr[10]~q ;
wire \active_addr[11]~q ;
wire \pending~2_combout ;
wire \active_addr[12]~q ;
wire \active_addr[13]~q ;
wire \pending~3_combout ;
wire \pending~4_combout ;
wire \active_addr[14]~q ;
wire \active_addr[15]~q ;
wire \pending~5_combout ;
wire \active_addr[16]~q ;
wire \active_addr[17]~q ;
wire \pending~6_combout ;
wire \active_addr[18]~q ;
wire \active_addr[19]~q ;
wire \pending~7_combout ;
wire \active_addr[20]~q ;
wire \Selector30~4_combout ;
wire \active_cs_n~0_combout ;
wire \active_cs_n~1_combout ;
wire \active_cs_n~q ;
wire \pending~8_combout ;
wire \pending~9_combout ;
wire \Selector41~4_combout ;
wire \Selector41~5_combout ;
wire \Selector41~8_combout ;
wire \m_state.000000010~q ;
wire \m_next~17_combout ;
wire \m_count[1]~2_combout ;
wire \Selector36~0_combout ;
wire \Selector27~0_combout ;
wire \Selector35~0_combout ;
wire \Selector34~3_combout ;
wire \Selector34~4_combout ;
wire \WideOr8~1_combout ;
wire \Selector34~5_combout ;
wire \Selector34~6_combout ;
wire \m_next.000010000~q ;
wire \pending~10_combout ;
wire \Selector27~1_combout ;
wire \Selector28~0_combout ;
wire \Selector27~3_combout ;
wire \Selector27~4_combout ;
wire \Selector27~5_combout ;
wire \Selector27~6_combout ;
wire \Selector27~7_combout ;
wire \m_state.000010000~q ;
wire \m_count[1]~11_combout ;
wire \Selector36~1_combout ;
wire \m_next.010000000~q ;
wire \m_count[1]~3_combout ;
wire \m_count[1]~4_combout ;
wire \m_count[1]~6_combout ;
wire \m_count[0]~9_combout ;
wire \m_count[0]~10_combout ;
wire \m_count[0]~q ;
wire \m_count[1]~5_combout ;
wire \m_count[1]~7_combout ;
wire \m_count[1]~8_combout ;
wire \m_count[1]~q ;
wire \Selector37~0_combout ;
wire \Selector37~1_combout ;
wire \Selector37~2_combout ;
wire \m_count[2]~q ;
wire \Selector31~0_combout ;
wire \m_state.010000000~q ;
wire \Selector34~1_combout ;
wire \Selector34~2_combout ;
wire \m_next.000001000~q ;
wire \Selector27~2_combout ;
wire \m_state.000001000~q ;
wire \WideOr9~0_combout ;
wire \Selector32~0_combout ;
wire \Selector32~1_combout ;
wire \m_state.100000000~q ;
wire \Selector29~0_combout ;
wire \Selector29~1_combout ;
wire \m_state.000100000~q ;
wire \Selector30~5_combout ;
wire \Selector30~6_combout ;
wire \m_state.001000000~q ;
wire \Selector26~0_combout ;
wire \Selector26~1_combout ;
wire \Selector34~0_combout ;
wire \Selector26~2_combout ;
wire \m_state.000000100~q ;
wire \Selector24~0_combout ;
wire \Selector33~0_combout ;
wire \Selector33~1_combout ;
wire \Selector33~2_combout ;
wire \Selector33~3_combout ;
wire \Selector33~4_combout ;
wire \m_next.000000001~q ;
wire \Selector24~2_combout ;
wire \m_state.000000001~q ;
wire \Selector23~0_combout ;
wire \ack_refresh_request~q ;
wire \refresh_request~0_combout ;
wire \refresh_request~q ;
wire \active_rnw~2_combout ;
wire \active_rnw~4_combout ;
wire \active_rnw~5_combout ;
wire \active_rnw~3_combout ;
wire \active_addr[9]~q ;
wire \Selector41~6_combout ;
wire \Selector41~7_combout ;
wire \f_pop~q ;
wire \m_addr[6]~2_combout ;
wire \active_addr[0]~q ;
wire \i_addr[11]~q ;
wire \Selector94~0_combout ;
wire \Selector94~1_combout ;
wire \m_addr[6]~3_combout ;
wire \m_addr[6]~4_combout ;
wire \active_addr[1]~q ;
wire \Selector93~0_combout ;
wire \Selector93~1_combout ;
wire \active_addr[2]~q ;
wire \Selector92~0_combout ;
wire \Selector92~1_combout ;
wire \active_addr[3]~q ;
wire \Selector91~0_combout ;
wire \Selector91~1_combout ;
wire \active_addr[4]~q ;
wire \f_select~combout ;
wire \Selector90~0_combout ;
wire \Selector90~1_combout ;
wire \active_addr[5]~q ;
wire \Selector89~0_combout ;
wire \Selector89~1_combout ;
wire \active_addr[6]~q ;
wire \Selector88~0_combout ;
wire \Selector88~1_combout ;
wire \active_addr[7]~q ;
wire \Selector87~0_combout ;
wire \Selector87~1_combout ;
wire \always5~0_combout ;
wire \Equal4~0_combout ;
wire \rd_valid[0]~q ;
wire \rd_valid[1]~q ;
wire \rd_valid[2]~q ;
wire \Selector86~2_combout ;
wire \Selector86~3_combout ;
wire \Selector85~2_combout ;
wire \Selector85~3_combout ;
wire \Selector84~2_combout ;
wire \Selector84~3_combout ;
wire \Selector83~2_combout ;
wire \Selector83~3_combout ;
wire \Selector96~0_combout ;
wire \WideOr16~0_combout ;
wire \Selector95~0_combout ;
wire \i_count[2]~0_combout ;
wire \Selector2~0_combout ;
wire \i_cmd[1]~q ;
wire \Selector21~0_combout ;
wire \Selector21~1_combout ;
wire \Selector0~0_combout ;
wire \i_cmd[3]~q ;
wire \Selector19~0_combout ;
wire \Selector19~1_combout ;
wire \Selector19~2_combout ;
wire \Selector19~3_combout ;
wire \active_dqm[0]~q ;
wire \Selector114~0_combout ;
wire \active_dqm[1]~q ;
wire \Selector113~0_combout ;
wire \WideOr8~0_combout ;
wire \Selector1~0_combout ;
wire \i_cmd[2]~q ;
wire \Selector20~0_combout ;
wire \Selector3~0_combout ;
wire \i_cmd[0]~q ;
wire \Selector22~0_combout ;
wire \Selector22~1_combout ;
wire \active_data[0]~q ;
wire \Selector112~0_combout ;
wire \Selector112~1_combout ;
wire \active_data[1]~q ;
wire \Selector111~0_combout ;
wire \Selector111~1_combout ;
wire \active_data[2]~q ;
wire \Selector110~0_combout ;
wire \Selector110~1_combout ;
wire \active_data[3]~q ;
wire \Selector109~0_combout ;
wire \Selector109~1_combout ;
wire \active_data[4]~q ;
wire \Selector108~0_combout ;
wire \Selector108~1_combout ;
wire \active_data[5]~q ;
wire \Selector107~0_combout ;
wire \Selector107~1_combout ;
wire \active_data[6]~q ;
wire \Selector106~0_combout ;
wire \Selector106~1_combout ;
wire \active_data[7]~q ;
wire \Selector105~0_combout ;
wire \Selector105~1_combout ;
wire \active_data[8]~q ;
wire \Selector104~0_combout ;
wire \Selector104~1_combout ;
wire \active_data[9]~q ;
wire \Selector103~0_combout ;
wire \Selector103~1_combout ;
wire \active_data[10]~q ;
wire \Selector102~0_combout ;
wire \Selector102~1_combout ;
wire \active_data[11]~q ;
wire \Selector101~0_combout ;
wire \Selector101~1_combout ;
wire \active_data[12]~q ;
wire \Selector100~0_combout ;
wire \Selector100~1_combout ;
wire \active_data[13]~q ;
wire \Selector99~0_combout ;
wire \Selector99~1_combout ;
wire \active_data[14]~q ;
wire \Selector98~0_combout ;
wire \Selector98~1_combout ;
wire \active_data[15]~q ;
wire \Selector97~0_combout ;
wire \Selector97~1_combout ;


sdram_controller_sdram_controller_new_sdram_controller_0_input_efifo_module the_sdram_controller_new_sdram_controller_0_input_efifo_module(
	.entries_1(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.entries_0(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.Equal0(Equal0),
	.reset_n(altera_reset_synchronizer_int_chain_out),
	.Equal1(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.rd_data_26(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[26]~0_combout ),
	.rd_data_40(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ),
	.rd_data_27(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[27]~2_combout ),
	.rd_data_39(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[39]~3_combout ),
	.rd_data_29(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[29]~4_combout ),
	.rd_data_28(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[28]~5_combout ),
	.rd_data_31(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[31]~6_combout ),
	.rd_data_30(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[30]~7_combout ),
	.rd_data_33(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[33]~8_combout ),
	.rd_data_32(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[32]~9_combout ),
	.rd_data_35(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[35]~10_combout ),
	.rd_data_34(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[34]~11_combout ),
	.rd_data_37(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[37]~12_combout ),
	.rd_data_36(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[36]~13_combout ),
	.rd_data_38(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[38]~14_combout ),
	.f_select(\f_select~combout ),
	.comb(\comb~0_combout ),
	.rd_data_18(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[18]~15_combout ),
	.rd_data_19(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[19]~16_combout ),
	.rd_data_20(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[20]~17_combout ),
	.rd_data_21(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[21]~18_combout ),
	.rd_data_22(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[22]~19_combout ),
	.rd_data_23(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[23]~20_combout ),
	.rd_data_24(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[24]~21_combout ),
	.rd_data_25(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[25]~22_combout ),
	.rd_data_16(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[16]~23_combout ),
	.rd_data_17(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[17]~24_combout ),
	.comb1(\comb~1_combout ),
	.comb2(\comb~2_combout ),
	.rd_data_0(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[0]~25_combout ),
	.rd_data_1(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[1]~26_combout ),
	.rd_data_2(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[2]~27_combout ),
	.rd_data_3(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[3]~28_combout ),
	.rd_data_4(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[4]~29_combout ),
	.rd_data_5(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[5]~30_combout ),
	.rd_data_6(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[6]~31_combout ),
	.rd_data_7(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[7]~32_combout ),
	.rd_data_8(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[8]~33_combout ),
	.rd_data_9(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[9]~34_combout ),
	.rd_data_10(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[10]~35_combout ),
	.rd_data_11(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[11]~36_combout ),
	.rd_data_12(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[12]~37_combout ),
	.rd_data_13(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[13]~38_combout ),
	.rd_data_14(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[14]~39_combout ),
	.rd_data_15(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[15]~40_combout ),
	.clk(clk_clk),
	.new_sdram_controller_0_s1_read_n(new_sdram_controller_0_s1_read_n),
	.new_sdram_controller_0_s1_write_n(new_sdram_controller_0_s1_write_n),
	.new_sdram_controller_0_s1_address_8(new_sdram_controller_0_s1_address_8),
	.new_sdram_controller_0_s1_address_9(new_sdram_controller_0_s1_address_9),
	.new_sdram_controller_0_s1_address_21(new_sdram_controller_0_s1_address_21),
	.new_sdram_controller_0_s1_address_11(new_sdram_controller_0_s1_address_11),
	.new_sdram_controller_0_s1_address_10(new_sdram_controller_0_s1_address_10),
	.new_sdram_controller_0_s1_address_13(new_sdram_controller_0_s1_address_13),
	.new_sdram_controller_0_s1_address_12(new_sdram_controller_0_s1_address_12),
	.new_sdram_controller_0_s1_address_15(new_sdram_controller_0_s1_address_15),
	.new_sdram_controller_0_s1_address_14(new_sdram_controller_0_s1_address_14),
	.new_sdram_controller_0_s1_address_17(new_sdram_controller_0_s1_address_17),
	.new_sdram_controller_0_s1_address_16(new_sdram_controller_0_s1_address_16),
	.new_sdram_controller_0_s1_address_19(new_sdram_controller_0_s1_address_19),
	.new_sdram_controller_0_s1_address_18(new_sdram_controller_0_s1_address_18),
	.new_sdram_controller_0_s1_address_20(new_sdram_controller_0_s1_address_20),
	.new_sdram_controller_0_s1_address_0(new_sdram_controller_0_s1_address_0),
	.new_sdram_controller_0_s1_address_1(new_sdram_controller_0_s1_address_1),
	.new_sdram_controller_0_s1_address_2(new_sdram_controller_0_s1_address_2),
	.new_sdram_controller_0_s1_address_3(new_sdram_controller_0_s1_address_3),
	.new_sdram_controller_0_s1_address_4(new_sdram_controller_0_s1_address_4),
	.new_sdram_controller_0_s1_address_5(new_sdram_controller_0_s1_address_5),
	.new_sdram_controller_0_s1_address_6(new_sdram_controller_0_s1_address_6),
	.new_sdram_controller_0_s1_address_7(new_sdram_controller_0_s1_address_7),
	.new_sdram_controller_0_s1_writedata_0(new_sdram_controller_0_s1_writedata_0),
	.new_sdram_controller_0_s1_writedata_1(new_sdram_controller_0_s1_writedata_1),
	.new_sdram_controller_0_s1_writedata_2(new_sdram_controller_0_s1_writedata_2),
	.new_sdram_controller_0_s1_writedata_3(new_sdram_controller_0_s1_writedata_3),
	.new_sdram_controller_0_s1_writedata_4(new_sdram_controller_0_s1_writedata_4),
	.new_sdram_controller_0_s1_writedata_5(new_sdram_controller_0_s1_writedata_5),
	.new_sdram_controller_0_s1_writedata_6(new_sdram_controller_0_s1_writedata_6),
	.new_sdram_controller_0_s1_writedata_7(new_sdram_controller_0_s1_writedata_7),
	.new_sdram_controller_0_s1_writedata_8(new_sdram_controller_0_s1_writedata_8),
	.new_sdram_controller_0_s1_writedata_9(new_sdram_controller_0_s1_writedata_9),
	.new_sdram_controller_0_s1_writedata_10(new_sdram_controller_0_s1_writedata_10),
	.new_sdram_controller_0_s1_writedata_11(new_sdram_controller_0_s1_writedata_11),
	.new_sdram_controller_0_s1_writedata_12(new_sdram_controller_0_s1_writedata_12),
	.new_sdram_controller_0_s1_writedata_13(new_sdram_controller_0_s1_writedata_13),
	.new_sdram_controller_0_s1_writedata_14(new_sdram_controller_0_s1_writedata_14),
	.new_sdram_controller_0_s1_writedata_15(new_sdram_controller_0_s1_writedata_15));

cyclone10lp_lcell_comb \comb~0 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datac(new_sdram_controller_0_s1_read_n),
	.datad(new_sdram_controller_0_s1_write_n),
	.cin(gnd),
	.combout(\comb~0_combout ),
	.cout());
defparam \comb~0 .lut_mask = 16'h0BBB;
defparam \comb~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \comb~1 (
	.dataa(new_sdram_controller_0_s1_byteenable_n_0),
	.datab(gnd),
	.datac(gnd),
	.datad(new_sdram_controller_0_s1_write_n),
	.cin(gnd),
	.combout(\comb~1_combout ),
	.cout());
defparam \comb~1 .lut_mask = 16'h00AA;
defparam \comb~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \comb~2 (
	.dataa(new_sdram_controller_0_s1_byteenable_n_1),
	.datab(gnd),
	.datac(gnd),
	.datad(new_sdram_controller_0_s1_write_n),
	.cin(gnd),
	.combout(\comb~2_combout ),
	.cout());
defparam \comb~2 .lut_mask = 16'h00AA;
defparam \comb~2 .sum_lutc_input = "datac";

dffeas \m_addr[0] (
	.clk(clk_clk),
	.d(\Selector94~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_0),
	.prn(vcc));
defparam \m_addr[0] .is_wysiwyg = "true";
defparam \m_addr[0] .power_up = "low";

dffeas \m_addr[1] (
	.clk(clk_clk),
	.d(\Selector93~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_1),
	.prn(vcc));
defparam \m_addr[1] .is_wysiwyg = "true";
defparam \m_addr[1] .power_up = "low";

dffeas \m_addr[2] (
	.clk(clk_clk),
	.d(\Selector92~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_2),
	.prn(vcc));
defparam \m_addr[2] .is_wysiwyg = "true";
defparam \m_addr[2] .power_up = "low";

dffeas \m_addr[3] (
	.clk(clk_clk),
	.d(\Selector91~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_3),
	.prn(vcc));
defparam \m_addr[3] .is_wysiwyg = "true";
defparam \m_addr[3] .power_up = "low";

dffeas \m_addr[4] (
	.clk(clk_clk),
	.d(\Selector90~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_4),
	.prn(vcc));
defparam \m_addr[4] .is_wysiwyg = "true";
defparam \m_addr[4] .power_up = "low";

dffeas \m_addr[5] (
	.clk(clk_clk),
	.d(\Selector89~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_5),
	.prn(vcc));
defparam \m_addr[5] .is_wysiwyg = "true";
defparam \m_addr[5] .power_up = "low";

dffeas \m_addr[6] (
	.clk(clk_clk),
	.d(\Selector88~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_6),
	.prn(vcc));
defparam \m_addr[6] .is_wysiwyg = "true";
defparam \m_addr[6] .power_up = "low";

dffeas \m_addr[7] (
	.clk(clk_clk),
	.d(\Selector87~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(\m_state.001000000~q ),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_7),
	.prn(vcc));
defparam \m_addr[7] .is_wysiwyg = "true";
defparam \m_addr[7] .power_up = "low";

dffeas oe(
	.clk(clk_clk),
	.d(\always5~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(!\m_state.000010000~q ),
	.sload(gnd),
	.ena(vcc),
	.q(oe1),
	.prn(vcc));
defparam oe.is_wysiwyg = "true";
defparam oe.power_up = "low";

dffeas \za_data[0] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_0),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_0),
	.prn(vcc));
defparam \za_data[0] .is_wysiwyg = "true";
defparam \za_data[0] .power_up = "low";

dffeas \za_data[1] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_1),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_1),
	.prn(vcc));
defparam \za_data[1] .is_wysiwyg = "true";
defparam \za_data[1] .power_up = "low";

dffeas \za_data[2] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_2),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_2),
	.prn(vcc));
defparam \za_data[2] .is_wysiwyg = "true";
defparam \za_data[2] .power_up = "low";

dffeas \za_data[3] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_3),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_3),
	.prn(vcc));
defparam \za_data[3] .is_wysiwyg = "true";
defparam \za_data[3] .power_up = "low";

dffeas \za_data[4] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_4),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_4),
	.prn(vcc));
defparam \za_data[4] .is_wysiwyg = "true";
defparam \za_data[4] .power_up = "low";

dffeas \za_data[5] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_5),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_5),
	.prn(vcc));
defparam \za_data[5] .is_wysiwyg = "true";
defparam \za_data[5] .power_up = "low";

dffeas \za_data[6] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_6),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_6),
	.prn(vcc));
defparam \za_data[6] .is_wysiwyg = "true";
defparam \za_data[6] .power_up = "low";

dffeas \za_data[7] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_7),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_7),
	.prn(vcc));
defparam \za_data[7] .is_wysiwyg = "true";
defparam \za_data[7] .power_up = "low";

dffeas \za_data[8] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_8),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_8),
	.prn(vcc));
defparam \za_data[8] .is_wysiwyg = "true";
defparam \za_data[8] .power_up = "low";

dffeas \za_data[9] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_9),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_9),
	.prn(vcc));
defparam \za_data[9] .is_wysiwyg = "true";
defparam \za_data[9] .power_up = "low";

dffeas \za_data[10] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_10),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_10),
	.prn(vcc));
defparam \za_data[10] .is_wysiwyg = "true";
defparam \za_data[10] .power_up = "low";

dffeas \za_data[11] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_11),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_11),
	.prn(vcc));
defparam \za_data[11] .is_wysiwyg = "true";
defparam \za_data[11] .power_up = "low";

dffeas \za_data[12] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_12),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_12),
	.prn(vcc));
defparam \za_data[12] .is_wysiwyg = "true";
defparam \za_data[12] .power_up = "low";

dffeas \za_data[13] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_13),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_13),
	.prn(vcc));
defparam \za_data[13] .is_wysiwyg = "true";
defparam \za_data[13] .power_up = "low";

dffeas \za_data[14] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_14),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_14),
	.prn(vcc));
defparam \za_data[14] .is_wysiwyg = "true";
defparam \za_data[14] .power_up = "low";

dffeas \za_data[15] (
	.clk(clk_clk),
	.d(new_sdram_controller_0_wire_dq_15),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_data_15),
	.prn(vcc));
defparam \za_data[15] .is_wysiwyg = "true";
defparam \za_data[15] .power_up = "low";

dffeas za_valid(
	.clk(clk_clk),
	.d(\rd_valid[2]~q ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(za_valid1),
	.prn(vcc));
defparam za_valid.is_wysiwyg = "true";
defparam za_valid.power_up = "low";

dffeas \m_addr[8] (
	.clk(clk_clk),
	.d(\Selector86~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_8),
	.prn(vcc));
defparam \m_addr[8] .is_wysiwyg = "true";
defparam \m_addr[8] .power_up = "low";

dffeas \m_addr[9] (
	.clk(clk_clk),
	.d(\Selector85~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_9),
	.prn(vcc));
defparam \m_addr[9] .is_wysiwyg = "true";
defparam \m_addr[9] .power_up = "low";

dffeas \m_addr[10] (
	.clk(clk_clk),
	.d(\Selector84~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_10),
	.prn(vcc));
defparam \m_addr[10] .is_wysiwyg = "true";
defparam \m_addr[10] .power_up = "low";

dffeas \m_addr[11] (
	.clk(clk_clk),
	.d(\Selector83~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\m_addr[6]~4_combout ),
	.q(m_addr_11),
	.prn(vcc));
defparam \m_addr[11] .is_wysiwyg = "true";
defparam \m_addr[11] .power_up = "low";

dffeas \m_bank[0] (
	.clk(clk_clk),
	.d(\Selector96~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\WideOr16~0_combout ),
	.q(m_bank_0),
	.prn(vcc));
defparam \m_bank[0] .is_wysiwyg = "true";
defparam \m_bank[0] .power_up = "low";

dffeas \m_bank[1] (
	.clk(clk_clk),
	.d(\Selector95~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\WideOr16~0_combout ),
	.q(m_bank_1),
	.prn(vcc));
defparam \m_bank[1] .is_wysiwyg = "true";
defparam \m_bank[1] .power_up = "low";

dffeas \m_cmd[1] (
	.clk(clk_clk),
	.d(\Selector21~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_cmd_1),
	.prn(vcc));
defparam \m_cmd[1] .is_wysiwyg = "true";
defparam \m_cmd[1] .power_up = "low";

dffeas \m_cmd[3] (
	.clk(clk_clk),
	.d(\Selector19~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_cmd_3),
	.prn(vcc));
defparam \m_cmd[3] .is_wysiwyg = "true";
defparam \m_cmd[3] .power_up = "low";

dffeas \m_dqm[0] (
	.clk(clk_clk),
	.d(\Selector114~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\WideOr16~0_combout ),
	.q(m_dqm_0),
	.prn(vcc));
defparam \m_dqm[0] .is_wysiwyg = "true";
defparam \m_dqm[0] .power_up = "low";

dffeas \m_dqm[1] (
	.clk(clk_clk),
	.d(\Selector113~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\WideOr16~0_combout ),
	.q(m_dqm_1),
	.prn(vcc));
defparam \m_dqm[1] .is_wysiwyg = "true";
defparam \m_dqm[1] .power_up = "low";

dffeas \m_cmd[2] (
	.clk(clk_clk),
	.d(\Selector20~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_cmd_2),
	.prn(vcc));
defparam \m_cmd[2] .is_wysiwyg = "true";
defparam \m_cmd[2] .power_up = "low";

dffeas \m_cmd[0] (
	.clk(clk_clk),
	.d(\Selector22~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_cmd_0),
	.prn(vcc));
defparam \m_cmd[0] .is_wysiwyg = "true";
defparam \m_cmd[0] .power_up = "low";

dffeas \m_data[0] (
	.clk(clk_clk),
	.d(\Selector112~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_0),
	.prn(vcc));
defparam \m_data[0] .is_wysiwyg = "true";
defparam \m_data[0] .power_up = "low";

dffeas \m_data[1] (
	.clk(clk_clk),
	.d(\Selector111~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_1),
	.prn(vcc));
defparam \m_data[1] .is_wysiwyg = "true";
defparam \m_data[1] .power_up = "low";

dffeas \m_data[2] (
	.clk(clk_clk),
	.d(\Selector110~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_2),
	.prn(vcc));
defparam \m_data[2] .is_wysiwyg = "true";
defparam \m_data[2] .power_up = "low";

dffeas \m_data[3] (
	.clk(clk_clk),
	.d(\Selector109~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_3),
	.prn(vcc));
defparam \m_data[3] .is_wysiwyg = "true";
defparam \m_data[3] .power_up = "low";

dffeas \m_data[4] (
	.clk(clk_clk),
	.d(\Selector108~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_4),
	.prn(vcc));
defparam \m_data[4] .is_wysiwyg = "true";
defparam \m_data[4] .power_up = "low";

dffeas \m_data[5] (
	.clk(clk_clk),
	.d(\Selector107~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_5),
	.prn(vcc));
defparam \m_data[5] .is_wysiwyg = "true";
defparam \m_data[5] .power_up = "low";

dffeas \m_data[6] (
	.clk(clk_clk),
	.d(\Selector106~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_6),
	.prn(vcc));
defparam \m_data[6] .is_wysiwyg = "true";
defparam \m_data[6] .power_up = "low";

dffeas \m_data[7] (
	.clk(clk_clk),
	.d(\Selector105~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_7),
	.prn(vcc));
defparam \m_data[7] .is_wysiwyg = "true";
defparam \m_data[7] .power_up = "low";

dffeas \m_data[8] (
	.clk(clk_clk),
	.d(\Selector104~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_8),
	.prn(vcc));
defparam \m_data[8] .is_wysiwyg = "true";
defparam \m_data[8] .power_up = "low";

dffeas \m_data[9] (
	.clk(clk_clk),
	.d(\Selector103~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_9),
	.prn(vcc));
defparam \m_data[9] .is_wysiwyg = "true";
defparam \m_data[9] .power_up = "low";

dffeas \m_data[10] (
	.clk(clk_clk),
	.d(\Selector102~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_10),
	.prn(vcc));
defparam \m_data[10] .is_wysiwyg = "true";
defparam \m_data[10] .power_up = "low";

dffeas \m_data[11] (
	.clk(clk_clk),
	.d(\Selector101~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_11),
	.prn(vcc));
defparam \m_data[11] .is_wysiwyg = "true";
defparam \m_data[11] .power_up = "low";

dffeas \m_data[12] (
	.clk(clk_clk),
	.d(\Selector100~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_12),
	.prn(vcc));
defparam \m_data[12] .is_wysiwyg = "true";
defparam \m_data[12] .power_up = "low";

dffeas \m_data[13] (
	.clk(clk_clk),
	.d(\Selector99~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_13),
	.prn(vcc));
defparam \m_data[13] .is_wysiwyg = "true";
defparam \m_data[13] .power_up = "low";

dffeas \m_data[14] (
	.clk(clk_clk),
	.d(\Selector98~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_14),
	.prn(vcc));
defparam \m_data[14] .is_wysiwyg = "true";
defparam \m_data[14] .power_up = "low";

dffeas \m_data[15] (
	.clk(clk_clk),
	.d(\Selector97~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(m_data_15),
	.prn(vcc));
defparam \m_data[15] .is_wysiwyg = "true";
defparam \m_data[15] .power_up = "low";

cyclone10lp_lcell_comb \Add0~0 (
	.dataa(\refresh_counter[0]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(\Add0~0_combout ),
	.cout(\Add0~1 ));
defparam \Add0~0 .lut_mask = 16'h55AA;
defparam \Add0~0 .sum_lutc_input = "datac";

dffeas \refresh_counter[0] (
	.clk(clk_clk),
	.d(\Add0~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[0]~q ),
	.prn(vcc));
defparam \refresh_counter[0] .is_wysiwyg = "true";
defparam \refresh_counter[0] .power_up = "low";

cyclone10lp_lcell_comb \Add0~2 (
	.dataa(\refresh_counter[1]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~1 ),
	.combout(\Add0~2_combout ),
	.cout(\Add0~3 ));
defparam \Add0~2 .lut_mask = 16'hA505;
defparam \Add0~2 .sum_lutc_input = "cin";

dffeas \refresh_counter[1] (
	.clk(clk_clk),
	.d(\Add0~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[1]~q ),
	.prn(vcc));
defparam \refresh_counter[1] .is_wysiwyg = "true";
defparam \refresh_counter[1] .power_up = "low";

cyclone10lp_lcell_comb \Add0~4 (
	.dataa(\refresh_counter[2]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~3 ),
	.combout(\Add0~4_combout ),
	.cout(\Add0~5 ));
defparam \Add0~4 .lut_mask = 16'h5AAF;
defparam \Add0~4 .sum_lutc_input = "cin";

dffeas \refresh_counter[2] (
	.clk(clk_clk),
	.d(\Add0~4_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[2]~q ),
	.prn(vcc));
defparam \refresh_counter[2] .is_wysiwyg = "true";
defparam \refresh_counter[2] .power_up = "low";

cyclone10lp_lcell_comb \Add0~6 (
	.dataa(\refresh_counter[3]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~5 ),
	.combout(\Add0~6_combout ),
	.cout(\Add0~7 ));
defparam \Add0~6 .lut_mask = 16'hA505;
defparam \Add0~6 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~7 (
	.dataa(\Add0~6_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~7_combout ),
	.cout());
defparam \refresh_counter~7 .lut_mask = 16'h00AA;
defparam \refresh_counter~7 .sum_lutc_input = "datac";

dffeas \refresh_counter[3] (
	.clk(clk_clk),
	.d(\refresh_counter~7_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[3]~q ),
	.prn(vcc));
defparam \refresh_counter[3] .is_wysiwyg = "true";
defparam \refresh_counter[3] .power_up = "low";

cyclone10lp_lcell_comb \Add0~8 (
	.dataa(\refresh_counter[4]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~7 ),
	.combout(\Add0~8_combout ),
	.cout(\Add0~9 ));
defparam \Add0~8 .lut_mask = 16'h5AAF;
defparam \Add0~8 .sum_lutc_input = "cin";

dffeas \refresh_counter[4] (
	.clk(clk_clk),
	.d(\Add0~8_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[4]~q ),
	.prn(vcc));
defparam \refresh_counter[4] .is_wysiwyg = "true";
defparam \refresh_counter[4] .power_up = "low";

cyclone10lp_lcell_comb \Add0~10 (
	.dataa(\refresh_counter[5]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~9 ),
	.combout(\Add0~10_combout ),
	.cout(\Add0~11 ));
defparam \Add0~10 .lut_mask = 16'h5A0A;
defparam \Add0~10 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~6 (
	.dataa(\Add0~10_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~6_combout ),
	.cout());
defparam \refresh_counter~6 .lut_mask = 16'hFF55;
defparam \refresh_counter~6 .sum_lutc_input = "datac";

dffeas \refresh_counter[5] (
	.clk(clk_clk),
	.d(\refresh_counter~6_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[5]~q ),
	.prn(vcc));
defparam \refresh_counter[5] .is_wysiwyg = "true";
defparam \refresh_counter[5] .power_up = "low";

cyclone10lp_lcell_comb \Add0~12 (
	.dataa(\refresh_counter[6]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~11 ),
	.combout(\Add0~12_combout ),
	.cout(\Add0~13 ));
defparam \Add0~12 .lut_mask = 16'hA55F;
defparam \Add0~12 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~5 (
	.dataa(\Add0~12_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~5_combout ),
	.cout());
defparam \refresh_counter~5 .lut_mask = 16'hFF55;
defparam \refresh_counter~5 .sum_lutc_input = "datac";

dffeas \refresh_counter[6] (
	.clk(clk_clk),
	.d(\refresh_counter~5_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[6]~q ),
	.prn(vcc));
defparam \refresh_counter[6] .is_wysiwyg = "true";
defparam \refresh_counter[6] .power_up = "low";

cyclone10lp_lcell_comb \Add0~14 (
	.dataa(\refresh_counter[7]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~13 ),
	.combout(\Add0~14_combout ),
	.cout(\Add0~15 ));
defparam \Add0~14 .lut_mask = 16'hA505;
defparam \Add0~14 .sum_lutc_input = "cin";

dffeas \refresh_counter[7] (
	.clk(clk_clk),
	.d(\Add0~14_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[7]~q ),
	.prn(vcc));
defparam \refresh_counter[7] .is_wysiwyg = "true";
defparam \refresh_counter[7] .power_up = "low";

cyclone10lp_lcell_comb \Add0~16 (
	.dataa(\refresh_counter[8]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~15 ),
	.combout(\Add0~16_combout ),
	.cout(\Add0~17 ));
defparam \Add0~16 .lut_mask = 16'h5AAF;
defparam \Add0~16 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~4 (
	.dataa(\Add0~16_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~4_combout ),
	.cout());
defparam \refresh_counter~4 .lut_mask = 16'h00AA;
defparam \refresh_counter~4 .sum_lutc_input = "datac";

dffeas \refresh_counter[8] (
	.clk(clk_clk),
	.d(\refresh_counter~4_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[8]~q ),
	.prn(vcc));
defparam \refresh_counter[8] .is_wysiwyg = "true";
defparam \refresh_counter[8] .power_up = "low";

cyclone10lp_lcell_comb \Add0~18 (
	.dataa(\refresh_counter[9]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~17 ),
	.combout(\Add0~18_combout ),
	.cout(\Add0~19 ));
defparam \Add0~18 .lut_mask = 16'hA505;
defparam \Add0~18 .sum_lutc_input = "cin";

dffeas \refresh_counter[9] (
	.clk(clk_clk),
	.d(\Add0~18_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[9]~q ),
	.prn(vcc));
defparam \refresh_counter[9] .is_wysiwyg = "true";
defparam \refresh_counter[9] .power_up = "low";

cyclone10lp_lcell_comb \Add0~20 (
	.dataa(\refresh_counter[10]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~19 ),
	.combout(\Add0~20_combout ),
	.cout(\Add0~21 ));
defparam \Add0~20 .lut_mask = 16'hA55F;
defparam \Add0~20 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter[10]~10 (
	.dataa(\Add0~20_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\refresh_counter[10]~10_combout ),
	.cout());
defparam \refresh_counter[10]~10 .lut_mask = 16'h5555;
defparam \refresh_counter[10]~10 .sum_lutc_input = "datac";

dffeas \refresh_counter[10] (
	.clk(clk_clk),
	.d(\refresh_counter[10]~10_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[10]~q ),
	.prn(vcc));
defparam \refresh_counter[10] .is_wysiwyg = "true";
defparam \refresh_counter[10] .power_up = "low";

cyclone10lp_lcell_comb \Add0~22 (
	.dataa(\refresh_counter[11]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~21 ),
	.combout(\Add0~22_combout ),
	.cout(\Add0~23 ));
defparam \Add0~22 .lut_mask = 16'hA505;
defparam \Add0~22 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~3 (
	.dataa(\Add0~22_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~3_combout ),
	.cout());
defparam \refresh_counter~3 .lut_mask = 16'h00AA;
defparam \refresh_counter~3 .sum_lutc_input = "datac";

dffeas \refresh_counter[11] (
	.clk(clk_clk),
	.d(\refresh_counter~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[11]~q ),
	.prn(vcc));
defparam \refresh_counter[11] .is_wysiwyg = "true";
defparam \refresh_counter[11] .power_up = "low";

cyclone10lp_lcell_comb \Add0~24 (
	.dataa(\refresh_counter[12]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~23 ),
	.combout(\Add0~24_combout ),
	.cout(\Add0~25 ));
defparam \Add0~24 .lut_mask = 16'hA55F;
defparam \Add0~24 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~1 (
	.dataa(\Add0~24_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~1_combout ),
	.cout());
defparam \refresh_counter~1 .lut_mask = 16'hFF55;
defparam \refresh_counter~1 .sum_lutc_input = "datac";

dffeas \refresh_counter[12] (
	.clk(clk_clk),
	.d(\refresh_counter~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[12]~q ),
	.prn(vcc));
defparam \refresh_counter[12] .is_wysiwyg = "true";
defparam \refresh_counter[12] .power_up = "low";

cyclone10lp_lcell_comb \Add0~26 (
	.dataa(\refresh_counter[13]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\Add0~25 ),
	.combout(\Add0~26_combout ),
	.cout(\Add0~27 ));
defparam \Add0~26 .lut_mask = 16'hA505;
defparam \Add0~26 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~2 (
	.dataa(\Add0~26_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~2_combout ),
	.cout());
defparam \refresh_counter~2 .lut_mask = 16'h00AA;
defparam \refresh_counter~2 .sum_lutc_input = "datac";

dffeas \refresh_counter[13] (
	.clk(clk_clk),
	.d(\refresh_counter~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[13]~q ),
	.prn(vcc));
defparam \refresh_counter[13] .is_wysiwyg = "true";
defparam \refresh_counter[13] .power_up = "low";

cyclone10lp_lcell_comb \Add0~28 (
	.dataa(\refresh_counter[14]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(\Add0~27 ),
	.combout(\Add0~28_combout ),
	.cout());
defparam \Add0~28 .lut_mask = 16'hA5A5;
defparam \Add0~28 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \refresh_counter~0 (
	.dataa(\Add0~28_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\Equal0~4_combout ),
	.cin(gnd),
	.combout(\refresh_counter~0_combout ),
	.cout());
defparam \refresh_counter~0 .lut_mask = 16'hFF55;
defparam \refresh_counter~0 .sum_lutc_input = "datac";

dffeas \refresh_counter[14] (
	.clk(clk_clk),
	.d(\refresh_counter~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_counter[14]~q ),
	.prn(vcc));
defparam \refresh_counter[14] .is_wysiwyg = "true";
defparam \refresh_counter[14] .power_up = "low";

cyclone10lp_lcell_comb \Equal0~0 (
	.dataa(\refresh_counter[14]~q ),
	.datab(\refresh_counter[12]~q ),
	.datac(\refresh_counter[13]~q ),
	.datad(\refresh_counter[11]~q ),
	.cin(gnd),
	.combout(\Equal0~0_combout ),
	.cout());
defparam \Equal0~0 .lut_mask = 16'h0008;
defparam \Equal0~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal0~1 (
	.dataa(\refresh_counter[10]~q ),
	.datab(\refresh_counter[9]~q ),
	.datac(\refresh_counter[8]~q ),
	.datad(\refresh_counter[7]~q ),
	.cin(gnd),
	.combout(\Equal0~1_combout ),
	.cout());
defparam \Equal0~1 .lut_mask = 16'h0002;
defparam \Equal0~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal0~2 (
	.dataa(\refresh_counter[6]~q ),
	.datab(\refresh_counter[5]~q ),
	.datac(\refresh_counter[4]~q ),
	.datad(\refresh_counter[3]~q ),
	.cin(gnd),
	.combout(\Equal0~2_combout ),
	.cout());
defparam \Equal0~2 .lut_mask = 16'h0008;
defparam \Equal0~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal0~3 (
	.dataa(gnd),
	.datab(\refresh_counter[2]~q ),
	.datac(\refresh_counter[1]~q ),
	.datad(\refresh_counter[0]~q ),
	.cin(gnd),
	.combout(\Equal0~3_combout ),
	.cout());
defparam \Equal0~3 .lut_mask = 16'h0003;
defparam \Equal0~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal0~4 (
	.dataa(\Equal0~0_combout ),
	.datab(\Equal0~1_combout ),
	.datac(\Equal0~2_combout ),
	.datad(\Equal0~3_combout ),
	.cin(gnd),
	.combout(\Equal0~4_combout ),
	.cout());
defparam \Equal0~4 .lut_mask = 16'h8000;
defparam \Equal0~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_next.000~0 (
	.dataa(\i_next.000~q ),
	.datab(\i_state.000~q ),
	.datac(\i_state.101~q ),
	.datad(\i_state.011~q ),
	.cin(gnd),
	.combout(\i_next.000~0_combout ),
	.cout());
defparam \i_next.000~0 .lut_mask = 16'hAAAE;
defparam \i_next.000~0 .sum_lutc_input = "datac";

dffeas \i_next.000 (
	.clk(clk_clk),
	.d(\i_next.000~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_next.000~q ),
	.prn(vcc));
defparam \i_next.000 .is_wysiwyg = "true";
defparam \i_next.000 .power_up = "low";

cyclone10lp_lcell_comb \Selector7~0 (
	.dataa(\i_count[2]~1_combout ),
	.datab(\i_state.000~q ),
	.datac(\Equal0~4_combout ),
	.datad(\i_next.000~q ),
	.cin(gnd),
	.combout(\Selector7~0_combout ),
	.cout());
defparam \Selector7~0 .lut_mask = 16'hFC54;
defparam \Selector7~0 .sum_lutc_input = "datac";

dffeas \i_state.000 (
	.clk(clk_clk),
	.d(\Selector7~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.000~q ),
	.prn(vcc));
defparam \i_state.000 .is_wysiwyg = "true";
defparam \i_state.000 .power_up = "low";

cyclone10lp_lcell_comb \Selector8~0 (
	.dataa(\Equal0~4_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\i_state.000~q ),
	.cin(gnd),
	.combout(\Selector8~0_combout ),
	.cout());
defparam \Selector8~0 .lut_mask = 16'h00AA;
defparam \Selector8~0 .sum_lutc_input = "datac";

dffeas \i_state.001 (
	.clk(clk_clk),
	.d(\Selector8~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.001~q ),
	.prn(vcc));
defparam \i_state.001 .is_wysiwyg = "true";
defparam \i_state.001 .power_up = "low";

cyclone10lp_lcell_comb \Selector6~0 (
	.dataa(\i_state.000~q ),
	.datab(gnd),
	.datac(\i_state.010~q ),
	.datad(\i_refs[0]~q ),
	.cin(gnd),
	.combout(\Selector6~0_combout ),
	.cout());
defparam \Selector6~0 .lut_mask = 16'h0AF0;
defparam \Selector6~0 .sum_lutc_input = "datac";

dffeas \i_refs[0] (
	.clk(clk_clk),
	.d(\Selector6~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(altera_reset_synchronizer_int_chain_out),
	.q(\i_refs[0]~q ),
	.prn(vcc));
defparam \i_refs[0] .is_wysiwyg = "true";
defparam \i_refs[0] .power_up = "low";

cyclone10lp_lcell_comb \Selector5~0 (
	.dataa(\i_state.000~q ),
	.datab(\i_state.010~q ),
	.datac(\i_refs[1]~q ),
	.datad(\i_refs[0]~q ),
	.cin(gnd),
	.combout(\Selector5~0_combout ),
	.cout());
defparam \Selector5~0 .lut_mask = 16'h2CE0;
defparam \Selector5~0 .sum_lutc_input = "datac";

dffeas \i_refs[1] (
	.clk(clk_clk),
	.d(\Selector5~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(altera_reset_synchronizer_int_chain_out),
	.q(\i_refs[1]~q ),
	.prn(vcc));
defparam \i_refs[1] .is_wysiwyg = "true";
defparam \i_refs[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector4~0 (
	.dataa(\i_state.010~q ),
	.datab(\i_refs[2]~q ),
	.datac(\i_refs[1]~q ),
	.datad(\i_refs[0]~q ),
	.cin(gnd),
	.combout(\Selector4~0_combout ),
	.cout());
defparam \Selector4~0 .lut_mask = 16'h2888;
defparam \Selector4~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector4~1 (
	.dataa(\Selector4~0_combout ),
	.datab(\i_state.000~q ),
	.datac(\i_refs[2]~q ),
	.datad(\i_state.010~q ),
	.cin(gnd),
	.combout(\Selector4~1_combout ),
	.cout());
defparam \Selector4~1 .lut_mask = 16'hAAEA;
defparam \Selector4~1 .sum_lutc_input = "datac";

dffeas \i_refs[2] (
	.clk(clk_clk),
	.d(\Selector4~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(altera_reset_synchronizer_int_chain_out),
	.q(\i_refs[2]~q ),
	.prn(vcc));
defparam \i_refs[2] .is_wysiwyg = "true";
defparam \i_refs[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector16~0 (
	.dataa(\i_state.010~q ),
	.datab(\i_refs[2]~q ),
	.datac(\i_refs[1]~q ),
	.datad(\i_refs[0]~q ),
	.cin(gnd),
	.combout(\Selector16~0_combout ),
	.cout());
defparam \Selector16~0 .lut_mask = 16'hA8AA;
defparam \Selector16~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_count[2]~2 (
	.dataa(\i_state.000~q ),
	.datab(gnd),
	.datac(\i_state.101~q ),
	.datad(\i_state.011~q ),
	.cin(gnd),
	.combout(\i_count[2]~2_combout ),
	.cout());
defparam \i_count[2]~2 .lut_mask = 16'h000A;
defparam \i_count[2]~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector16~1 (
	.dataa(\i_state.001~q ),
	.datab(\Selector16~0_combout ),
	.datac(\i_next.010~q ),
	.datad(\i_count[2]~2_combout ),
	.cin(gnd),
	.combout(\Selector16~1_combout ),
	.cout());
defparam \Selector16~1 .lut_mask = 16'hEEFE;
defparam \Selector16~1 .sum_lutc_input = "datac";

dffeas \i_next.010 (
	.clk(clk_clk),
	.d(\Selector16~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_next.010~q ),
	.prn(vcc));
defparam \i_next.010 .is_wysiwyg = "true";
defparam \i_next.010 .power_up = "low";

cyclone10lp_lcell_comb \i_count[0]~6 (
	.dataa(gnd),
	.datab(\i_state.011~q ),
	.datac(\i_state.010~q ),
	.datad(\i_count[0]~q ),
	.cin(gnd),
	.combout(\i_count[0]~6_combout ),
	.cout());
defparam \i_count[0]~6 .lut_mask = 16'h03CF;
defparam \i_count[0]~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_count[2]~3 (
	.dataa(\i_state.000~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(\i_state.101~q ),
	.cin(gnd),
	.combout(\i_count[2]~3_combout ),
	.cout());
defparam \i_count[2]~3 .lut_mask = 16'h00AA;
defparam \i_count[2]~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_count[0]~7 (
	.dataa(\i_count[0]~q ),
	.datab(\i_count[0]~6_combout ),
	.datac(\i_count[2]~3_combout ),
	.datad(\i_count[2]~1_combout ),
	.cin(gnd),
	.combout(\i_count[0]~7_combout ),
	.cout());
defparam \i_count[0]~7 .lut_mask = 16'hAACA;
defparam \i_count[0]~7 .sum_lutc_input = "datac";

dffeas \i_count[0] (
	.clk(clk_clk),
	.d(\i_count[0]~7_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_count[0]~q ),
	.prn(vcc));
defparam \i_count[0] .is_wysiwyg = "true";
defparam \i_count[0] .power_up = "low";

cyclone10lp_lcell_comb \Selector14~1 (
	.dataa(\i_state.011~q ),
	.datab(\i_count[1]~q ),
	.datac(\i_count[0]~q ),
	.datad(\i_count[2]~q ),
	.cin(gnd),
	.combout(\Selector14~1_combout ),
	.cout());
defparam \Selector14~1 .lut_mask = 16'h8280;
defparam \Selector14~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector14~0 (
	.dataa(gnd),
	.datab(gnd),
	.datac(\i_state.111~q ),
	.datad(\i_state.010~q ),
	.cin(gnd),
	.combout(\Selector14~0_combout ),
	.cout());
defparam \Selector14~0 .lut_mask = 16'h000F;
defparam \Selector14~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector14~2 (
	.dataa(\Selector14~1_combout ),
	.datab(\i_count[1]~q ),
	.datac(\i_count[2]~3_combout ),
	.datad(\Selector14~0_combout ),
	.cin(gnd),
	.combout(\Selector14~2_combout ),
	.cout());
defparam \Selector14~2 .lut_mask = 16'hAEFF;
defparam \Selector14~2 .sum_lutc_input = "datac";

dffeas \i_count[1] (
	.clk(clk_clk),
	.d(\Selector14~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_count[1]~q ),
	.prn(vcc));
defparam \i_count[1] .is_wysiwyg = "true";
defparam \i_count[1] .power_up = "low";

cyclone10lp_lcell_comb \i_count[2]~4 (
	.dataa(\i_state.011~q ),
	.datab(\i_count[1]~q ),
	.datac(\i_count[0]~q ),
	.datad(\i_count[2]~3_combout ),
	.cin(gnd),
	.combout(\i_count[2]~4_combout ),
	.cout());
defparam \i_count[2]~4 .lut_mask = 16'hA8FF;
defparam \i_count[2]~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_count[2]~5 (
	.dataa(\i_state.010~q ),
	.datab(\i_count[2]~q ),
	.datac(\i_count[2]~4_combout ),
	.datad(\i_count[2]~2_combout ),
	.cin(gnd),
	.combout(\i_count[2]~5_combout ),
	.cout());
defparam \i_count[2]~5 .lut_mask = 16'hEAC0;
defparam \i_count[2]~5 .sum_lutc_input = "datac";

dffeas \i_count[2] (
	.clk(clk_clk),
	.d(\i_count[2]~5_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_count[2]~q ),
	.prn(vcc));
defparam \i_count[2] .is_wysiwyg = "true";
defparam \i_count[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector9~0 (
	.dataa(\i_state.011~q ),
	.datab(\i_next.010~q ),
	.datac(\i_count[2]~q ),
	.datad(\i_count[1]~q ),
	.cin(gnd),
	.combout(\Selector9~0_combout ),
	.cout());
defparam \Selector9~0 .lut_mask = 16'h0008;
defparam \Selector9~0 .sum_lutc_input = "datac";

dffeas \i_state.010 (
	.clk(clk_clk),
	.d(\Selector9~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.010~q ),
	.prn(vcc));
defparam \i_state.010 .is_wysiwyg = "true";
defparam \i_state.010 .power_up = "low";

cyclone10lp_lcell_comb \Selector18~0 (
	.dataa(\i_state.010~q ),
	.datab(\i_refs[0]~q ),
	.datac(\i_refs[2]~q ),
	.datad(\i_refs[1]~q ),
	.cin(gnd),
	.combout(\Selector18~0_combout ),
	.cout());
defparam \Selector18~0 .lut_mask = 16'h0008;
defparam \Selector18~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector18~1 (
	.dataa(\Selector18~0_combout ),
	.datab(\i_next.111~q ),
	.datac(gnd),
	.datad(\i_count[2]~2_combout ),
	.cin(gnd),
	.combout(\Selector18~1_combout ),
	.cout());
defparam \Selector18~1 .lut_mask = 16'hAAEE;
defparam \Selector18~1 .sum_lutc_input = "datac";

dffeas \i_next.111 (
	.clk(clk_clk),
	.d(\Selector18~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_next.111~q ),
	.prn(vcc));
defparam \i_next.111 .is_wysiwyg = "true";
defparam \i_next.111 .power_up = "low";

cyclone10lp_lcell_comb \Selector12~0 (
	.dataa(\i_state.011~q ),
	.datab(\i_next.111~q ),
	.datac(\i_count[2]~q ),
	.datad(\i_count[1]~q ),
	.cin(gnd),
	.combout(\Selector12~0_combout ),
	.cout());
defparam \Selector12~0 .lut_mask = 16'h0008;
defparam \Selector12~0 .sum_lutc_input = "datac";

dffeas \i_state.111 (
	.clk(clk_clk),
	.d(\Selector12~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.111~q ),
	.prn(vcc));
defparam \i_state.111 .is_wysiwyg = "true";
defparam \i_state.111 .power_up = "low";

cyclone10lp_lcell_comb \Selector10~2 (
	.dataa(\i_state.001~q ),
	.datab(\i_state.011~q ),
	.datac(\i_count[2]~q ),
	.datad(\i_count[1]~q ),
	.cin(gnd),
	.combout(\Selector10~2_combout ),
	.cout());
defparam \Selector10~2 .lut_mask = 16'hEEEA;
defparam \Selector10~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector10~3 (
	.dataa(\i_state.111~q ),
	.datab(\i_state.010~q ),
	.datac(\Selector10~2_combout ),
	.datad(gnd),
	.cin(gnd),
	.combout(\Selector10~3_combout ),
	.cout());
defparam \Selector10~3 .lut_mask = 16'hFEFE;
defparam \Selector10~3 .sum_lutc_input = "datac";

dffeas \i_state.011 (
	.clk(clk_clk),
	.d(\Selector10~3_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.011~q ),
	.prn(vcc));
defparam \i_state.011 .is_wysiwyg = "true";
defparam \i_state.011 .power_up = "low";

cyclone10lp_lcell_comb \i_count[2]~1 (
	.dataa(\i_state.011~q ),
	.datab(gnd),
	.datac(\i_count[2]~q ),
	.datad(\i_count[1]~q ),
	.cin(gnd),
	.combout(\i_count[2]~1_combout ),
	.cout());
defparam \i_count[2]~1 .lut_mask = 16'h000A;
defparam \i_count[2]~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector17~0 (
	.dataa(\i_state.111~q ),
	.datab(\i_next.101~q ),
	.datac(gnd),
	.datad(\i_count[2]~2_combout ),
	.cin(gnd),
	.combout(\Selector17~0_combout ),
	.cout());
defparam \Selector17~0 .lut_mask = 16'hAAEE;
defparam \Selector17~0 .sum_lutc_input = "datac";

dffeas \i_next.101 (
	.clk(clk_clk),
	.d(\Selector17~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_next.101~q ),
	.prn(vcc));
defparam \i_next.101 .is_wysiwyg = "true";
defparam \i_next.101 .power_up = "low";

cyclone10lp_lcell_comb \i_state.101~0 (
	.dataa(\i_state.101~q ),
	.datab(\i_count[2]~1_combout ),
	.datac(\i_next.101~q ),
	.datad(gnd),
	.cin(gnd),
	.combout(\i_state.101~0_combout ),
	.cout());
defparam \i_state.101~0 .lut_mask = 16'hEAEA;
defparam \i_state.101~0 .sum_lutc_input = "datac";

dffeas \i_state.101 (
	.clk(clk_clk),
	.d(\i_state.101~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_state.101~q ),
	.prn(vcc));
defparam \i_state.101 .is_wysiwyg = "true";
defparam \i_state.101 .power_up = "low";

cyclone10lp_lcell_comb \init_done~0 (
	.dataa(\init_done~q ),
	.datab(\i_state.101~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\init_done~0_combout ),
	.cout());
defparam \init_done~0 .lut_mask = 16'hEEEE;
defparam \init_done~0 .sum_lutc_input = "datac";

dffeas init_done(
	.clk(clk_clk),
	.d(\init_done~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\init_done~q ),
	.prn(vcc));
defparam init_done.is_wysiwyg = "true";
defparam init_done.power_up = "low";

cyclone10lp_lcell_comb \Selector24~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.datac(\refresh_request~q ),
	.datad(\init_done~q ),
	.cin(gnd),
	.combout(\Selector24~1_combout ),
	.cout());
defparam \Selector24~1 .lut_mask = 16'h01FF;
defparam \Selector24~1 .sum_lutc_input = "datac";

dffeas active_rnw(
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_rnw~q ),
	.prn(vcc));
defparam active_rnw.is_wysiwyg = "true";
defparam active_rnw.power_up = "low";

dffeas \active_addr[8] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[26]~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[8]~q ),
	.prn(vcc));
defparam \active_addr[8] .is_wysiwyg = "true";
defparam \active_addr[8] .power_up = "low";

cyclone10lp_lcell_comb \pending~0 (
	.dataa(\active_rnw~q ),
	.datab(\active_addr[8]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[26]~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ),
	.cin(gnd),
	.combout(\pending~0_combout ),
	.cout());
defparam \pending~0 .lut_mask = 16'h8241;
defparam \pending~0 .sum_lutc_input = "datac";

dffeas \active_addr[21] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[39]~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[21]~q ),
	.prn(vcc));
defparam \active_addr[21] .is_wysiwyg = "true";
defparam \active_addr[21] .power_up = "low";

cyclone10lp_lcell_comb \pending~1 (
	.dataa(\active_addr[21]~q ),
	.datab(\active_addr[9]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[27]~2_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[39]~3_combout ),
	.cin(gnd),
	.combout(\pending~1_combout ),
	.cout());
defparam \pending~1 .lut_mask = 16'h8241;
defparam \pending~1 .sum_lutc_input = "datac";

dffeas \active_addr[10] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[28]~5_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[10]~q ),
	.prn(vcc));
defparam \active_addr[10] .is_wysiwyg = "true";
defparam \active_addr[10] .power_up = "low";

dffeas \active_addr[11] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[29]~4_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[11]~q ),
	.prn(vcc));
defparam \active_addr[11] .is_wysiwyg = "true";
defparam \active_addr[11] .power_up = "low";

cyclone10lp_lcell_comb \pending~2 (
	.dataa(\active_addr[10]~q ),
	.datab(\active_addr[11]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[29]~4_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[28]~5_combout ),
	.cin(gnd),
	.combout(\pending~2_combout ),
	.cout());
defparam \pending~2 .lut_mask = 16'h8241;
defparam \pending~2 .sum_lutc_input = "datac";

dffeas \active_addr[12] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[30]~7_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[12]~q ),
	.prn(vcc));
defparam \active_addr[12] .is_wysiwyg = "true";
defparam \active_addr[12] .power_up = "low";

dffeas \active_addr[13] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[31]~6_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[13]~q ),
	.prn(vcc));
defparam \active_addr[13] .is_wysiwyg = "true";
defparam \active_addr[13] .power_up = "low";

cyclone10lp_lcell_comb \pending~3 (
	.dataa(\active_addr[12]~q ),
	.datab(\active_addr[13]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[31]~6_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[30]~7_combout ),
	.cin(gnd),
	.combout(\pending~3_combout ),
	.cout());
defparam \pending~3 .lut_mask = 16'h8241;
defparam \pending~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \pending~4 (
	.dataa(\pending~0_combout ),
	.datab(\pending~1_combout ),
	.datac(\pending~2_combout ),
	.datad(\pending~3_combout ),
	.cin(gnd),
	.combout(\pending~4_combout ),
	.cout());
defparam \pending~4 .lut_mask = 16'h8000;
defparam \pending~4 .sum_lutc_input = "datac";

dffeas \active_addr[14] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[32]~9_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[14]~q ),
	.prn(vcc));
defparam \active_addr[14] .is_wysiwyg = "true";
defparam \active_addr[14] .power_up = "low";

dffeas \active_addr[15] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[33]~8_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[15]~q ),
	.prn(vcc));
defparam \active_addr[15] .is_wysiwyg = "true";
defparam \active_addr[15] .power_up = "low";

cyclone10lp_lcell_comb \pending~5 (
	.dataa(\active_addr[14]~q ),
	.datab(\active_addr[15]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[33]~8_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[32]~9_combout ),
	.cin(gnd),
	.combout(\pending~5_combout ),
	.cout());
defparam \pending~5 .lut_mask = 16'h8241;
defparam \pending~5 .sum_lutc_input = "datac";

dffeas \active_addr[16] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[34]~11_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[16]~q ),
	.prn(vcc));
defparam \active_addr[16] .is_wysiwyg = "true";
defparam \active_addr[16] .power_up = "low";

dffeas \active_addr[17] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[35]~10_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[17]~q ),
	.prn(vcc));
defparam \active_addr[17] .is_wysiwyg = "true";
defparam \active_addr[17] .power_up = "low";

cyclone10lp_lcell_comb \pending~6 (
	.dataa(\active_addr[16]~q ),
	.datab(\active_addr[17]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[35]~10_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[34]~11_combout ),
	.cin(gnd),
	.combout(\pending~6_combout ),
	.cout());
defparam \pending~6 .lut_mask = 16'h8241;
defparam \pending~6 .sum_lutc_input = "datac";

dffeas \active_addr[18] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[36]~13_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[18]~q ),
	.prn(vcc));
defparam \active_addr[18] .is_wysiwyg = "true";
defparam \active_addr[18] .power_up = "low";

dffeas \active_addr[19] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[37]~12_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[19]~q ),
	.prn(vcc));
defparam \active_addr[19] .is_wysiwyg = "true";
defparam \active_addr[19] .power_up = "low";

cyclone10lp_lcell_comb \pending~7 (
	.dataa(\active_addr[18]~q ),
	.datab(\active_addr[19]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[37]~12_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[36]~13_combout ),
	.cin(gnd),
	.combout(\pending~7_combout ),
	.cout());
defparam \pending~7 .lut_mask = 16'h8241;
defparam \pending~7 .sum_lutc_input = "datac";

dffeas \active_addr[20] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[38]~14_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[20]~q ),
	.prn(vcc));
defparam \active_addr[20] .is_wysiwyg = "true";
defparam \active_addr[20] .power_up = "low";

cyclone10lp_lcell_comb \Selector30~4 (
	.dataa(\init_done~q ),
	.datab(\refresh_request~q ),
	.datac(gnd),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector30~4_combout ),
	.cout());
defparam \Selector30~4 .lut_mask = 16'h0088;
defparam \Selector30~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \active_cs_n~0 (
	.dataa(\m_state.000000001~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.datad(\init_done~q ),
	.cin(gnd),
	.combout(\active_cs_n~0_combout ),
	.cout());
defparam \active_cs_n~0 .lut_mask = 16'hABFF;
defparam \active_cs_n~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \active_cs_n~1 (
	.dataa(\active_cs_n~q ),
	.datab(\Selector30~4_combout ),
	.datac(altera_reset_synchronizer_int_chain_out),
	.datad(\active_cs_n~0_combout ),
	.cin(gnd),
	.combout(\active_cs_n~1_combout ),
	.cout());
defparam \active_cs_n~1 .lut_mask = 16'hEACA;
defparam \active_cs_n~1 .sum_lutc_input = "datac";

dffeas active_cs_n(
	.clk(clk_clk),
	.d(\active_cs_n~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\active_cs_n~q ),
	.prn(vcc));
defparam active_cs_n.is_wysiwyg = "true";
defparam active_cs_n.power_up = "low";

cyclone10lp_lcell_comb \pending~8 (
	.dataa(\active_addr[20]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[38]~14_combout ),
	.datac(gnd),
	.datad(\active_cs_n~q ),
	.cin(gnd),
	.combout(\pending~8_combout ),
	.cout());
defparam \pending~8 .lut_mask = 16'h0099;
defparam \pending~8 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \pending~9 (
	.dataa(\pending~5_combout ),
	.datab(\pending~6_combout ),
	.datac(\pending~7_combout ),
	.datad(\pending~8_combout ),
	.cin(gnd),
	.combout(\pending~9_combout ),
	.cout());
defparam \pending~9 .lut_mask = 16'h8000;
defparam \pending~9 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector41~4 (
	.dataa(\pending~4_combout ),
	.datab(\pending~9_combout ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.cin(gnd),
	.combout(\Selector41~4_combout ),
	.cout());
defparam \Selector41~4 .lut_mask = 16'h8880;
defparam \Selector41~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector41~5 (
	.dataa(\init_done~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector41~5_combout ),
	.cout());
defparam \Selector41~5 .lut_mask = 16'h00AA;
defparam \Selector41~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector41~8 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.datac(\Selector41~5_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector41~8_combout ),
	.cout());
defparam \Selector41~8 .lut_mask = 16'h00E0;
defparam \Selector41~8 .sum_lutc_input = "datac";

dffeas \m_state.000000010 (
	.clk(clk_clk),
	.d(\Selector41~8_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.000000010~q ),
	.prn(vcc));
defparam \m_state.000000010 .is_wysiwyg = "true";
defparam \m_state.000000010 .power_up = "low";

cyclone10lp_lcell_comb \m_next~17 (
	.dataa(\pending~4_combout ),
	.datab(\pending~9_combout ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\m_next~17_combout ),
	.cout());
defparam \m_next~17 .lut_mask = 16'h008F;
defparam \m_next~17 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[1]~2 (
	.dataa(gnd),
	.datab(\m_state.100000000~q ),
	.datac(\m_next~17_combout ),
	.datad(\m_state.001000000~q ),
	.cin(gnd),
	.combout(\m_count[1]~2_combout ),
	.cout());
defparam \m_count[1]~2 .lut_mask = 16'h003F;
defparam \m_count[1]~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector36~0 (
	.dataa(\Selector41~5_combout ),
	.datab(\m_state.000000100~q ),
	.datac(\m_state.000100000~q ),
	.datad(\m_count[1]~2_combout ),
	.cin(gnd),
	.combout(\Selector36~0_combout ),
	.cout());
defparam \Selector36~0 .lut_mask = 16'hFEFF;
defparam \Selector36~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~0 (
	.dataa(\Selector24~0_combout ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datac(\refresh_request~q ),
	.datad(\m_state.100000000~q ),
	.cin(gnd),
	.combout(\Selector27~0_combout ),
	.cout());
defparam \Selector27~0 .lut_mask = 16'h02AA;
defparam \Selector27~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector35~0 (
	.dataa(\Selector34~1_combout ),
	.datab(gnd),
	.datac(gnd),
	.datad(\active_rnw~q ),
	.cin(gnd),
	.combout(\Selector35~0_combout ),
	.cout());
defparam \Selector35~0 .lut_mask = 16'h00AA;
defparam \Selector35~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~3 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.cin(gnd),
	.combout(\Selector34~3_combout ),
	.cout());
defparam \Selector34~3 .lut_mask = 16'hEEE0;
defparam \Selector34~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~4 (
	.dataa(\refresh_request~q ),
	.datab(\pending~4_combout ),
	.datac(\pending~9_combout ),
	.datad(\Selector34~3_combout ),
	.cin(gnd),
	.combout(\Selector34~4_combout ),
	.cout());
defparam \Selector34~4 .lut_mask = 16'h8000;
defparam \Selector34~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \WideOr8~1 (
	.dataa(gnd),
	.datab(gnd),
	.datac(\m_state.000000010~q ),
	.datad(\m_state.010000000~q ),
	.cin(gnd),
	.combout(\WideOr8~1_combout ),
	.cout());
defparam \WideOr8~1 .lut_mask = 16'h000F;
defparam \WideOr8~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~5 (
	.dataa(\refresh_request~q ),
	.datab(\init_done~q ),
	.datac(\m_state.000000001~q ),
	.datad(\WideOr8~1_combout ),
	.cin(gnd),
	.combout(\Selector34~5_combout ),
	.cout());
defparam \Selector34~5 .lut_mask = 16'h0BFF;
defparam \Selector34~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~6 (
	.dataa(\Selector34~4_combout ),
	.datab(\Selector34~5_combout ),
	.datac(\m_state.100000000~q ),
	.datad(\m_next~17_combout ),
	.cin(gnd),
	.combout(\Selector34~6_combout ),
	.cout());
defparam \Selector34~6 .lut_mask = 16'hEEFE;
defparam \Selector34~6 .sum_lutc_input = "datac";

dffeas \m_next.000010000 (
	.clk(clk_clk),
	.d(\Selector35~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\Selector34~6_combout ),
	.q(\m_next.000010000~q ),
	.prn(vcc));
defparam \m_next.000010000 .is_wysiwyg = "true";
defparam \m_next.000010000 .power_up = "low";

cyclone10lp_lcell_comb \pending~10 (
	.dataa(\pending~4_combout ),
	.datab(\pending~9_combout ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\pending~10_combout ),
	.cout());
defparam \pending~10 .lut_mask = 16'h8888;
defparam \pending~10 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datab(\pending~10_combout ),
	.datac(\m_state.100000000~q ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector27~1_combout ),
	.cout());
defparam \Selector27~1 .lut_mask = 16'h0080;
defparam \Selector27~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector28~0 (
	.dataa(\Selector27~0_combout ),
	.datab(\m_next.000010000~q ),
	.datac(\Selector27~1_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ),
	.cin(gnd),
	.combout(\Selector28~0_combout ),
	.cout());
defparam \Selector28~0 .lut_mask = 16'h88F8;
defparam \Selector28~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~3 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datab(\refresh_request~q ),
	.datac(\init_done~q ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector27~3_combout ),
	.cout());
defparam \Selector27~3 .lut_mask = 16'h00EF;
defparam \Selector27~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~4 (
	.dataa(\Selector24~0_combout ),
	.datab(\m_state.100000000~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector27~4_combout ),
	.cout());
defparam \Selector27~4 .lut_mask = 16'hEEEA;
defparam \Selector27~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~5 (
	.dataa(\m_state.001000000~q ),
	.datab(\Selector27~3_combout ),
	.datac(\Selector27~4_combout ),
	.datad(\WideOr8~1_combout ),
	.cin(gnd),
	.combout(\Selector27~5_combout ),
	.cout());
defparam \Selector27~5 .lut_mask = 16'hFEFF;
defparam \Selector27~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~6 (
	.dataa(\m_state.000000001~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datac(\pending~4_combout ),
	.datad(\pending~9_combout ),
	.cin(gnd),
	.combout(\Selector27~6_combout ),
	.cout());
defparam \Selector27~6 .lut_mask = 16'h2AAA;
defparam \Selector27~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector27~7 (
	.dataa(\Selector27~5_combout ),
	.datab(\refresh_request~q ),
	.datac(\Selector27~6_combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector27~7_combout ),
	.cout());
defparam \Selector27~7 .lut_mask = 16'hAAFE;
defparam \Selector27~7 .sum_lutc_input = "datac";

dffeas \m_state.000010000 (
	.clk(clk_clk),
	.d(\Selector28~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\Selector27~7_combout ),
	.q(\m_state.000010000~q ),
	.prn(vcc));
defparam \m_state.000010000 .is_wysiwyg = "true";
defparam \m_state.000010000 .power_up = "low";

cyclone10lp_lcell_comb \m_count[1]~11 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\Selector41~4_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\m_count[1]~11_combout ),
	.cout());
defparam \m_count[1]~11 .lut_mask = 16'h0EEE;
defparam \m_count[1]~11 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector36~1 (
	.dataa(\Selector30~4_combout ),
	.datab(\m_next.010000000~q ),
	.datac(\Selector36~0_combout ),
	.datad(\m_count[1]~11_combout ),
	.cin(gnd),
	.combout(\Selector36~1_combout ),
	.cout());
defparam \Selector36~1 .lut_mask = 16'hEEEA;
defparam \Selector36~1 .sum_lutc_input = "datac";

dffeas \m_next.010000000 (
	.clk(clk_clk),
	.d(\Selector36~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_next.010000000~q ),
	.prn(vcc));
defparam \m_next.010000000 .is_wysiwyg = "true";
defparam \m_next.010000000 .power_up = "low";

cyclone10lp_lcell_comb \m_count[1]~3 (
	.dataa(gnd),
	.datab(\init_done~q ),
	.datac(\refresh_request~q ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\m_count[1]~3_combout ),
	.cout());
defparam \m_count[1]~3 .lut_mask = 16'h003F;
defparam \m_count[1]~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[1]~4 (
	.dataa(\Selector24~0_combout ),
	.datab(\m_count[1]~11_combout ),
	.datac(\m_count[1]~3_combout ),
	.datad(\m_count[1]~2_combout ),
	.cin(gnd),
	.combout(\m_count[1]~4_combout ),
	.cout());
defparam \m_count[1]~4 .lut_mask = 16'hFEFF;
defparam \m_count[1]~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[1]~6 (
	.dataa(\WideOr9~0_combout ),
	.datab(\m_state.000000001~q ),
	.datac(\WideOr8~1_combout ),
	.datad(\Selector30~5_combout ),
	.cin(gnd),
	.combout(\m_count[1]~6_combout ),
	.cout());
defparam \m_count[1]~6 .lut_mask = 16'h0080;
defparam \m_count[1]~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[0]~9 (
	.dataa(\Selector30~5_combout ),
	.datab(\WideOr8~1_combout ),
	.datac(\m_state.000000001~q ),
	.datad(\m_state.100000000~q ),
	.cin(gnd),
	.combout(\m_count[0]~9_combout ),
	.cout());
defparam \m_count[0]~9 .lut_mask = 16'hFFAE;
defparam \m_count[0]~9 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[0]~10 (
	.dataa(\m_count[1]~6_combout ),
	.datab(\m_count[0]~q ),
	.datac(\m_count[1]~4_combout ),
	.datad(\m_count[0]~9_combout ),
	.cin(gnd),
	.combout(\m_count[0]~10_combout ),
	.cout());
defparam \m_count[0]~10 .lut_mask = 16'hCFC2;
defparam \m_count[0]~10 .sum_lutc_input = "datac";

dffeas \m_count[0] (
	.clk(clk_clk),
	.d(\m_count[0]~10_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_count[0]~q ),
	.prn(vcc));
defparam \m_count[0] .is_wysiwyg = "true";
defparam \m_count[0] .power_up = "low";

cyclone10lp_lcell_comb \m_count[1]~5 (
	.dataa(\m_state.000000001~q ),
	.datab(\m_count[2]~q ),
	.datac(\m_count[1]~q ),
	.datad(\m_state.000100000~q ),
	.cin(gnd),
	.combout(\m_count[1]~5_combout ),
	.cout());
defparam \m_count[1]~5 .lut_mask = 16'hA8AA;
defparam \m_count[1]~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[1]~7 (
	.dataa(\m_count[1]~q ),
	.datab(\m_count[0]~q ),
	.datac(\m_count[1]~5_combout ),
	.datad(\m_count[1]~6_combout ),
	.cin(gnd),
	.combout(\m_count[1]~7_combout ),
	.cout());
defparam \m_count[1]~7 .lut_mask = 16'h99F0;
defparam \m_count[1]~7 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_count[1]~8 (
	.dataa(\m_count[1]~q ),
	.datab(\m_count[1]~4_combout ),
	.datac(\m_count[1]~7_combout ),
	.datad(\m_state.100000000~q ),
	.cin(gnd),
	.combout(\m_count[1]~8_combout ),
	.cout());
defparam \m_count[1]~8 .lut_mask = 16'h88B8;
defparam \m_count[1]~8 .sum_lutc_input = "datac";

dffeas \m_count[1] (
	.clk(clk_clk),
	.d(\m_count[1]~8_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_count[1]~q ),
	.prn(vcc));
defparam \m_count[1] .is_wysiwyg = "true";
defparam \m_count[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector37~0 (
	.dataa(\m_state.000000100~q ),
	.datab(\m_state.000100000~q ),
	.datac(\m_count[1]~q ),
	.datad(\m_count[0]~q ),
	.cin(gnd),
	.combout(\Selector37~0_combout ),
	.cout());
defparam \Selector37~0 .lut_mask = 16'hEEE0;
defparam \Selector37~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector37~1 (
	.dataa(\m_state.010000000~q ),
	.datab(\m_count[2]~q ),
	.datac(\Selector37~0_combout ),
	.datad(\m_count[1]~3_combout ),
	.cin(gnd),
	.combout(\Selector37~1_combout ),
	.cout());
defparam \Selector37~1 .lut_mask = 16'hEEEA;
defparam \Selector37~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector37~2 (
	.dataa(\Selector37~1_combout ),
	.datab(\m_count[2]~q ),
	.datac(\m_count[1]~11_combout ),
	.datad(\m_count[1]~2_combout ),
	.cin(gnd),
	.combout(\Selector37~2_combout ),
	.cout());
defparam \Selector37~2 .lut_mask = 16'hEAEE;
defparam \Selector37~2 .sum_lutc_input = "datac";

dffeas \m_count[2] (
	.clk(clk_clk),
	.d(\Selector37~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_count[2]~q ),
	.prn(vcc));
defparam \m_count[2] .is_wysiwyg = "true";
defparam \m_count[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector31~0 (
	.dataa(\m_state.000000100~q ),
	.datab(\m_next.010000000~q ),
	.datac(\m_count[2]~q ),
	.datad(\m_count[1]~q ),
	.cin(gnd),
	.combout(\Selector31~0_combout ),
	.cout());
defparam \Selector31~0 .lut_mask = 16'h0008;
defparam \Selector31~0 .sum_lutc_input = "datac";

dffeas \m_state.010000000 (
	.clk(clk_clk),
	.d(\Selector31~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.010000000~q ),
	.prn(vcc));
defparam \m_state.010000000 .is_wysiwyg = "true";
defparam \m_state.010000000 .power_up = "low";

cyclone10lp_lcell_comb \Selector34~1 (
	.dataa(\m_state.000000010~q ),
	.datab(\m_next~17_combout ),
	.datac(\m_state.100000000~q ),
	.datad(\m_state.010000000~q ),
	.cin(gnd),
	.combout(\Selector34~1_combout ),
	.cout());
defparam \Selector34~1 .lut_mask = 16'h008A;
defparam \Selector34~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~2 (
	.dataa(\active_rnw~q ),
	.datab(\Selector34~1_combout ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\Selector34~2_combout ),
	.cout());
defparam \Selector34~2 .lut_mask = 16'h8888;
defparam \Selector34~2 .sum_lutc_input = "datac";

dffeas \m_next.000001000 (
	.clk(clk_clk),
	.d(\Selector34~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\Selector34~6_combout ),
	.q(\m_next.000001000~q ),
	.prn(vcc));
defparam \m_next.000001000 .is_wysiwyg = "true";
defparam \m_next.000001000 .power_up = "low";

cyclone10lp_lcell_comb \Selector27~2 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[40]~1_combout ),
	.datab(\m_next.000001000~q ),
	.datac(\Selector27~0_combout ),
	.datad(\Selector27~1_combout ),
	.cin(gnd),
	.combout(\Selector27~2_combout ),
	.cout());
defparam \Selector27~2 .lut_mask = 16'hEAC0;
defparam \Selector27~2 .sum_lutc_input = "datac";

dffeas \m_state.000001000 (
	.clk(clk_clk),
	.d(\Selector27~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\Selector27~7_combout ),
	.q(\m_state.000001000~q ),
	.prn(vcc));
defparam \m_state.000001000 .is_wysiwyg = "true";
defparam \m_state.000001000 .power_up = "low";

cyclone10lp_lcell_comb \WideOr9~0 (
	.dataa(gnd),
	.datab(gnd),
	.datac(\m_state.000001000~q ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\WideOr9~0_combout ),
	.cout());
defparam \WideOr9~0 .lut_mask = 16'h000F;
defparam \WideOr9~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector32~0 (
	.dataa(\m_state.100000000~q ),
	.datab(\refresh_request~q ),
	.datac(\Selector41~4_combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector32~0_combout ),
	.cout());
defparam \Selector32~0 .lut_mask = 16'h002F;
defparam \Selector32~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector32~1 (
	.dataa(\Selector32~0_combout ),
	.datab(\m_state.100000000~q ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector32~1_combout ),
	.cout());
defparam \Selector32~1 .lut_mask = 16'hAAAE;
defparam \Selector32~1 .sum_lutc_input = "datac";

dffeas \m_state.100000000 (
	.clk(clk_clk),
	.d(\Selector32~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.100000000~q ),
	.prn(vcc));
defparam \m_state.100000000 .is_wysiwyg = "true";
defparam \m_state.100000000 .power_up = "low";

cyclone10lp_lcell_comb \Selector29~0 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datab(\m_state.100000000~q ),
	.datac(\pending~10_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector29~0_combout ),
	.cout());
defparam \Selector29~0 .lut_mask = 16'h0008;
defparam \Selector29~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector29~1 (
	.dataa(\Selector29~0_combout ),
	.datab(\m_state.000100000~q ),
	.datac(\m_count[2]~q ),
	.datad(\m_count[1]~q ),
	.cin(gnd),
	.combout(\Selector29~1_combout ),
	.cout());
defparam \Selector29~1 .lut_mask = 16'hEEEA;
defparam \Selector29~1 .sum_lutc_input = "datac";

dffeas \m_state.000100000 (
	.clk(clk_clk),
	.d(\Selector29~1_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.000100000~q ),
	.prn(vcc));
defparam \m_state.000100000 .is_wysiwyg = "true";
defparam \m_state.000100000 .power_up = "low";

cyclone10lp_lcell_comb \Selector30~5 (
	.dataa(\m_state.000100000~q ),
	.datab(gnd),
	.datac(\m_count[2]~q ),
	.datad(\m_count[1]~q ),
	.cin(gnd),
	.combout(\Selector30~5_combout ),
	.cout());
defparam \Selector30~5 .lut_mask = 16'h000A;
defparam \Selector30~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector30~6 (
	.dataa(\init_done~q ),
	.datab(\refresh_request~q ),
	.datac(\m_state.000000001~q ),
	.datad(\Selector30~5_combout ),
	.cin(gnd),
	.combout(\Selector30~6_combout ),
	.cout());
defparam \Selector30~6 .lut_mask = 16'hFF08;
defparam \Selector30~6 .sum_lutc_input = "datac";

dffeas \m_state.001000000 (
	.clk(clk_clk),
	.d(\Selector30~6_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.001000000~q ),
	.prn(vcc));
defparam \m_state.001000000 .is_wysiwyg = "true";
defparam \m_state.001000000 .power_up = "low";

cyclone10lp_lcell_comb \Selector26~0 (
	.dataa(\m_state.001000000~q ),
	.datab(\m_state.100000000~q ),
	.datac(\refresh_request~q ),
	.datad(\WideOr8~1_combout ),
	.cin(gnd),
	.combout(\Selector26~0_combout ),
	.cout());
defparam \Selector26~0 .lut_mask = 16'hEAFF;
defparam \Selector26~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector26~1 (
	.dataa(\Selector26~0_combout ),
	.datab(\m_state.000000100~q ),
	.datac(\m_count[2]~q ),
	.datad(\m_count[1]~q ),
	.cin(gnd),
	.combout(\Selector26~1_combout ),
	.cout());
defparam \Selector26~1 .lut_mask = 16'hEEEA;
defparam \Selector26~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector34~0 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datab(\pending~4_combout ),
	.datac(\pending~9_combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector34~0_combout ),
	.cout());
defparam \Selector34~0 .lut_mask = 16'h0080;
defparam \Selector34~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector26~2 (
	.dataa(\Selector26~1_combout ),
	.datab(\Selector34~0_combout ),
	.datac(\m_state.000000100~q ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector26~2_combout ),
	.cout());
defparam \Selector26~2 .lut_mask = 16'hEEEA;
defparam \Selector26~2 .sum_lutc_input = "datac";

dffeas \m_state.000000100 (
	.clk(clk_clk),
	.d(\Selector26~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.000000100~q ),
	.prn(vcc));
defparam \m_state.000000100 .is_wysiwyg = "true";
defparam \m_state.000000100 .power_up = "low";

cyclone10lp_lcell_comb \Selector24~0 (
	.dataa(\m_state.000000100~q ),
	.datab(gnd),
	.datac(\m_count[2]~q ),
	.datad(\m_count[1]~q ),
	.cin(gnd),
	.combout(\Selector24~0_combout ),
	.cout());
defparam \Selector24~0 .lut_mask = 16'h000A;
defparam \Selector24~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector33~0 (
	.dataa(\m_state.001000000~q ),
	.datab(\m_state.100000000~q ),
	.datac(\m_state.000000100~q ),
	.datad(\m_state.000100000~q ),
	.cin(gnd),
	.combout(\Selector33~0_combout ),
	.cout());
defparam \Selector33~0 .lut_mask = 16'hFFFE;
defparam \Selector33~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector33~1 (
	.dataa(\Selector33~0_combout ),
	.datab(\m_state.000000001~q ),
	.datac(\refresh_request~q ),
	.datad(\m_next.000000001~q ),
	.cin(gnd),
	.combout(\Selector33~1_combout ),
	.cout());
defparam \Selector33~1 .lut_mask = 16'h00AB;
defparam \Selector33~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector33~2 (
	.dataa(\Selector41~4_combout ),
	.datab(\refresh_request~q ),
	.datac(\m_next.000000001~q ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector33~2_combout ),
	.cout());
defparam \Selector33~2 .lut_mask = 16'h008F;
defparam \Selector33~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector33~3 (
	.dataa(\m_state.010000000~q ),
	.datab(\Selector33~2_combout ),
	.datac(\m_state.100000000~q ),
	.datad(\m_next~17_combout ),
	.cin(gnd),
	.combout(\Selector33~3_combout ),
	.cout());
defparam \Selector33~3 .lut_mask = 16'hEEFE;
defparam \Selector33~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector33~4 (
	.dataa(\Selector33~1_combout ),
	.datab(\Selector33~3_combout ),
	.datac(\init_done~q ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector33~4_combout ),
	.cout());
defparam \Selector33~4 .lut_mask = 16'h1110;
defparam \Selector33~4 .sum_lutc_input = "datac";

dffeas \m_next.000000001 (
	.clk(clk_clk),
	.d(\Selector33~4_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_next.000000001~q ),
	.prn(vcc));
defparam \m_next.000000001 .is_wysiwyg = "true";
defparam \m_next.000000001 .power_up = "low";

cyclone10lp_lcell_comb \Selector24~2 (
	.dataa(\Selector24~1_combout ),
	.datab(\Selector24~0_combout ),
	.datac(\m_state.000000001~q ),
	.datad(\m_next.000000001~q ),
	.cin(gnd),
	.combout(\Selector24~2_combout ),
	.cout());
defparam \Selector24~2 .lut_mask = 16'hF531;
defparam \Selector24~2 .sum_lutc_input = "datac";

dffeas \m_state.000000001 (
	.clk(clk_clk),
	.d(\Selector24~2_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\m_state.000000001~q ),
	.prn(vcc));
defparam \m_state.000000001 .is_wysiwyg = "true";
defparam \m_state.000000001 .power_up = "low";

cyclone10lp_lcell_comb \Selector23~0 (
	.dataa(\m_state.000000001~q ),
	.datab(\ack_refresh_request~q ),
	.datac(\m_state.010000000~q ),
	.datad(\init_done~q ),
	.cin(gnd),
	.combout(\Selector23~0_combout ),
	.cout());
defparam \Selector23~0 .lut_mask = 16'hA8EC;
defparam \Selector23~0 .sum_lutc_input = "datac";

dffeas ack_refresh_request(
	.clk(clk_clk),
	.d(\Selector23~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\ack_refresh_request~q ),
	.prn(vcc));
defparam ack_refresh_request.is_wysiwyg = "true";
defparam ack_refresh_request.power_up = "low";

cyclone10lp_lcell_comb \refresh_request~0 (
	.dataa(\init_done~q ),
	.datab(\refresh_request~q ),
	.datac(\Equal0~4_combout ),
	.datad(\ack_refresh_request~q ),
	.cin(gnd),
	.combout(\refresh_request~0_combout ),
	.cout());
defparam \refresh_request~0 .lut_mask = 16'h00A8;
defparam \refresh_request~0 .sum_lutc_input = "datac";

dffeas refresh_request(
	.clk(clk_clk),
	.d(\refresh_request~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\refresh_request~q ),
	.prn(vcc));
defparam refresh_request.is_wysiwyg = "true";
defparam refresh_request.power_up = "low";

cyclone10lp_lcell_comb \active_rnw~2 (
	.dataa(\m_state.000000001~q ),
	.datab(\m_state.100000000~q ),
	.datac(\pending~9_combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\active_rnw~2_combout ),
	.cout());
defparam \active_rnw~2 .lut_mask = 16'hC0E0;
defparam \active_rnw~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \active_rnw~4 (
	.dataa(\pending~4_combout ),
	.datab(\init_done~q ),
	.datac(\active_rnw~2_combout ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\active_rnw~4_combout ),
	.cout());
defparam \active_rnw~4 .lut_mask = 16'h5F13;
defparam \active_rnw~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \active_rnw~5 (
	.dataa(gnd),
	.datab(\active_rnw~4_combout ),
	.datac(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[0]~q ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|entries[1]~q ),
	.cin(gnd),
	.combout(\active_rnw~5_combout ),
	.cout());
defparam \active_rnw~5 .lut_mask = 16'hCCCF;
defparam \active_rnw~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \active_rnw~3 (
	.dataa(\refresh_request~q ),
	.datab(altera_reset_synchronizer_int_chain_out),
	.datac(\active_rnw~5_combout ),
	.datad(gnd),
	.cin(gnd),
	.combout(\active_rnw~3_combout ),
	.cout());
defparam \active_rnw~3 .lut_mask = 16'h0404;
defparam \active_rnw~3 .sum_lutc_input = "datac";

dffeas \active_addr[9] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[27]~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[9]~q ),
	.prn(vcc));
defparam \active_addr[9] .is_wysiwyg = "true";
defparam \active_addr[9] .power_up = "low";

cyclone10lp_lcell_comb \Selector41~6 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datab(\pending~4_combout ),
	.datac(\pending~9_combout ),
	.datad(\m_state.100000000~q ),
	.cin(gnd),
	.combout(\Selector41~6_combout ),
	.cout());
defparam \Selector41~6 .lut_mask = 16'h8000;
defparam \Selector41~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector41~7 (
	.dataa(\Selector41~8_combout ),
	.datab(\Selector41~6_combout ),
	.datac(\Selector34~0_combout ),
	.datad(\refresh_request~q ),
	.cin(gnd),
	.combout(\Selector41~7_combout ),
	.cout());
defparam \Selector41~7 .lut_mask = 16'hAAFE;
defparam \Selector41~7 .sum_lutc_input = "datac";

dffeas f_pop(
	.clk(clk_clk),
	.d(\Selector41~7_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\f_pop~q ),
	.prn(vcc));
defparam f_pop.is_wysiwyg = "true";
defparam f_pop.power_up = "low";

cyclone10lp_lcell_comb \m_addr[6]~2 (
	.dataa(\m_state.000000010~q ),
	.datab(\f_pop~q ),
	.datac(\Selector41~4_combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\m_addr[6]~2_combout ),
	.cout());
defparam \m_addr[6]~2 .lut_mask = 16'hAAC0;
defparam \m_addr[6]~2 .sum_lutc_input = "datac";

dffeas \active_addr[0] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[18]~15_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[0]~q ),
	.prn(vcc));
defparam \active_addr[0] .is_wysiwyg = "true";
defparam \active_addr[0] .power_up = "low";

dffeas \i_addr[11] (
	.clk(clk_clk),
	.d(\i_state.111~q ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_addr[11]~q ),
	.prn(vcc));
defparam \i_addr[11] .is_wysiwyg = "true";
defparam \i_addr[11] .power_up = "low";

cyclone10lp_lcell_comb \Selector94~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[0]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector94~0_combout ),
	.cout());
defparam \Selector94~0 .lut_mask = 16'h0E5E;
defparam \Selector94~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector94~1 (
	.dataa(\active_addr[9]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector94~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[18]~15_combout ),
	.cin(gnd),
	.combout(\Selector94~1_combout ),
	.cout());
defparam \Selector94~1 .lut_mask = 16'hF838;
defparam \Selector94~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_addr[6]~3 (
	.dataa(\m_state.010000000~q ),
	.datab(\m_state.100000000~q ),
	.datac(\m_state.000000100~q ),
	.datad(\m_state.000100000~q ),
	.cin(gnd),
	.combout(\m_addr[6]~3_combout ),
	.cout());
defparam \m_addr[6]~3 .lut_mask = 16'hFFFE;
defparam \m_addr[6]~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \m_addr[6]~4 (
	.dataa(\init_done~q ),
	.datab(\m_state.000000001~q ),
	.datac(\m_addr[6]~3_combout ),
	.datad(gnd),
	.cin(gnd),
	.combout(\m_addr[6]~4_combout ),
	.cout());
defparam \m_addr[6]~4 .lut_mask = 16'h0D0D;
defparam \m_addr[6]~4 .sum_lutc_input = "datac";

dffeas \active_addr[1] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[19]~16_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[1]~q ),
	.prn(vcc));
defparam \active_addr[1] .is_wysiwyg = "true";
defparam \active_addr[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector93~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[1]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector93~0_combout ),
	.cout());
defparam \Selector93~0 .lut_mask = 16'h0E5E;
defparam \Selector93~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector93~1 (
	.dataa(\active_addr[10]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector93~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[19]~16_combout ),
	.cin(gnd),
	.combout(\Selector93~1_combout ),
	.cout());
defparam \Selector93~1 .lut_mask = 16'hF838;
defparam \Selector93~1 .sum_lutc_input = "datac";

dffeas \active_addr[2] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[20]~17_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[2]~q ),
	.prn(vcc));
defparam \active_addr[2] .is_wysiwyg = "true";
defparam \active_addr[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector92~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[2]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector92~0_combout ),
	.cout());
defparam \Selector92~0 .lut_mask = 16'h0E5E;
defparam \Selector92~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector92~1 (
	.dataa(\active_addr[11]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector92~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[20]~17_combout ),
	.cin(gnd),
	.combout(\Selector92~1_combout ),
	.cout());
defparam \Selector92~1 .lut_mask = 16'hF838;
defparam \Selector92~1 .sum_lutc_input = "datac";

dffeas \active_addr[3] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[21]~18_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[3]~q ),
	.prn(vcc));
defparam \active_addr[3] .is_wysiwyg = "true";
defparam \active_addr[3] .power_up = "low";

cyclone10lp_lcell_comb \Selector91~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[3]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector91~0_combout ),
	.cout());
defparam \Selector91~0 .lut_mask = 16'h0E5E;
defparam \Selector91~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector91~1 (
	.dataa(\active_addr[12]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector91~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[21]~18_combout ),
	.cin(gnd),
	.combout(\Selector91~1_combout ),
	.cout());
defparam \Selector91~1 .lut_mask = 16'hF838;
defparam \Selector91~1 .sum_lutc_input = "datac";

dffeas \active_addr[4] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[22]~19_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[4]~q ),
	.prn(vcc));
defparam \active_addr[4] .is_wysiwyg = "true";
defparam \active_addr[4] .power_up = "low";

cyclone10lp_lcell_comb f_select(
	.dataa(\f_pop~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datac(\pending~4_combout ),
	.datad(\pending~9_combout ),
	.cin(gnd),
	.combout(\f_select~combout ),
	.cout());
defparam f_select.lut_mask = 16'h8000;
defparam f_select.sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector90~0 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[22]~19_combout ),
	.datab(\active_addr[4]~q ),
	.datac(\f_select~combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector90~0_combout ),
	.cout());
defparam \Selector90~0 .lut_mask = 16'h00AC;
defparam \Selector90~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector90~1 (
	.dataa(\Selector90~0_combout ),
	.datab(\WideOr9~0_combout ),
	.datac(\active_addr[13]~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector90~1_combout ),
	.cout());
defparam \Selector90~1 .lut_mask = 16'hEAEE;
defparam \Selector90~1 .sum_lutc_input = "datac";

dffeas \active_addr[5] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[23]~20_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[5]~q ),
	.prn(vcc));
defparam \active_addr[5] .is_wysiwyg = "true";
defparam \active_addr[5] .power_up = "low";

cyclone10lp_lcell_comb \Selector89~0 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[23]~20_combout ),
	.datab(\active_addr[5]~q ),
	.datac(\f_select~combout ),
	.datad(\WideOr9~0_combout ),
	.cin(gnd),
	.combout(\Selector89~0_combout ),
	.cout());
defparam \Selector89~0 .lut_mask = 16'h00AC;
defparam \Selector89~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector89~1 (
	.dataa(\Selector89~0_combout ),
	.datab(\WideOr9~0_combout ),
	.datac(\active_addr[14]~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector89~1_combout ),
	.cout());
defparam \Selector89~1 .lut_mask = 16'hEAEE;
defparam \Selector89~1 .sum_lutc_input = "datac";

dffeas \active_addr[6] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[24]~21_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[6]~q ),
	.prn(vcc));
defparam \active_addr[6] .is_wysiwyg = "true";
defparam \active_addr[6] .power_up = "low";

cyclone10lp_lcell_comb \Selector88~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[6]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector88~0_combout ),
	.cout());
defparam \Selector88~0 .lut_mask = 16'h0E5E;
defparam \Selector88~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector88~1 (
	.dataa(\active_addr[15]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector88~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[24]~21_combout ),
	.cin(gnd),
	.combout(\Selector88~1_combout ),
	.cout());
defparam \Selector88~1 .lut_mask = 16'hF838;
defparam \Selector88~1 .sum_lutc_input = "datac";

dffeas \active_addr[7] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[25]~22_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_addr[7]~q ),
	.prn(vcc));
defparam \active_addr[7] .is_wysiwyg = "true";
defparam \active_addr[7] .power_up = "low";

cyclone10lp_lcell_comb \Selector87~0 (
	.dataa(\m_addr[6]~2_combout ),
	.datab(\active_addr[7]~q ),
	.datac(\WideOr9~0_combout ),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector87~0_combout ),
	.cout());
defparam \Selector87~0 .lut_mask = 16'h0E5E;
defparam \Selector87~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector87~1 (
	.dataa(\active_addr[16]~q ),
	.datab(\m_addr[6]~2_combout ),
	.datac(\Selector87~0_combout ),
	.datad(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[25]~22_combout ),
	.cin(gnd),
	.combout(\Selector87~1_combout ),
	.cout());
defparam \Selector87~1 .lut_mask = 16'hF838;
defparam \Selector87~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \always5~0 (
	.dataa(\f_pop~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|Equal1~0_combout ),
	.datac(\pending~4_combout ),
	.datad(\pending~9_combout ),
	.cin(gnd),
	.combout(\always5~0_combout ),
	.cout());
defparam \always5~0 .lut_mask = 16'hD555;
defparam \always5~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal4~0 (
	.dataa(m_cmd_1),
	.datab(gnd),
	.datac(m_cmd_2),
	.datad(m_cmd_0),
	.cin(gnd),
	.combout(\Equal4~0_combout ),
	.cout());
defparam \Equal4~0 .lut_mask = 16'h000A;
defparam \Equal4~0 .sum_lutc_input = "datac";

dffeas \rd_valid[0] (
	.clk(clk_clk),
	.d(\Equal4~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\rd_valid[0]~q ),
	.prn(vcc));
defparam \rd_valid[0] .is_wysiwyg = "true";
defparam \rd_valid[0] .power_up = "low";

dffeas \rd_valid[1] (
	.clk(clk_clk),
	.d(\rd_valid[0]~q ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\rd_valid[1]~q ),
	.prn(vcc));
defparam \rd_valid[1] .is_wysiwyg = "true";
defparam \rd_valid[1] .power_up = "low";

dffeas \rd_valid[2] (
	.clk(clk_clk),
	.d(\rd_valid[1]~q ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\rd_valid[2]~q ),
	.prn(vcc));
defparam \rd_valid[2] .is_wysiwyg = "true";
defparam \rd_valid[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector86~2 (
	.dataa(\active_addr[17]~q ),
	.datab(\m_state.000000010~q ),
	.datac(gnd),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector86~2_combout ),
	.cout());
defparam \Selector86~2 .lut_mask = 16'h88BB;
defparam \Selector86~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector86~3 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\m_state.001000000~q ),
	.datad(\Selector86~2_combout ),
	.cin(gnd),
	.combout(\Selector86~3_combout ),
	.cout());
defparam \Selector86~3 .lut_mask = 16'hF1F0;
defparam \Selector86~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector85~2 (
	.dataa(\active_addr[18]~q ),
	.datab(\m_state.000000010~q ),
	.datac(gnd),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector85~2_combout ),
	.cout());
defparam \Selector85~2 .lut_mask = 16'h88BB;
defparam \Selector85~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector85~3 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\m_state.001000000~q ),
	.datad(\Selector85~2_combout ),
	.cin(gnd),
	.combout(\Selector85~3_combout ),
	.cout());
defparam \Selector85~3 .lut_mask = 16'hF1F0;
defparam \Selector85~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector84~2 (
	.dataa(\active_addr[19]~q ),
	.datab(\m_state.000000010~q ),
	.datac(gnd),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector84~2_combout ),
	.cout());
defparam \Selector84~2 .lut_mask = 16'h88BB;
defparam \Selector84~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector84~3 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\m_state.001000000~q ),
	.datad(\Selector84~2_combout ),
	.cin(gnd),
	.combout(\Selector84~3_combout ),
	.cout());
defparam \Selector84~3 .lut_mask = 16'hF1F0;
defparam \Selector84~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector83~2 (
	.dataa(\active_addr[20]~q ),
	.datab(\m_state.000000010~q ),
	.datac(gnd),
	.datad(\i_addr[11]~q ),
	.cin(gnd),
	.combout(\Selector83~2_combout ),
	.cout());
defparam \Selector83~2 .lut_mask = 16'h88BB;
defparam \Selector83~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector83~3 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\m_state.001000000~q ),
	.datad(\Selector83~2_combout ),
	.cin(gnd),
	.combout(\Selector83~3_combout ),
	.cout());
defparam \Selector83~3 .lut_mask = 16'hF1F0;
defparam \Selector83~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector96~0 (
	.dataa(\active_addr[8]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[26]~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector96~0_combout ),
	.cout());
defparam \Selector96~0 .lut_mask = 16'hAACA;
defparam \Selector96~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \WideOr16~0 (
	.dataa(\m_state.000001000~q ),
	.datab(\m_state.000010000~q ),
	.datac(\m_state.000000010~q ),
	.datad(gnd),
	.cin(gnd),
	.combout(\WideOr16~0_combout ),
	.cout());
defparam \WideOr16~0 .lut_mask = 16'hFEFE;
defparam \WideOr16~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector95~0 (
	.dataa(\active_addr[21]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[39]~3_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector95~0_combout ),
	.cout());
defparam \Selector95~0 .lut_mask = 16'hAACA;
defparam \Selector95~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \i_count[2]~0 (
	.dataa(\i_state.000~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(\i_state.011~q ),
	.cin(gnd),
	.combout(\i_count[2]~0_combout ),
	.cout());
defparam \i_count[2]~0 .lut_mask = 16'h00AA;
defparam \i_count[2]~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector2~0 (
	.dataa(\i_state.001~q ),
	.datab(\i_state.101~q ),
	.datac(\i_cmd[1]~q ),
	.datad(\i_count[2]~0_combout ),
	.cin(gnd),
	.combout(\Selector2~0_combout ),
	.cout());
defparam \Selector2~0 .lut_mask = 16'h5100;
defparam \Selector2~0 .sum_lutc_input = "datac";

dffeas \i_cmd[1] (
	.clk(clk_clk),
	.d(\Selector2~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_cmd[1]~q ),
	.prn(vcc));
defparam \i_cmd[1] .is_wysiwyg = "true";
defparam \i_cmd[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector21~0 (
	.dataa(\init_done~q ),
	.datab(\i_cmd[1]~q ),
	.datac(\m_state.000000001~q ),
	.datad(\m_state.010000000~q ),
	.cin(gnd),
	.combout(\Selector21~0_combout ),
	.cout());
defparam \Selector21~0 .lut_mask = 16'hF404;
defparam \Selector21~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector21~1 (
	.dataa(\always5~0_combout ),
	.datab(\WideOr9~0_combout ),
	.datac(\m_state.000000001~q ),
	.datad(\Selector21~0_combout ),
	.cin(gnd),
	.combout(\Selector21~1_combout ),
	.cout());
defparam \Selector21~1 .lut_mask = 16'hEE20;
defparam \Selector21~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector0~0 (
	.dataa(\i_state.101~q ),
	.datab(gnd),
	.datac(\i_cmd[3]~q ),
	.datad(\i_state.000~q ),
	.cin(gnd),
	.combout(\Selector0~0_combout ),
	.cout());
defparam \Selector0~0 .lut_mask = 16'hF500;
defparam \Selector0~0 .sum_lutc_input = "datac";

dffeas \i_cmd[3] (
	.clk(clk_clk),
	.d(\Selector0~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_cmd[3]~q ),
	.prn(vcc));
defparam \i_cmd[3] .is_wysiwyg = "true";
defparam \i_cmd[3] .power_up = "low";

cyclone10lp_lcell_comb \Selector19~0 (
	.dataa(\init_done~q ),
	.datab(\i_cmd[3]~q ),
	.datac(\refresh_request~q ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector19~0_combout ),
	.cout());
defparam \Selector19~0 .lut_mask = 16'h001B;
defparam \Selector19~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector19~1 (
	.dataa(\m_state.000000001~q ),
	.datab(\m_state.001000000~q ),
	.datac(\m_state.010000000~q ),
	.datad(\m_state.000000100~q ),
	.cin(gnd),
	.combout(\Selector19~1_combout ),
	.cout());
defparam \Selector19~1 .lut_mask = 16'h0002;
defparam \Selector19~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector19~2 (
	.dataa(\m_state.001000000~q ),
	.datab(\m_state.000000100~q ),
	.datac(\refresh_request~q ),
	.datad(\m_next.010000000~q ),
	.cin(gnd),
	.combout(\Selector19~2_combout ),
	.cout());
defparam \Selector19~2 .lut_mask = 16'h0ACE;
defparam \Selector19~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector19~3 (
	.dataa(\Selector19~0_combout ),
	.datab(\active_cs_n~q ),
	.datac(\Selector19~1_combout ),
	.datad(\Selector19~2_combout ),
	.cin(gnd),
	.combout(\Selector19~3_combout ),
	.cout());
defparam \Selector19~3 .lut_mask = 16'h1115;
defparam \Selector19~3 .sum_lutc_input = "datac";

dffeas \active_dqm[0] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[16]~23_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_dqm[0]~q ),
	.prn(vcc));
defparam \active_dqm[0] .is_wysiwyg = "true";
defparam \active_dqm[0] .power_up = "low";

cyclone10lp_lcell_comb \Selector114~0 (
	.dataa(\active_dqm[0]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[16]~23_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector114~0_combout ),
	.cout());
defparam \Selector114~0 .lut_mask = 16'hAACA;
defparam \Selector114~0 .sum_lutc_input = "datac";

dffeas \active_dqm[1] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[17]~24_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_dqm[1]~q ),
	.prn(vcc));
defparam \active_dqm[1] .is_wysiwyg = "true";
defparam \active_dqm[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector113~0 (
	.dataa(\active_dqm[1]~q ),
	.datab(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[17]~24_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector113~0_combout ),
	.cout());
defparam \Selector113~0 .lut_mask = 16'hAACA;
defparam \Selector113~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \WideOr8~0 (
	.dataa(\m_state.000000001~q ),
	.datab(\m_state.000000010~q ),
	.datac(\m_state.001000000~q ),
	.datad(\m_state.010000000~q ),
	.cin(gnd),
	.combout(\WideOr8~0_combout ),
	.cout());
defparam \WideOr8~0 .lut_mask = 16'h0002;
defparam \WideOr8~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector1~0 (
	.dataa(\i_state.011~q ),
	.datab(\i_state.101~q ),
	.datac(\i_cmd[2]~q ),
	.datad(\i_state.000~q ),
	.cin(gnd),
	.combout(\Selector1~0_combout ),
	.cout());
defparam \Selector1~0 .lut_mask = 16'h5100;
defparam \Selector1~0 .sum_lutc_input = "datac";

dffeas \i_cmd[2] (
	.clk(clk_clk),
	.d(\Selector1~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_cmd[2]~q ),
	.prn(vcc));
defparam \i_cmd[2] .is_wysiwyg = "true";
defparam \i_cmd[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector20~0 (
	.dataa(\WideOr8~0_combout ),
	.datab(\init_done~q ),
	.datac(\i_cmd[2]~q ),
	.datad(\m_state.000000001~q ),
	.cin(gnd),
	.combout(\Selector20~0_combout ),
	.cout());
defparam \Selector20~0 .lut_mask = 16'h5510;
defparam \Selector20~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector3~0 (
	.dataa(\i_state.010~q ),
	.datab(\i_state.101~q ),
	.datac(\i_cmd[0]~q ),
	.datad(\i_count[2]~0_combout ),
	.cin(gnd),
	.combout(\Selector3~0_combout ),
	.cout());
defparam \Selector3~0 .lut_mask = 16'h5100;
defparam \Selector3~0 .sum_lutc_input = "datac";

dffeas \i_cmd[0] (
	.clk(clk_clk),
	.d(\Selector3~0_combout ),
	.asdata(vcc),
	.clrn(altera_reset_synchronizer_int_chain_out),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\i_cmd[0]~q ),
	.prn(vcc));
defparam \i_cmd[0] .is_wysiwyg = "true";
defparam \i_cmd[0] .power_up = "low";

cyclone10lp_lcell_comb \Selector22~0 (
	.dataa(\init_done~q ),
	.datab(\i_cmd[0]~q ),
	.datac(\m_state.000000001~q ),
	.datad(\m_state.001000000~q ),
	.cin(gnd),
	.combout(\Selector22~0_combout ),
	.cout());
defparam \Selector22~0 .lut_mask = 16'hF404;
defparam \Selector22~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector22~1 (
	.dataa(\m_state.000010000~q ),
	.datab(\always5~0_combout ),
	.datac(\m_state.000000001~q ),
	.datad(\Selector22~0_combout ),
	.cin(gnd),
	.combout(\Selector22~1_combout ),
	.cout());
defparam \Selector22~1 .lut_mask = 16'hDD80;
defparam \Selector22~1 .sum_lutc_input = "datac";

dffeas \active_data[0] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[0]~25_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[0]~q ),
	.prn(vcc));
defparam \active_data[0] .is_wysiwyg = "true";
defparam \active_data[0] .power_up = "low";

cyclone10lp_lcell_comb \Selector112~0 (
	.dataa(\active_data[0]~q ),
	.datab(m_data_0),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector112~0_combout ),
	.cout());
defparam \Selector112~0 .lut_mask = 16'hAAAC;
defparam \Selector112~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector112~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[0]~25_combout ),
	.datab(\Selector112~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector112~1_combout ),
	.cout());
defparam \Selector112~1 .lut_mask = 16'hACCC;
defparam \Selector112~1 .sum_lutc_input = "datac";

dffeas \active_data[1] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[1]~26_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[1]~q ),
	.prn(vcc));
defparam \active_data[1] .is_wysiwyg = "true";
defparam \active_data[1] .power_up = "low";

cyclone10lp_lcell_comb \Selector111~0 (
	.dataa(\active_data[1]~q ),
	.datab(m_data_1),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector111~0_combout ),
	.cout());
defparam \Selector111~0 .lut_mask = 16'hAAAC;
defparam \Selector111~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector111~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[1]~26_combout ),
	.datab(\Selector111~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector111~1_combout ),
	.cout());
defparam \Selector111~1 .lut_mask = 16'hACCC;
defparam \Selector111~1 .sum_lutc_input = "datac";

dffeas \active_data[2] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[2]~27_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[2]~q ),
	.prn(vcc));
defparam \active_data[2] .is_wysiwyg = "true";
defparam \active_data[2] .power_up = "low";

cyclone10lp_lcell_comb \Selector110~0 (
	.dataa(\active_data[2]~q ),
	.datab(m_data_2),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector110~0_combout ),
	.cout());
defparam \Selector110~0 .lut_mask = 16'hAAAC;
defparam \Selector110~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector110~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[2]~27_combout ),
	.datab(\Selector110~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector110~1_combout ),
	.cout());
defparam \Selector110~1 .lut_mask = 16'hACCC;
defparam \Selector110~1 .sum_lutc_input = "datac";

dffeas \active_data[3] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[3]~28_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[3]~q ),
	.prn(vcc));
defparam \active_data[3] .is_wysiwyg = "true";
defparam \active_data[3] .power_up = "low";

cyclone10lp_lcell_comb \Selector109~0 (
	.dataa(\active_data[3]~q ),
	.datab(m_data_3),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector109~0_combout ),
	.cout());
defparam \Selector109~0 .lut_mask = 16'hAAAC;
defparam \Selector109~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector109~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[3]~28_combout ),
	.datab(\Selector109~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector109~1_combout ),
	.cout());
defparam \Selector109~1 .lut_mask = 16'hACCC;
defparam \Selector109~1 .sum_lutc_input = "datac";

dffeas \active_data[4] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[4]~29_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[4]~q ),
	.prn(vcc));
defparam \active_data[4] .is_wysiwyg = "true";
defparam \active_data[4] .power_up = "low";

cyclone10lp_lcell_comb \Selector108~0 (
	.dataa(\active_data[4]~q ),
	.datab(m_data_4),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector108~0_combout ),
	.cout());
defparam \Selector108~0 .lut_mask = 16'hAAAC;
defparam \Selector108~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector108~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[4]~29_combout ),
	.datab(\Selector108~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector108~1_combout ),
	.cout());
defparam \Selector108~1 .lut_mask = 16'hACCC;
defparam \Selector108~1 .sum_lutc_input = "datac";

dffeas \active_data[5] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[5]~30_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[5]~q ),
	.prn(vcc));
defparam \active_data[5] .is_wysiwyg = "true";
defparam \active_data[5] .power_up = "low";

cyclone10lp_lcell_comb \Selector107~0 (
	.dataa(\active_data[5]~q ),
	.datab(m_data_5),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector107~0_combout ),
	.cout());
defparam \Selector107~0 .lut_mask = 16'hAAAC;
defparam \Selector107~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector107~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[5]~30_combout ),
	.datab(\Selector107~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector107~1_combout ),
	.cout());
defparam \Selector107~1 .lut_mask = 16'hACCC;
defparam \Selector107~1 .sum_lutc_input = "datac";

dffeas \active_data[6] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[6]~31_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[6]~q ),
	.prn(vcc));
defparam \active_data[6] .is_wysiwyg = "true";
defparam \active_data[6] .power_up = "low";

cyclone10lp_lcell_comb \Selector106~0 (
	.dataa(\active_data[6]~q ),
	.datab(m_data_6),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector106~0_combout ),
	.cout());
defparam \Selector106~0 .lut_mask = 16'hAAAC;
defparam \Selector106~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector106~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[6]~31_combout ),
	.datab(\Selector106~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector106~1_combout ),
	.cout());
defparam \Selector106~1 .lut_mask = 16'hACCC;
defparam \Selector106~1 .sum_lutc_input = "datac";

dffeas \active_data[7] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[7]~32_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[7]~q ),
	.prn(vcc));
defparam \active_data[7] .is_wysiwyg = "true";
defparam \active_data[7] .power_up = "low";

cyclone10lp_lcell_comb \Selector105~0 (
	.dataa(\active_data[7]~q ),
	.datab(m_data_7),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector105~0_combout ),
	.cout());
defparam \Selector105~0 .lut_mask = 16'hAAAC;
defparam \Selector105~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector105~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[7]~32_combout ),
	.datab(\Selector105~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector105~1_combout ),
	.cout());
defparam \Selector105~1 .lut_mask = 16'hACCC;
defparam \Selector105~1 .sum_lutc_input = "datac";

dffeas \active_data[8] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[8]~33_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[8]~q ),
	.prn(vcc));
defparam \active_data[8] .is_wysiwyg = "true";
defparam \active_data[8] .power_up = "low";

cyclone10lp_lcell_comb \Selector104~0 (
	.dataa(\active_data[8]~q ),
	.datab(m_data_8),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector104~0_combout ),
	.cout());
defparam \Selector104~0 .lut_mask = 16'hAAAC;
defparam \Selector104~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector104~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[8]~33_combout ),
	.datab(\Selector104~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector104~1_combout ),
	.cout());
defparam \Selector104~1 .lut_mask = 16'hACCC;
defparam \Selector104~1 .sum_lutc_input = "datac";

dffeas \active_data[9] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[9]~34_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[9]~q ),
	.prn(vcc));
defparam \active_data[9] .is_wysiwyg = "true";
defparam \active_data[9] .power_up = "low";

cyclone10lp_lcell_comb \Selector103~0 (
	.dataa(\active_data[9]~q ),
	.datab(m_data_9),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector103~0_combout ),
	.cout());
defparam \Selector103~0 .lut_mask = 16'hAAAC;
defparam \Selector103~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector103~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[9]~34_combout ),
	.datab(\Selector103~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector103~1_combout ),
	.cout());
defparam \Selector103~1 .lut_mask = 16'hACCC;
defparam \Selector103~1 .sum_lutc_input = "datac";

dffeas \active_data[10] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[10]~35_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[10]~q ),
	.prn(vcc));
defparam \active_data[10] .is_wysiwyg = "true";
defparam \active_data[10] .power_up = "low";

cyclone10lp_lcell_comb \Selector102~0 (
	.dataa(\active_data[10]~q ),
	.datab(m_data_10),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector102~0_combout ),
	.cout());
defparam \Selector102~0 .lut_mask = 16'hAAAC;
defparam \Selector102~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector102~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[10]~35_combout ),
	.datab(\Selector102~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector102~1_combout ),
	.cout());
defparam \Selector102~1 .lut_mask = 16'hACCC;
defparam \Selector102~1 .sum_lutc_input = "datac";

dffeas \active_data[11] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[11]~36_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[11]~q ),
	.prn(vcc));
defparam \active_data[11] .is_wysiwyg = "true";
defparam \active_data[11] .power_up = "low";

cyclone10lp_lcell_comb \Selector101~0 (
	.dataa(\active_data[11]~q ),
	.datab(m_data_11),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector101~0_combout ),
	.cout());
defparam \Selector101~0 .lut_mask = 16'hAAAC;
defparam \Selector101~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector101~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[11]~36_combout ),
	.datab(\Selector101~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector101~1_combout ),
	.cout());
defparam \Selector101~1 .lut_mask = 16'hACCC;
defparam \Selector101~1 .sum_lutc_input = "datac";

dffeas \active_data[12] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[12]~37_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[12]~q ),
	.prn(vcc));
defparam \active_data[12] .is_wysiwyg = "true";
defparam \active_data[12] .power_up = "low";

cyclone10lp_lcell_comb \Selector100~0 (
	.dataa(\active_data[12]~q ),
	.datab(m_data_12),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector100~0_combout ),
	.cout());
defparam \Selector100~0 .lut_mask = 16'hAAAC;
defparam \Selector100~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector100~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[12]~37_combout ),
	.datab(\Selector100~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector100~1_combout ),
	.cout());
defparam \Selector100~1 .lut_mask = 16'hACCC;
defparam \Selector100~1 .sum_lutc_input = "datac";

dffeas \active_data[13] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[13]~38_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[13]~q ),
	.prn(vcc));
defparam \active_data[13] .is_wysiwyg = "true";
defparam \active_data[13] .power_up = "low";

cyclone10lp_lcell_comb \Selector99~0 (
	.dataa(\active_data[13]~q ),
	.datab(m_data_13),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector99~0_combout ),
	.cout());
defparam \Selector99~0 .lut_mask = 16'hAAAC;
defparam \Selector99~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector99~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[13]~38_combout ),
	.datab(\Selector99~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector99~1_combout ),
	.cout());
defparam \Selector99~1 .lut_mask = 16'hACCC;
defparam \Selector99~1 .sum_lutc_input = "datac";

dffeas \active_data[14] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[14]~39_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[14]~q ),
	.prn(vcc));
defparam \active_data[14] .is_wysiwyg = "true";
defparam \active_data[14] .power_up = "low";

cyclone10lp_lcell_comb \Selector98~0 (
	.dataa(\active_data[14]~q ),
	.datab(m_data_14),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector98~0_combout ),
	.cout());
defparam \Selector98~0 .lut_mask = 16'hAAAC;
defparam \Selector98~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector98~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[14]~39_combout ),
	.datab(\Selector98~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector98~1_combout ),
	.cout());
defparam \Selector98~1 .lut_mask = 16'hACCC;
defparam \Selector98~1 .sum_lutc_input = "datac";

dffeas \active_data[15] (
	.clk(clk_clk),
	.d(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[15]~40_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\active_rnw~3_combout ),
	.q(\active_data[15]~q ),
	.prn(vcc));
defparam \active_data[15] .is_wysiwyg = "true";
defparam \active_data[15] .power_up = "low";

cyclone10lp_lcell_comb \Selector97~0 (
	.dataa(\active_data[15]~q ),
	.datab(m_data_15),
	.datac(\m_state.000010000~q ),
	.datad(\m_state.000000010~q ),
	.cin(gnd),
	.combout(\Selector97~0_combout ),
	.cout());
defparam \Selector97~0 .lut_mask = 16'hAAAC;
defparam \Selector97~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Selector97~1 (
	.dataa(\the_sdram_controller_new_sdram_controller_0_input_efifo_module|rd_data[15]~40_combout ),
	.datab(\Selector97~0_combout ),
	.datac(\f_select~combout ),
	.datad(\m_state.000010000~q ),
	.cin(gnd),
	.combout(\Selector97~1_combout ),
	.cout());
defparam \Selector97~1 .lut_mask = 16'hACCC;
defparam \Selector97~1 .sum_lutc_input = "datac";

endmodule

module sdram_controller_sdram_controller_new_sdram_controller_0_input_efifo_module (
	entries_1,
	entries_0,
	Equal0,
	reset_n,
	Equal1,
	rd_data_26,
	rd_data_40,
	rd_data_27,
	rd_data_39,
	rd_data_29,
	rd_data_28,
	rd_data_31,
	rd_data_30,
	rd_data_33,
	rd_data_32,
	rd_data_35,
	rd_data_34,
	rd_data_37,
	rd_data_36,
	rd_data_38,
	f_select,
	comb,
	rd_data_18,
	rd_data_19,
	rd_data_20,
	rd_data_21,
	rd_data_22,
	rd_data_23,
	rd_data_24,
	rd_data_25,
	rd_data_16,
	rd_data_17,
	comb1,
	comb2,
	rd_data_0,
	rd_data_1,
	rd_data_2,
	rd_data_3,
	rd_data_4,
	rd_data_5,
	rd_data_6,
	rd_data_7,
	rd_data_8,
	rd_data_9,
	rd_data_10,
	rd_data_11,
	rd_data_12,
	rd_data_13,
	rd_data_14,
	rd_data_15,
	clk,
	new_sdram_controller_0_s1_read_n,
	new_sdram_controller_0_s1_write_n,
	new_sdram_controller_0_s1_address_8,
	new_sdram_controller_0_s1_address_9,
	new_sdram_controller_0_s1_address_21,
	new_sdram_controller_0_s1_address_11,
	new_sdram_controller_0_s1_address_10,
	new_sdram_controller_0_s1_address_13,
	new_sdram_controller_0_s1_address_12,
	new_sdram_controller_0_s1_address_15,
	new_sdram_controller_0_s1_address_14,
	new_sdram_controller_0_s1_address_17,
	new_sdram_controller_0_s1_address_16,
	new_sdram_controller_0_s1_address_19,
	new_sdram_controller_0_s1_address_18,
	new_sdram_controller_0_s1_address_20,
	new_sdram_controller_0_s1_address_0,
	new_sdram_controller_0_s1_address_1,
	new_sdram_controller_0_s1_address_2,
	new_sdram_controller_0_s1_address_3,
	new_sdram_controller_0_s1_address_4,
	new_sdram_controller_0_s1_address_5,
	new_sdram_controller_0_s1_address_6,
	new_sdram_controller_0_s1_address_7,
	new_sdram_controller_0_s1_writedata_0,
	new_sdram_controller_0_s1_writedata_1,
	new_sdram_controller_0_s1_writedata_2,
	new_sdram_controller_0_s1_writedata_3,
	new_sdram_controller_0_s1_writedata_4,
	new_sdram_controller_0_s1_writedata_5,
	new_sdram_controller_0_s1_writedata_6,
	new_sdram_controller_0_s1_writedata_7,
	new_sdram_controller_0_s1_writedata_8,
	new_sdram_controller_0_s1_writedata_9,
	new_sdram_controller_0_s1_writedata_10,
	new_sdram_controller_0_s1_writedata_11,
	new_sdram_controller_0_s1_writedata_12,
	new_sdram_controller_0_s1_writedata_13,
	new_sdram_controller_0_s1_writedata_14,
	new_sdram_controller_0_s1_writedata_15)/* synthesis synthesis_greybox=0 */;
output 	entries_1;
output 	entries_0;
output 	Equal0;
input 	reset_n;
output 	Equal1;
output 	rd_data_26;
output 	rd_data_40;
output 	rd_data_27;
output 	rd_data_39;
output 	rd_data_29;
output 	rd_data_28;
output 	rd_data_31;
output 	rd_data_30;
output 	rd_data_33;
output 	rd_data_32;
output 	rd_data_35;
output 	rd_data_34;
output 	rd_data_37;
output 	rd_data_36;
output 	rd_data_38;
input 	f_select;
input 	comb;
output 	rd_data_18;
output 	rd_data_19;
output 	rd_data_20;
output 	rd_data_21;
output 	rd_data_22;
output 	rd_data_23;
output 	rd_data_24;
output 	rd_data_25;
output 	rd_data_16;
output 	rd_data_17;
input 	comb1;
input 	comb2;
output 	rd_data_0;
output 	rd_data_1;
output 	rd_data_2;
output 	rd_data_3;
output 	rd_data_4;
output 	rd_data_5;
output 	rd_data_6;
output 	rd_data_7;
output 	rd_data_8;
output 	rd_data_9;
output 	rd_data_10;
output 	rd_data_11;
output 	rd_data_12;
output 	rd_data_13;
output 	rd_data_14;
output 	rd_data_15;
input 	clk;
input 	new_sdram_controller_0_s1_read_n;
input 	new_sdram_controller_0_s1_write_n;
input 	new_sdram_controller_0_s1_address_8;
input 	new_sdram_controller_0_s1_address_9;
input 	new_sdram_controller_0_s1_address_21;
input 	new_sdram_controller_0_s1_address_11;
input 	new_sdram_controller_0_s1_address_10;
input 	new_sdram_controller_0_s1_address_13;
input 	new_sdram_controller_0_s1_address_12;
input 	new_sdram_controller_0_s1_address_15;
input 	new_sdram_controller_0_s1_address_14;
input 	new_sdram_controller_0_s1_address_17;
input 	new_sdram_controller_0_s1_address_16;
input 	new_sdram_controller_0_s1_address_19;
input 	new_sdram_controller_0_s1_address_18;
input 	new_sdram_controller_0_s1_address_20;
input 	new_sdram_controller_0_s1_address_0;
input 	new_sdram_controller_0_s1_address_1;
input 	new_sdram_controller_0_s1_address_2;
input 	new_sdram_controller_0_s1_address_3;
input 	new_sdram_controller_0_s1_address_4;
input 	new_sdram_controller_0_s1_address_5;
input 	new_sdram_controller_0_s1_address_6;
input 	new_sdram_controller_0_s1_address_7;
input 	new_sdram_controller_0_s1_writedata_0;
input 	new_sdram_controller_0_s1_writedata_1;
input 	new_sdram_controller_0_s1_writedata_2;
input 	new_sdram_controller_0_s1_writedata_3;
input 	new_sdram_controller_0_s1_writedata_4;
input 	new_sdram_controller_0_s1_writedata_5;
input 	new_sdram_controller_0_s1_writedata_6;
input 	new_sdram_controller_0_s1_writedata_7;
input 	new_sdram_controller_0_s1_writedata_8;
input 	new_sdram_controller_0_s1_writedata_9;
input 	new_sdram_controller_0_s1_writedata_10;
input 	new_sdram_controller_0_s1_writedata_11;
input 	new_sdram_controller_0_s1_writedata_12;
input 	new_sdram_controller_0_s1_writedata_13;
input 	new_sdram_controller_0_s1_writedata_14;
input 	new_sdram_controller_0_s1_writedata_15;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \entries[1]~0_combout ;
wire \entries[0]~1_combout ;
wire \wr_address~0_combout ;
wire \wr_address~q ;
wire \entry_1[40]~0_combout ;
wire \entry_1[26]~q ;
wire \entry_0[40]~0_combout ;
wire \entry_0[26]~q ;
wire \rd_address~0_combout ;
wire \rd_address~q ;
wire \entry_1[40]~q ;
wire \entry_0[40]~q ;
wire \entry_1[27]~q ;
wire \entry_0[27]~q ;
wire \entry_1[39]~q ;
wire \entry_0[39]~q ;
wire \entry_1[29]~q ;
wire \entry_0[29]~q ;
wire \entry_1[28]~q ;
wire \entry_0[28]~q ;
wire \entry_1[31]~q ;
wire \entry_0[31]~q ;
wire \entry_1[30]~q ;
wire \entry_0[30]~q ;
wire \entry_1[33]~q ;
wire \entry_0[33]~q ;
wire \entry_1[32]~q ;
wire \entry_0[32]~q ;
wire \entry_1[35]~q ;
wire \entry_0[35]~q ;
wire \entry_1[34]~q ;
wire \entry_0[34]~q ;
wire \entry_1[37]~q ;
wire \entry_0[37]~q ;
wire \entry_1[36]~q ;
wire \entry_0[36]~q ;
wire \entry_1[38]~q ;
wire \entry_0[38]~q ;
wire \entry_1[18]~q ;
wire \entry_0[18]~q ;
wire \entry_1[19]~q ;
wire \entry_0[19]~q ;
wire \entry_1[20]~q ;
wire \entry_0[20]~q ;
wire \entry_1[21]~q ;
wire \entry_0[21]~q ;
wire \entry_1[22]~q ;
wire \entry_0[22]~q ;
wire \entry_1[23]~q ;
wire \entry_0[23]~q ;
wire \entry_1[24]~q ;
wire \entry_0[24]~q ;
wire \entry_1[25]~q ;
wire \entry_0[25]~q ;
wire \entry_1[16]~q ;
wire \entry_0[16]~q ;
wire \entry_1[17]~q ;
wire \entry_0[17]~q ;
wire \entry_1[0]~q ;
wire \entry_0[0]~q ;
wire \entry_1[1]~q ;
wire \entry_0[1]~q ;
wire \entry_1[2]~q ;
wire \entry_0[2]~q ;
wire \entry_1[3]~q ;
wire \entry_0[3]~q ;
wire \entry_1[4]~q ;
wire \entry_0[4]~q ;
wire \entry_1[5]~q ;
wire \entry_0[5]~q ;
wire \entry_1[6]~q ;
wire \entry_0[6]~q ;
wire \entry_1[7]~q ;
wire \entry_0[7]~q ;
wire \entry_1[8]~q ;
wire \entry_0[8]~q ;
wire \entry_1[9]~q ;
wire \entry_0[9]~q ;
wire \entry_1[10]~q ;
wire \entry_0[10]~q ;
wire \entry_1[11]~q ;
wire \entry_0[11]~q ;
wire \entry_1[12]~q ;
wire \entry_0[12]~q ;
wire \entry_1[13]~q ;
wire \entry_0[13]~q ;
wire \entry_1[14]~q ;
wire \entry_0[14]~q ;
wire \entry_1[15]~q ;
wire \entry_0[15]~q ;


dffeas \entries[1] (
	.clk(clk),
	.d(\entries[1]~0_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(entries_1),
	.prn(vcc));
defparam \entries[1] .is_wysiwyg = "true";
defparam \entries[1] .power_up = "low";

dffeas \entries[0] (
	.clk(clk),
	.d(\entries[0]~1_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(entries_0),
	.prn(vcc));
defparam \entries[0] .is_wysiwyg = "true";
defparam \entries[0] .power_up = "low";

cyclone10lp_lcell_comb \Equal0~0 (
	.dataa(entries_1),
	.datab(gnd),
	.datac(gnd),
	.datad(entries_0),
	.cin(gnd),
	.combout(Equal0),
	.cout());
defparam \Equal0~0 .lut_mask = 16'h00AA;
defparam \Equal0~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal1~0 (
	.dataa(entries_1),
	.datab(entries_0),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(Equal1),
	.cout());
defparam \Equal1~0 .lut_mask = 16'hEEEE;
defparam \Equal1~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[26]~0 (
	.dataa(\entry_1[26]~q ),
	.datab(\entry_0[26]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_26),
	.cout());
defparam \rd_data[26]~0 .lut_mask = 16'hAACC;
defparam \rd_data[26]~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[40]~1 (
	.dataa(\entry_1[40]~q ),
	.datab(\entry_0[40]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_40),
	.cout());
defparam \rd_data[40]~1 .lut_mask = 16'hAACC;
defparam \rd_data[40]~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[27]~2 (
	.dataa(\entry_1[27]~q ),
	.datab(\entry_0[27]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_27),
	.cout());
defparam \rd_data[27]~2 .lut_mask = 16'hAACC;
defparam \rd_data[27]~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[39]~3 (
	.dataa(\entry_1[39]~q ),
	.datab(\entry_0[39]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_39),
	.cout());
defparam \rd_data[39]~3 .lut_mask = 16'hAACC;
defparam \rd_data[39]~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[29]~4 (
	.dataa(\entry_1[29]~q ),
	.datab(\entry_0[29]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_29),
	.cout());
defparam \rd_data[29]~4 .lut_mask = 16'hAACC;
defparam \rd_data[29]~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[28]~5 (
	.dataa(\entry_1[28]~q ),
	.datab(\entry_0[28]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_28),
	.cout());
defparam \rd_data[28]~5 .lut_mask = 16'hAACC;
defparam \rd_data[28]~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[31]~6 (
	.dataa(\entry_1[31]~q ),
	.datab(\entry_0[31]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_31),
	.cout());
defparam \rd_data[31]~6 .lut_mask = 16'hAACC;
defparam \rd_data[31]~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[30]~7 (
	.dataa(\entry_1[30]~q ),
	.datab(\entry_0[30]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_30),
	.cout());
defparam \rd_data[30]~7 .lut_mask = 16'hAACC;
defparam \rd_data[30]~7 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[33]~8 (
	.dataa(\entry_1[33]~q ),
	.datab(\entry_0[33]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_33),
	.cout());
defparam \rd_data[33]~8 .lut_mask = 16'hAACC;
defparam \rd_data[33]~8 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[32]~9 (
	.dataa(\entry_1[32]~q ),
	.datab(\entry_0[32]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_32),
	.cout());
defparam \rd_data[32]~9 .lut_mask = 16'hAACC;
defparam \rd_data[32]~9 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[35]~10 (
	.dataa(\entry_1[35]~q ),
	.datab(\entry_0[35]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_35),
	.cout());
defparam \rd_data[35]~10 .lut_mask = 16'hAACC;
defparam \rd_data[35]~10 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[34]~11 (
	.dataa(\entry_1[34]~q ),
	.datab(\entry_0[34]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_34),
	.cout());
defparam \rd_data[34]~11 .lut_mask = 16'hAACC;
defparam \rd_data[34]~11 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[37]~12 (
	.dataa(\entry_1[37]~q ),
	.datab(\entry_0[37]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_37),
	.cout());
defparam \rd_data[37]~12 .lut_mask = 16'hAACC;
defparam \rd_data[37]~12 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[36]~13 (
	.dataa(\entry_1[36]~q ),
	.datab(\entry_0[36]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_36),
	.cout());
defparam \rd_data[36]~13 .lut_mask = 16'hAACC;
defparam \rd_data[36]~13 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[38]~14 (
	.dataa(\entry_1[38]~q ),
	.datab(\entry_0[38]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_38),
	.cout());
defparam \rd_data[38]~14 .lut_mask = 16'hAACC;
defparam \rd_data[38]~14 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[18]~15 (
	.dataa(\entry_1[18]~q ),
	.datab(\entry_0[18]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_18),
	.cout());
defparam \rd_data[18]~15 .lut_mask = 16'hAACC;
defparam \rd_data[18]~15 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[19]~16 (
	.dataa(\entry_1[19]~q ),
	.datab(\entry_0[19]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_19),
	.cout());
defparam \rd_data[19]~16 .lut_mask = 16'hAACC;
defparam \rd_data[19]~16 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[20]~17 (
	.dataa(\entry_1[20]~q ),
	.datab(\entry_0[20]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_20),
	.cout());
defparam \rd_data[20]~17 .lut_mask = 16'hAACC;
defparam \rd_data[20]~17 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[21]~18 (
	.dataa(\entry_1[21]~q ),
	.datab(\entry_0[21]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_21),
	.cout());
defparam \rd_data[21]~18 .lut_mask = 16'hAACC;
defparam \rd_data[21]~18 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[22]~19 (
	.dataa(\entry_1[22]~q ),
	.datab(\entry_0[22]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_22),
	.cout());
defparam \rd_data[22]~19 .lut_mask = 16'hAACC;
defparam \rd_data[22]~19 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[23]~20 (
	.dataa(\entry_1[23]~q ),
	.datab(\entry_0[23]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_23),
	.cout());
defparam \rd_data[23]~20 .lut_mask = 16'hAACC;
defparam \rd_data[23]~20 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[24]~21 (
	.dataa(\entry_1[24]~q ),
	.datab(\entry_0[24]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_24),
	.cout());
defparam \rd_data[24]~21 .lut_mask = 16'hAACC;
defparam \rd_data[24]~21 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[25]~22 (
	.dataa(\entry_1[25]~q ),
	.datab(\entry_0[25]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_25),
	.cout());
defparam \rd_data[25]~22 .lut_mask = 16'hAACC;
defparam \rd_data[25]~22 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[16]~23 (
	.dataa(\entry_1[16]~q ),
	.datab(\entry_0[16]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_16),
	.cout());
defparam \rd_data[16]~23 .lut_mask = 16'hAACC;
defparam \rd_data[16]~23 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[17]~24 (
	.dataa(\entry_1[17]~q ),
	.datab(\entry_0[17]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_17),
	.cout());
defparam \rd_data[17]~24 .lut_mask = 16'hAACC;
defparam \rd_data[17]~24 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[0]~25 (
	.dataa(\entry_1[0]~q ),
	.datab(\entry_0[0]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_0),
	.cout());
defparam \rd_data[0]~25 .lut_mask = 16'hAACC;
defparam \rd_data[0]~25 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[1]~26 (
	.dataa(\entry_1[1]~q ),
	.datab(\entry_0[1]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_1),
	.cout());
defparam \rd_data[1]~26 .lut_mask = 16'hAACC;
defparam \rd_data[1]~26 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[2]~27 (
	.dataa(\entry_1[2]~q ),
	.datab(\entry_0[2]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_2),
	.cout());
defparam \rd_data[2]~27 .lut_mask = 16'hAACC;
defparam \rd_data[2]~27 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[3]~28 (
	.dataa(\entry_1[3]~q ),
	.datab(\entry_0[3]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_3),
	.cout());
defparam \rd_data[3]~28 .lut_mask = 16'hAACC;
defparam \rd_data[3]~28 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[4]~29 (
	.dataa(\entry_1[4]~q ),
	.datab(\entry_0[4]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_4),
	.cout());
defparam \rd_data[4]~29 .lut_mask = 16'hAACC;
defparam \rd_data[4]~29 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[5]~30 (
	.dataa(\entry_1[5]~q ),
	.datab(\entry_0[5]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_5),
	.cout());
defparam \rd_data[5]~30 .lut_mask = 16'hAACC;
defparam \rd_data[5]~30 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[6]~31 (
	.dataa(\entry_1[6]~q ),
	.datab(\entry_0[6]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_6),
	.cout());
defparam \rd_data[6]~31 .lut_mask = 16'hAACC;
defparam \rd_data[6]~31 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[7]~32 (
	.dataa(\entry_1[7]~q ),
	.datab(\entry_0[7]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_7),
	.cout());
defparam \rd_data[7]~32 .lut_mask = 16'hAACC;
defparam \rd_data[7]~32 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[8]~33 (
	.dataa(\entry_1[8]~q ),
	.datab(\entry_0[8]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_8),
	.cout());
defparam \rd_data[8]~33 .lut_mask = 16'hAACC;
defparam \rd_data[8]~33 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[9]~34 (
	.dataa(\entry_1[9]~q ),
	.datab(\entry_0[9]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_9),
	.cout());
defparam \rd_data[9]~34 .lut_mask = 16'hAACC;
defparam \rd_data[9]~34 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[10]~35 (
	.dataa(\entry_1[10]~q ),
	.datab(\entry_0[10]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_10),
	.cout());
defparam \rd_data[10]~35 .lut_mask = 16'hAACC;
defparam \rd_data[10]~35 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[11]~36 (
	.dataa(\entry_1[11]~q ),
	.datab(\entry_0[11]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_11),
	.cout());
defparam \rd_data[11]~36 .lut_mask = 16'hAACC;
defparam \rd_data[11]~36 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[12]~37 (
	.dataa(\entry_1[12]~q ),
	.datab(\entry_0[12]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_12),
	.cout());
defparam \rd_data[12]~37 .lut_mask = 16'hAACC;
defparam \rd_data[12]~37 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[13]~38 (
	.dataa(\entry_1[13]~q ),
	.datab(\entry_0[13]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_13),
	.cout());
defparam \rd_data[13]~38 .lut_mask = 16'hAACC;
defparam \rd_data[13]~38 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[14]~39 (
	.dataa(\entry_1[14]~q ),
	.datab(\entry_0[14]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_14),
	.cout());
defparam \rd_data[14]~39 .lut_mask = 16'hAACC;
defparam \rd_data[14]~39 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \rd_data[15]~40 (
	.dataa(\entry_1[15]~q ),
	.datab(\entry_0[15]~q ),
	.datac(gnd),
	.datad(\rd_address~q ),
	.cin(gnd),
	.combout(rd_data_15),
	.cout());
defparam \rd_data[15]~40 .lut_mask = 16'hAACC;
defparam \rd_data[15]~40 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \entries[1]~0 (
	.dataa(entries_1),
	.datab(f_select),
	.datac(entries_0),
	.datad(comb),
	.cin(gnd),
	.combout(\entries[1]~0_combout ),
	.cout());
defparam \entries[1]~0 .lut_mask = 16'h9AA6;
defparam \entries[1]~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \entries[0]~1 (
	.dataa(entries_0),
	.datab(f_select),
	.datac(comb),
	.datad(gnd),
	.cin(gnd),
	.combout(\entries[0]~1_combout ),
	.cout());
defparam \entries[0]~1 .lut_mask = 16'h9696;
defparam \entries[0]~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \wr_address~0 (
	.dataa(\wr_address~q ),
	.datab(Equal0),
	.datac(new_sdram_controller_0_s1_read_n),
	.datad(new_sdram_controller_0_s1_write_n),
	.cin(gnd),
	.combout(\wr_address~0_combout ),
	.cout());
defparam \wr_address~0 .lut_mask = 16'hA999;
defparam \wr_address~0 .sum_lutc_input = "datac";

dffeas wr_address(
	.clk(clk),
	.d(\wr_address~0_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\wr_address~q ),
	.prn(vcc));
defparam wr_address.is_wysiwyg = "true";
defparam wr_address.power_up = "low";

cyclone10lp_lcell_comb \entry_1[40]~0 (
	.dataa(\wr_address~q ),
	.datab(new_sdram_controller_0_s1_read_n),
	.datac(new_sdram_controller_0_s1_write_n),
	.datad(Equal0),
	.cin(gnd),
	.combout(\entry_1[40]~0_combout ),
	.cout());
defparam \entry_1[40]~0 .lut_mask = 16'h002A;
defparam \entry_1[40]~0 .sum_lutc_input = "datac";

dffeas \entry_1[26] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_8),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[26]~q ),
	.prn(vcc));
defparam \entry_1[26] .is_wysiwyg = "true";
defparam \entry_1[26] .power_up = "low";

cyclone10lp_lcell_comb \entry_0[40]~0 (
	.dataa(new_sdram_controller_0_s1_read_n),
	.datab(new_sdram_controller_0_s1_write_n),
	.datac(Equal0),
	.datad(\wr_address~q ),
	.cin(gnd),
	.combout(\entry_0[40]~0_combout ),
	.cout());
defparam \entry_0[40]~0 .lut_mask = 16'h0007;
defparam \entry_0[40]~0 .sum_lutc_input = "datac";

dffeas \entry_0[26] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_8),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[26]~q ),
	.prn(vcc));
defparam \entry_0[26] .is_wysiwyg = "true";
defparam \entry_0[26] .power_up = "low";

cyclone10lp_lcell_comb \rd_address~0 (
	.dataa(gnd),
	.datab(gnd),
	.datac(\rd_address~q ),
	.datad(f_select),
	.cin(gnd),
	.combout(\rd_address~0_combout ),
	.cout());
defparam \rd_address~0 .lut_mask = 16'h0FF0;
defparam \rd_address~0 .sum_lutc_input = "datac";

dffeas rd_address(
	.clk(clk),
	.d(\rd_address~0_combout ),
	.asdata(vcc),
	.clrn(reset_n),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\rd_address~q ),
	.prn(vcc));
defparam rd_address.is_wysiwyg = "true";
defparam rd_address.power_up = "low";

dffeas \entry_1[40] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_write_n),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[40]~q ),
	.prn(vcc));
defparam \entry_1[40] .is_wysiwyg = "true";
defparam \entry_1[40] .power_up = "low";

dffeas \entry_0[40] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_write_n),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[40]~q ),
	.prn(vcc));
defparam \entry_0[40] .is_wysiwyg = "true";
defparam \entry_0[40] .power_up = "low";

dffeas \entry_1[27] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_9),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[27]~q ),
	.prn(vcc));
defparam \entry_1[27] .is_wysiwyg = "true";
defparam \entry_1[27] .power_up = "low";

dffeas \entry_0[27] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_9),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[27]~q ),
	.prn(vcc));
defparam \entry_0[27] .is_wysiwyg = "true";
defparam \entry_0[27] .power_up = "low";

dffeas \entry_1[39] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_21),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[39]~q ),
	.prn(vcc));
defparam \entry_1[39] .is_wysiwyg = "true";
defparam \entry_1[39] .power_up = "low";

dffeas \entry_0[39] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_21),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[39]~q ),
	.prn(vcc));
defparam \entry_0[39] .is_wysiwyg = "true";
defparam \entry_0[39] .power_up = "low";

dffeas \entry_1[29] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_11),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[29]~q ),
	.prn(vcc));
defparam \entry_1[29] .is_wysiwyg = "true";
defparam \entry_1[29] .power_up = "low";

dffeas \entry_0[29] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_11),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[29]~q ),
	.prn(vcc));
defparam \entry_0[29] .is_wysiwyg = "true";
defparam \entry_0[29] .power_up = "low";

dffeas \entry_1[28] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_10),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[28]~q ),
	.prn(vcc));
defparam \entry_1[28] .is_wysiwyg = "true";
defparam \entry_1[28] .power_up = "low";

dffeas \entry_0[28] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_10),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[28]~q ),
	.prn(vcc));
defparam \entry_0[28] .is_wysiwyg = "true";
defparam \entry_0[28] .power_up = "low";

dffeas \entry_1[31] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_13),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[31]~q ),
	.prn(vcc));
defparam \entry_1[31] .is_wysiwyg = "true";
defparam \entry_1[31] .power_up = "low";

dffeas \entry_0[31] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_13),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[31]~q ),
	.prn(vcc));
defparam \entry_0[31] .is_wysiwyg = "true";
defparam \entry_0[31] .power_up = "low";

dffeas \entry_1[30] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_12),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[30]~q ),
	.prn(vcc));
defparam \entry_1[30] .is_wysiwyg = "true";
defparam \entry_1[30] .power_up = "low";

dffeas \entry_0[30] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_12),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[30]~q ),
	.prn(vcc));
defparam \entry_0[30] .is_wysiwyg = "true";
defparam \entry_0[30] .power_up = "low";

dffeas \entry_1[33] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_15),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[33]~q ),
	.prn(vcc));
defparam \entry_1[33] .is_wysiwyg = "true";
defparam \entry_1[33] .power_up = "low";

dffeas \entry_0[33] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_15),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[33]~q ),
	.prn(vcc));
defparam \entry_0[33] .is_wysiwyg = "true";
defparam \entry_0[33] .power_up = "low";

dffeas \entry_1[32] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_14),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[32]~q ),
	.prn(vcc));
defparam \entry_1[32] .is_wysiwyg = "true";
defparam \entry_1[32] .power_up = "low";

dffeas \entry_0[32] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_14),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[32]~q ),
	.prn(vcc));
defparam \entry_0[32] .is_wysiwyg = "true";
defparam \entry_0[32] .power_up = "low";

dffeas \entry_1[35] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_17),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[35]~q ),
	.prn(vcc));
defparam \entry_1[35] .is_wysiwyg = "true";
defparam \entry_1[35] .power_up = "low";

dffeas \entry_0[35] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_17),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[35]~q ),
	.prn(vcc));
defparam \entry_0[35] .is_wysiwyg = "true";
defparam \entry_0[35] .power_up = "low";

dffeas \entry_1[34] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_16),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[34]~q ),
	.prn(vcc));
defparam \entry_1[34] .is_wysiwyg = "true";
defparam \entry_1[34] .power_up = "low";

dffeas \entry_0[34] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_16),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[34]~q ),
	.prn(vcc));
defparam \entry_0[34] .is_wysiwyg = "true";
defparam \entry_0[34] .power_up = "low";

dffeas \entry_1[37] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_19),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[37]~q ),
	.prn(vcc));
defparam \entry_1[37] .is_wysiwyg = "true";
defparam \entry_1[37] .power_up = "low";

dffeas \entry_0[37] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_19),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[37]~q ),
	.prn(vcc));
defparam \entry_0[37] .is_wysiwyg = "true";
defparam \entry_0[37] .power_up = "low";

dffeas \entry_1[36] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_18),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[36]~q ),
	.prn(vcc));
defparam \entry_1[36] .is_wysiwyg = "true";
defparam \entry_1[36] .power_up = "low";

dffeas \entry_0[36] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_18),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[36]~q ),
	.prn(vcc));
defparam \entry_0[36] .is_wysiwyg = "true";
defparam \entry_0[36] .power_up = "low";

dffeas \entry_1[38] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_20),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[38]~q ),
	.prn(vcc));
defparam \entry_1[38] .is_wysiwyg = "true";
defparam \entry_1[38] .power_up = "low";

dffeas \entry_0[38] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_20),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[38]~q ),
	.prn(vcc));
defparam \entry_0[38] .is_wysiwyg = "true";
defparam \entry_0[38] .power_up = "low";

dffeas \entry_1[18] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_0),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[18]~q ),
	.prn(vcc));
defparam \entry_1[18] .is_wysiwyg = "true";
defparam \entry_1[18] .power_up = "low";

dffeas \entry_0[18] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_0),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[18]~q ),
	.prn(vcc));
defparam \entry_0[18] .is_wysiwyg = "true";
defparam \entry_0[18] .power_up = "low";

dffeas \entry_1[19] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[19]~q ),
	.prn(vcc));
defparam \entry_1[19] .is_wysiwyg = "true";
defparam \entry_1[19] .power_up = "low";

dffeas \entry_0[19] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[19]~q ),
	.prn(vcc));
defparam \entry_0[19] .is_wysiwyg = "true";
defparam \entry_0[19] .power_up = "low";

dffeas \entry_1[20] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[20]~q ),
	.prn(vcc));
defparam \entry_1[20] .is_wysiwyg = "true";
defparam \entry_1[20] .power_up = "low";

dffeas \entry_0[20] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[20]~q ),
	.prn(vcc));
defparam \entry_0[20] .is_wysiwyg = "true";
defparam \entry_0[20] .power_up = "low";

dffeas \entry_1[21] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_3),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[21]~q ),
	.prn(vcc));
defparam \entry_1[21] .is_wysiwyg = "true";
defparam \entry_1[21] .power_up = "low";

dffeas \entry_0[21] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_3),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[21]~q ),
	.prn(vcc));
defparam \entry_0[21] .is_wysiwyg = "true";
defparam \entry_0[21] .power_up = "low";

dffeas \entry_1[22] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_4),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[22]~q ),
	.prn(vcc));
defparam \entry_1[22] .is_wysiwyg = "true";
defparam \entry_1[22] .power_up = "low";

dffeas \entry_0[22] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_4),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[22]~q ),
	.prn(vcc));
defparam \entry_0[22] .is_wysiwyg = "true";
defparam \entry_0[22] .power_up = "low";

dffeas \entry_1[23] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_5),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[23]~q ),
	.prn(vcc));
defparam \entry_1[23] .is_wysiwyg = "true";
defparam \entry_1[23] .power_up = "low";

dffeas \entry_0[23] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_5),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[23]~q ),
	.prn(vcc));
defparam \entry_0[23] .is_wysiwyg = "true";
defparam \entry_0[23] .power_up = "low";

dffeas \entry_1[24] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_6),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[24]~q ),
	.prn(vcc));
defparam \entry_1[24] .is_wysiwyg = "true";
defparam \entry_1[24] .power_up = "low";

dffeas \entry_0[24] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_6),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[24]~q ),
	.prn(vcc));
defparam \entry_0[24] .is_wysiwyg = "true";
defparam \entry_0[24] .power_up = "low";

dffeas \entry_1[25] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_7),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[25]~q ),
	.prn(vcc));
defparam \entry_1[25] .is_wysiwyg = "true";
defparam \entry_1[25] .power_up = "low";

dffeas \entry_0[25] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_address_7),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[25]~q ),
	.prn(vcc));
defparam \entry_0[25] .is_wysiwyg = "true";
defparam \entry_0[25] .power_up = "low";

dffeas \entry_1[16] (
	.clk(clk),
	.d(comb1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[16]~q ),
	.prn(vcc));
defparam \entry_1[16] .is_wysiwyg = "true";
defparam \entry_1[16] .power_up = "low";

dffeas \entry_0[16] (
	.clk(clk),
	.d(comb1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[16]~q ),
	.prn(vcc));
defparam \entry_0[16] .is_wysiwyg = "true";
defparam \entry_0[16] .power_up = "low";

dffeas \entry_1[17] (
	.clk(clk),
	.d(comb2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[17]~q ),
	.prn(vcc));
defparam \entry_1[17] .is_wysiwyg = "true";
defparam \entry_1[17] .power_up = "low";

dffeas \entry_0[17] (
	.clk(clk),
	.d(comb2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[17]~q ),
	.prn(vcc));
defparam \entry_0[17] .is_wysiwyg = "true";
defparam \entry_0[17] .power_up = "low";

dffeas \entry_1[0] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_0),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[0]~q ),
	.prn(vcc));
defparam \entry_1[0] .is_wysiwyg = "true";
defparam \entry_1[0] .power_up = "low";

dffeas \entry_0[0] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_0),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[0]~q ),
	.prn(vcc));
defparam \entry_0[0] .is_wysiwyg = "true";
defparam \entry_0[0] .power_up = "low";

dffeas \entry_1[1] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[1]~q ),
	.prn(vcc));
defparam \entry_1[1] .is_wysiwyg = "true";
defparam \entry_1[1] .power_up = "low";

dffeas \entry_0[1] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_1),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[1]~q ),
	.prn(vcc));
defparam \entry_0[1] .is_wysiwyg = "true";
defparam \entry_0[1] .power_up = "low";

dffeas \entry_1[2] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[2]~q ),
	.prn(vcc));
defparam \entry_1[2] .is_wysiwyg = "true";
defparam \entry_1[2] .power_up = "low";

dffeas \entry_0[2] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_2),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[2]~q ),
	.prn(vcc));
defparam \entry_0[2] .is_wysiwyg = "true";
defparam \entry_0[2] .power_up = "low";

dffeas \entry_1[3] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_3),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[3]~q ),
	.prn(vcc));
defparam \entry_1[3] .is_wysiwyg = "true";
defparam \entry_1[3] .power_up = "low";

dffeas \entry_0[3] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_3),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[3]~q ),
	.prn(vcc));
defparam \entry_0[3] .is_wysiwyg = "true";
defparam \entry_0[3] .power_up = "low";

dffeas \entry_1[4] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_4),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[4]~q ),
	.prn(vcc));
defparam \entry_1[4] .is_wysiwyg = "true";
defparam \entry_1[4] .power_up = "low";

dffeas \entry_0[4] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_4),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[4]~q ),
	.prn(vcc));
defparam \entry_0[4] .is_wysiwyg = "true";
defparam \entry_0[4] .power_up = "low";

dffeas \entry_1[5] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_5),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[5]~q ),
	.prn(vcc));
defparam \entry_1[5] .is_wysiwyg = "true";
defparam \entry_1[5] .power_up = "low";

dffeas \entry_0[5] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_5),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[5]~q ),
	.prn(vcc));
defparam \entry_0[5] .is_wysiwyg = "true";
defparam \entry_0[5] .power_up = "low";

dffeas \entry_1[6] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_6),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[6]~q ),
	.prn(vcc));
defparam \entry_1[6] .is_wysiwyg = "true";
defparam \entry_1[6] .power_up = "low";

dffeas \entry_0[6] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_6),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[6]~q ),
	.prn(vcc));
defparam \entry_0[6] .is_wysiwyg = "true";
defparam \entry_0[6] .power_up = "low";

dffeas \entry_1[7] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_7),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[7]~q ),
	.prn(vcc));
defparam \entry_1[7] .is_wysiwyg = "true";
defparam \entry_1[7] .power_up = "low";

dffeas \entry_0[7] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_7),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[7]~q ),
	.prn(vcc));
defparam \entry_0[7] .is_wysiwyg = "true";
defparam \entry_0[7] .power_up = "low";

dffeas \entry_1[8] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_8),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[8]~q ),
	.prn(vcc));
defparam \entry_1[8] .is_wysiwyg = "true";
defparam \entry_1[8] .power_up = "low";

dffeas \entry_0[8] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_8),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[8]~q ),
	.prn(vcc));
defparam \entry_0[8] .is_wysiwyg = "true";
defparam \entry_0[8] .power_up = "low";

dffeas \entry_1[9] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_9),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[9]~q ),
	.prn(vcc));
defparam \entry_1[9] .is_wysiwyg = "true";
defparam \entry_1[9] .power_up = "low";

dffeas \entry_0[9] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_9),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[9]~q ),
	.prn(vcc));
defparam \entry_0[9] .is_wysiwyg = "true";
defparam \entry_0[9] .power_up = "low";

dffeas \entry_1[10] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_10),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[10]~q ),
	.prn(vcc));
defparam \entry_1[10] .is_wysiwyg = "true";
defparam \entry_1[10] .power_up = "low";

dffeas \entry_0[10] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_10),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[10]~q ),
	.prn(vcc));
defparam \entry_0[10] .is_wysiwyg = "true";
defparam \entry_0[10] .power_up = "low";

dffeas \entry_1[11] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_11),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[11]~q ),
	.prn(vcc));
defparam \entry_1[11] .is_wysiwyg = "true";
defparam \entry_1[11] .power_up = "low";

dffeas \entry_0[11] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_11),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[11]~q ),
	.prn(vcc));
defparam \entry_0[11] .is_wysiwyg = "true";
defparam \entry_0[11] .power_up = "low";

dffeas \entry_1[12] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_12),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[12]~q ),
	.prn(vcc));
defparam \entry_1[12] .is_wysiwyg = "true";
defparam \entry_1[12] .power_up = "low";

dffeas \entry_0[12] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_12),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[12]~q ),
	.prn(vcc));
defparam \entry_0[12] .is_wysiwyg = "true";
defparam \entry_0[12] .power_up = "low";

dffeas \entry_1[13] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_13),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[13]~q ),
	.prn(vcc));
defparam \entry_1[13] .is_wysiwyg = "true";
defparam \entry_1[13] .power_up = "low";

dffeas \entry_0[13] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_13),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[13]~q ),
	.prn(vcc));
defparam \entry_0[13] .is_wysiwyg = "true";
defparam \entry_0[13] .power_up = "low";

dffeas \entry_1[14] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_14),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[14]~q ),
	.prn(vcc));
defparam \entry_1[14] .is_wysiwyg = "true";
defparam \entry_1[14] .power_up = "low";

dffeas \entry_0[14] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_14),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[14]~q ),
	.prn(vcc));
defparam \entry_0[14] .is_wysiwyg = "true";
defparam \entry_0[14] .power_up = "low";

dffeas \entry_1[15] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_15),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_1[40]~0_combout ),
	.q(\entry_1[15]~q ),
	.prn(vcc));
defparam \entry_1[15] .is_wysiwyg = "true";
defparam \entry_1[15] .power_up = "low";

dffeas \entry_0[15] (
	.clk(clk),
	.d(new_sdram_controller_0_s1_writedata_15),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(\entry_0[40]~0_combout ),
	.q(\entry_0[15]~q ),
	.prn(vcc));
defparam \entry_0[15] .is_wysiwyg = "true";
defparam \entry_0[15] .power_up = "low";

endmodule
