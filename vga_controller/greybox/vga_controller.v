// Copyright (C) 2020  Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions 
// and other software and tools, and any partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License 
// Subscription Agreement, the Intel Quartus Prime License Agreement,
// the Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is for
// the sole purpose of programming logic devices manufactured by
// Intel and sold by Intel or its authorized distributors.  Please
// refer to the applicable agreement for further details, at
// https://fpgasoftware.intel.com/eula.

// VENDOR "Altera"
// PROGRAM "Quartus Prime"
// VERSION "Version 20.1.0 Build 711 06/05/2020 SJ Lite Edition"

// DATE "10/24/2020 21:28:46"

// 
// Device: Altera 10CL025YU256C8G Package UFBGA256
// 

// 
// This greybox netlist file is for third party Synthesis Tools
// for timing and resource estimation only.
// 


module vga_controller (
	video_vga_controller_0_avalon_vga_sink_data,
	video_vga_controller_0_avalon_vga_sink_startofpacket,
	video_vga_controller_0_avalon_vga_sink_endofpacket,
	video_vga_controller_0_avalon_vga_sink_valid,
	video_vga_controller_0_avalon_vga_sink_ready,
	video_vga_controller_0_clk_clk,
	video_vga_controller_0_external_interface_CLK,
	video_vga_controller_0_external_interface_HS,
	video_vga_controller_0_external_interface_VS,
	video_vga_controller_0_external_interface_BLANK,
	video_vga_controller_0_external_interface_SYNC,
	video_vga_controller_0_external_interface_R,
	video_vga_controller_0_external_interface_G,
	video_vga_controller_0_external_interface_B,
	video_vga_controller_0_reset_reset)/* synthesis synthesis_greybox=0 */;
input 	[29:0] video_vga_controller_0_avalon_vga_sink_data;
input 	video_vga_controller_0_avalon_vga_sink_startofpacket;
input 	video_vga_controller_0_avalon_vga_sink_endofpacket;
input 	video_vga_controller_0_avalon_vga_sink_valid;
output 	video_vga_controller_0_avalon_vga_sink_ready;
input 	video_vga_controller_0_clk_clk;
output 	video_vga_controller_0_external_interface_CLK;
output 	video_vga_controller_0_external_interface_HS;
output 	video_vga_controller_0_external_interface_VS;
output 	video_vga_controller_0_external_interface_BLANK;
output 	video_vga_controller_0_external_interface_SYNC;
output 	[3:0] video_vga_controller_0_external_interface_R;
output 	[3:0] video_vga_controller_0_external_interface_G;
output 	[3:0] video_vga_controller_0_external_interface_B;
input 	video_vga_controller_0_reset_reset;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \video_vga_controller_0|ready~0_combout ;
wire \video_vga_controller_0|VGA_HS~q ;
wire \video_vga_controller_0|VGA_VS~q ;
wire \video_vga_controller_0|VGA_BLANK~q ;
wire \video_vga_controller_0|VGA_R[0]~q ;
wire \video_vga_controller_0|VGA_R[1]~q ;
wire \video_vga_controller_0|VGA_R[2]~q ;
wire \video_vga_controller_0|VGA_R[3]~q ;
wire \video_vga_controller_0|VGA_G[0]~q ;
wire \video_vga_controller_0|VGA_G[1]~q ;
wire \video_vga_controller_0|VGA_G[2]~q ;
wire \video_vga_controller_0|VGA_G[3]~q ;
wire \video_vga_controller_0|VGA_B[0]~q ;
wire \video_vga_controller_0|VGA_B[1]~q ;
wire \video_vga_controller_0|VGA_B[2]~q ;
wire \video_vga_controller_0|VGA_B[3]~q ;
wire \~GND~combout ;
wire \video_vga_controller_0_avalon_vga_sink_data[0]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[1]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[2]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[3]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[4]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[5]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[10]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[11]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[12]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[13]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[14]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[15]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[20]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[21]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[22]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[23]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[24]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[25]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_endofpacket~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_valid~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_startofpacket~input_o ;
wire \video_vga_controller_0_reset_reset~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[26]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[27]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[28]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[29]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[16]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[17]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[18]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[19]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[6]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[7]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[8]~input_o ;
wire \video_vga_controller_0_avalon_vga_sink_data[9]~input_o ;
wire \video_vga_controller_0_clk_clk~input_o ;


vga_controller_vga_controller_video_vga_controller_0 video_vga_controller_0(
	.ready(\video_vga_controller_0|ready~0_combout ),
	.VGA_HS1(\video_vga_controller_0|VGA_HS~q ),
	.VGA_VS1(\video_vga_controller_0|VGA_VS~q ),
	.VGA_BLANK1(\video_vga_controller_0|VGA_BLANK~q ),
	.VGA_R_0(\video_vga_controller_0|VGA_R[0]~q ),
	.VGA_R_1(\video_vga_controller_0|VGA_R[1]~q ),
	.VGA_R_2(\video_vga_controller_0|VGA_R[2]~q ),
	.VGA_R_3(\video_vga_controller_0|VGA_R[3]~q ),
	.VGA_G_0(\video_vga_controller_0|VGA_G[0]~q ),
	.VGA_G_1(\video_vga_controller_0|VGA_G[1]~q ),
	.VGA_G_2(\video_vga_controller_0|VGA_G[2]~q ),
	.VGA_G_3(\video_vga_controller_0|VGA_G[3]~q ),
	.VGA_B_0(\video_vga_controller_0|VGA_B[0]~q ),
	.VGA_B_1(\video_vga_controller_0|VGA_B[1]~q ),
	.VGA_B_2(\video_vga_controller_0|VGA_B[2]~q ),
	.VGA_B_3(\video_vga_controller_0|VGA_B[3]~q ),
	.GND_port(\~GND~combout ),
	.video_vga_controller_0_avalon_vga_sink_valid(\video_vga_controller_0_avalon_vga_sink_valid~input_o ),
	.video_vga_controller_0_avalon_vga_sink_startofpacket(\video_vga_controller_0_avalon_vga_sink_startofpacket~input_o ),
	.video_vga_controller_0_clk_clk(\video_vga_controller_0_clk_clk~input_o ),
	.video_vga_controller_0_reset_reset(\video_vga_controller_0_reset_reset~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_26(\video_vga_controller_0_avalon_vga_sink_data[26]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_27(\video_vga_controller_0_avalon_vga_sink_data[27]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_28(\video_vga_controller_0_avalon_vga_sink_data[28]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_29(\video_vga_controller_0_avalon_vga_sink_data[29]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_16(\video_vga_controller_0_avalon_vga_sink_data[16]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_17(\video_vga_controller_0_avalon_vga_sink_data[17]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_18(\video_vga_controller_0_avalon_vga_sink_data[18]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_19(\video_vga_controller_0_avalon_vga_sink_data[19]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_6(\video_vga_controller_0_avalon_vga_sink_data[6]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_7(\video_vga_controller_0_avalon_vga_sink_data[7]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_8(\video_vga_controller_0_avalon_vga_sink_data[8]~input_o ),
	.video_vga_controller_0_avalon_vga_sink_data_9(\video_vga_controller_0_avalon_vga_sink_data[9]~input_o ));

cyclone10lp_lcell_comb \~GND (
	.dataa(gnd),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\~GND~combout ),
	.cout());
defparam \~GND .lut_mask = 16'h0000;
defparam \~GND .sum_lutc_input = "datac";

assign \video_vga_controller_0_avalon_vga_sink_valid~input_o  = video_vga_controller_0_avalon_vga_sink_valid;

assign \video_vga_controller_0_avalon_vga_sink_startofpacket~input_o  = video_vga_controller_0_avalon_vga_sink_startofpacket;

assign \video_vga_controller_0_reset_reset~input_o  = video_vga_controller_0_reset_reset;

assign \video_vga_controller_0_avalon_vga_sink_data[26]~input_o  = video_vga_controller_0_avalon_vga_sink_data[26];

assign \video_vga_controller_0_avalon_vga_sink_data[27]~input_o  = video_vga_controller_0_avalon_vga_sink_data[27];

assign \video_vga_controller_0_avalon_vga_sink_data[28]~input_o  = video_vga_controller_0_avalon_vga_sink_data[28];

assign \video_vga_controller_0_avalon_vga_sink_data[29]~input_o  = video_vga_controller_0_avalon_vga_sink_data[29];

assign \video_vga_controller_0_avalon_vga_sink_data[16]~input_o  = video_vga_controller_0_avalon_vga_sink_data[16];

assign \video_vga_controller_0_avalon_vga_sink_data[17]~input_o  = video_vga_controller_0_avalon_vga_sink_data[17];

assign \video_vga_controller_0_avalon_vga_sink_data[18]~input_o  = video_vga_controller_0_avalon_vga_sink_data[18];

assign \video_vga_controller_0_avalon_vga_sink_data[19]~input_o  = video_vga_controller_0_avalon_vga_sink_data[19];

assign \video_vga_controller_0_avalon_vga_sink_data[6]~input_o  = video_vga_controller_0_avalon_vga_sink_data[6];

assign \video_vga_controller_0_avalon_vga_sink_data[7]~input_o  = video_vga_controller_0_avalon_vga_sink_data[7];

assign \video_vga_controller_0_avalon_vga_sink_data[8]~input_o  = video_vga_controller_0_avalon_vga_sink_data[8];

assign \video_vga_controller_0_avalon_vga_sink_data[9]~input_o  = video_vga_controller_0_avalon_vga_sink_data[9];

assign video_vga_controller_0_avalon_vga_sink_ready = \video_vga_controller_0|ready~0_combout ;

assign video_vga_controller_0_external_interface_CLK = ~ \video_vga_controller_0_clk_clk~input_o ;

assign video_vga_controller_0_external_interface_HS = \video_vga_controller_0|VGA_HS~q ;

assign video_vga_controller_0_external_interface_VS = \video_vga_controller_0|VGA_VS~q ;

assign video_vga_controller_0_external_interface_BLANK = \video_vga_controller_0|VGA_BLANK~q ;

assign video_vga_controller_0_external_interface_SYNC = gnd;

assign video_vga_controller_0_external_interface_R[0] = \video_vga_controller_0|VGA_R[0]~q ;

assign video_vga_controller_0_external_interface_R[1] = \video_vga_controller_0|VGA_R[1]~q ;

assign video_vga_controller_0_external_interface_R[2] = \video_vga_controller_0|VGA_R[2]~q ;

assign video_vga_controller_0_external_interface_R[3] = \video_vga_controller_0|VGA_R[3]~q ;

assign video_vga_controller_0_external_interface_G[0] = \video_vga_controller_0|VGA_G[0]~q ;

assign video_vga_controller_0_external_interface_G[1] = \video_vga_controller_0|VGA_G[1]~q ;

assign video_vga_controller_0_external_interface_G[2] = \video_vga_controller_0|VGA_G[2]~q ;

assign video_vga_controller_0_external_interface_G[3] = \video_vga_controller_0|VGA_G[3]~q ;

assign video_vga_controller_0_external_interface_B[0] = \video_vga_controller_0|VGA_B[0]~q ;

assign video_vga_controller_0_external_interface_B[1] = \video_vga_controller_0|VGA_B[1]~q ;

assign video_vga_controller_0_external_interface_B[2] = \video_vga_controller_0|VGA_B[2]~q ;

assign video_vga_controller_0_external_interface_B[3] = \video_vga_controller_0|VGA_B[3]~q ;

assign \video_vga_controller_0_clk_clk~input_o  = video_vga_controller_0_clk_clk;

assign \video_vga_controller_0_avalon_vga_sink_data[0]~input_o  = video_vga_controller_0_avalon_vga_sink_data[0];

assign \video_vga_controller_0_avalon_vga_sink_data[1]~input_o  = video_vga_controller_0_avalon_vga_sink_data[1];

assign \video_vga_controller_0_avalon_vga_sink_data[2]~input_o  = video_vga_controller_0_avalon_vga_sink_data[2];

assign \video_vga_controller_0_avalon_vga_sink_data[3]~input_o  = video_vga_controller_0_avalon_vga_sink_data[3];

assign \video_vga_controller_0_avalon_vga_sink_data[4]~input_o  = video_vga_controller_0_avalon_vga_sink_data[4];

assign \video_vga_controller_0_avalon_vga_sink_data[5]~input_o  = video_vga_controller_0_avalon_vga_sink_data[5];

assign \video_vga_controller_0_avalon_vga_sink_data[10]~input_o  = video_vga_controller_0_avalon_vga_sink_data[10];

assign \video_vga_controller_0_avalon_vga_sink_data[11]~input_o  = video_vga_controller_0_avalon_vga_sink_data[11];

assign \video_vga_controller_0_avalon_vga_sink_data[12]~input_o  = video_vga_controller_0_avalon_vga_sink_data[12];

assign \video_vga_controller_0_avalon_vga_sink_data[13]~input_o  = video_vga_controller_0_avalon_vga_sink_data[13];

assign \video_vga_controller_0_avalon_vga_sink_data[14]~input_o  = video_vga_controller_0_avalon_vga_sink_data[14];

assign \video_vga_controller_0_avalon_vga_sink_data[15]~input_o  = video_vga_controller_0_avalon_vga_sink_data[15];

assign \video_vga_controller_0_avalon_vga_sink_data[20]~input_o  = video_vga_controller_0_avalon_vga_sink_data[20];

assign \video_vga_controller_0_avalon_vga_sink_data[21]~input_o  = video_vga_controller_0_avalon_vga_sink_data[21];

assign \video_vga_controller_0_avalon_vga_sink_data[22]~input_o  = video_vga_controller_0_avalon_vga_sink_data[22];

assign \video_vga_controller_0_avalon_vga_sink_data[23]~input_o  = video_vga_controller_0_avalon_vga_sink_data[23];

assign \video_vga_controller_0_avalon_vga_sink_data[24]~input_o  = video_vga_controller_0_avalon_vga_sink_data[24];

assign \video_vga_controller_0_avalon_vga_sink_data[25]~input_o  = video_vga_controller_0_avalon_vga_sink_data[25];

assign \video_vga_controller_0_avalon_vga_sink_endofpacket~input_o  = video_vga_controller_0_avalon_vga_sink_endofpacket;

endmodule

module vga_controller_vga_controller_video_vga_controller_0 (
	ready,
	VGA_HS1,
	VGA_VS1,
	VGA_BLANK1,
	VGA_R_0,
	VGA_R_1,
	VGA_R_2,
	VGA_R_3,
	VGA_G_0,
	VGA_G_1,
	VGA_G_2,
	VGA_G_3,
	VGA_B_0,
	VGA_B_1,
	VGA_B_2,
	VGA_B_3,
	GND_port,
	video_vga_controller_0_avalon_vga_sink_valid,
	video_vga_controller_0_avalon_vga_sink_startofpacket,
	video_vga_controller_0_clk_clk,
	video_vga_controller_0_reset_reset,
	video_vga_controller_0_avalon_vga_sink_data_26,
	video_vga_controller_0_avalon_vga_sink_data_27,
	video_vga_controller_0_avalon_vga_sink_data_28,
	video_vga_controller_0_avalon_vga_sink_data_29,
	video_vga_controller_0_avalon_vga_sink_data_16,
	video_vga_controller_0_avalon_vga_sink_data_17,
	video_vga_controller_0_avalon_vga_sink_data_18,
	video_vga_controller_0_avalon_vga_sink_data_19,
	video_vga_controller_0_avalon_vga_sink_data_6,
	video_vga_controller_0_avalon_vga_sink_data_7,
	video_vga_controller_0_avalon_vga_sink_data_8,
	video_vga_controller_0_avalon_vga_sink_data_9)/* synthesis synthesis_greybox=0 */;
output 	ready;
output 	VGA_HS1;
output 	VGA_VS1;
output 	VGA_BLANK1;
output 	VGA_R_0;
output 	VGA_R_1;
output 	VGA_R_2;
output 	VGA_R_3;
output 	VGA_G_0;
output 	VGA_G_1;
output 	VGA_G_2;
output 	VGA_G_3;
output 	VGA_B_0;
output 	VGA_B_1;
output 	VGA_B_2;
output 	VGA_B_3;
input 	GND_port;
input 	video_vga_controller_0_avalon_vga_sink_valid;
input 	video_vga_controller_0_avalon_vga_sink_startofpacket;
input 	video_vga_controller_0_clk_clk;
input 	video_vga_controller_0_reset_reset;
input 	video_vga_controller_0_avalon_vga_sink_data_26;
input 	video_vga_controller_0_avalon_vga_sink_data_27;
input 	video_vga_controller_0_avalon_vga_sink_data_28;
input 	video_vga_controller_0_avalon_vga_sink_data_29;
input 	video_vga_controller_0_avalon_vga_sink_data_16;
input 	video_vga_controller_0_avalon_vga_sink_data_17;
input 	video_vga_controller_0_avalon_vga_sink_data_18;
input 	video_vga_controller_0_avalon_vga_sink_data_19;
input 	video_vga_controller_0_avalon_vga_sink_data_6;
input 	video_vga_controller_0_avalon_vga_sink_data_7;
input 	video_vga_controller_0_avalon_vga_sink_data_8;
input 	video_vga_controller_0_avalon_vga_sink_data_9;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \VGA_Timing|blanking_pulse~q ;
wire \VGA_Timing|end_of_active_frame~q ;
wire \VGA_Timing|vga_h_sync~q ;
wire \VGA_Timing|vga_v_sync~q ;
wire \VGA_Timing|vga_blank~q ;
wire \VGA_Timing|vga_red[0]~q ;
wire \VGA_Timing|vga_red[1]~q ;
wire \VGA_Timing|vga_red[2]~q ;
wire \VGA_Timing|vga_red[3]~q ;
wire \VGA_Timing|vga_green[0]~q ;
wire \VGA_Timing|vga_green[1]~q ;
wire \VGA_Timing|vga_green[2]~q ;
wire \VGA_Timing|vga_green[3]~q ;
wire \VGA_Timing|vga_blue[0]~q ;
wire \VGA_Timing|vga_blue[1]~q ;
wire \VGA_Timing|vga_blue[2]~q ;
wire \VGA_Timing|vga_blue[3]~q ;
wire \ns_mode~0_combout ;
wire \s_mode~q ;


vga_controller_altera_up_avalon_video_vga_timing VGA_Timing(
	.blanking_pulse1(\VGA_Timing|blanking_pulse~q ),
	.end_of_active_frame1(\VGA_Timing|end_of_active_frame~q ),
	.vga_h_sync1(\VGA_Timing|vga_h_sync~q ),
	.vga_v_sync1(\VGA_Timing|vga_v_sync~q ),
	.vga_blank1(\VGA_Timing|vga_blank~q ),
	.vga_red_0(\VGA_Timing|vga_red[0]~q ),
	.vga_red_1(\VGA_Timing|vga_red[1]~q ),
	.vga_red_2(\VGA_Timing|vga_red[2]~q ),
	.vga_red_3(\VGA_Timing|vga_red[3]~q ),
	.vga_green_0(\VGA_Timing|vga_green[0]~q ),
	.vga_green_1(\VGA_Timing|vga_green[1]~q ),
	.vga_green_2(\VGA_Timing|vga_green[2]~q ),
	.vga_green_3(\VGA_Timing|vga_green[3]~q ),
	.vga_blue_0(\VGA_Timing|vga_blue[0]~q ),
	.vga_blue_1(\VGA_Timing|vga_blue[1]~q ),
	.vga_blue_2(\VGA_Timing|vga_blue[2]~q ),
	.vga_blue_3(\VGA_Timing|vga_blue[3]~q ),
	.GND_port(GND_port),
	.clk(video_vga_controller_0_clk_clk),
	.video_vga_controller_0_reset_reset(video_vga_controller_0_reset_reset),
	.video_vga_controller_0_avalon_vga_sink_data_26(video_vga_controller_0_avalon_vga_sink_data_26),
	.video_vga_controller_0_avalon_vga_sink_data_27(video_vga_controller_0_avalon_vga_sink_data_27),
	.video_vga_controller_0_avalon_vga_sink_data_28(video_vga_controller_0_avalon_vga_sink_data_28),
	.video_vga_controller_0_avalon_vga_sink_data_29(video_vga_controller_0_avalon_vga_sink_data_29),
	.video_vga_controller_0_avalon_vga_sink_data_16(video_vga_controller_0_avalon_vga_sink_data_16),
	.video_vga_controller_0_avalon_vga_sink_data_17(video_vga_controller_0_avalon_vga_sink_data_17),
	.video_vga_controller_0_avalon_vga_sink_data_18(video_vga_controller_0_avalon_vga_sink_data_18),
	.video_vga_controller_0_avalon_vga_sink_data_19(video_vga_controller_0_avalon_vga_sink_data_19),
	.video_vga_controller_0_avalon_vga_sink_data_6(video_vga_controller_0_avalon_vga_sink_data_6),
	.video_vga_controller_0_avalon_vga_sink_data_7(video_vga_controller_0_avalon_vga_sink_data_7),
	.video_vga_controller_0_avalon_vga_sink_data_8(video_vga_controller_0_avalon_vga_sink_data_8),
	.video_vga_controller_0_avalon_vga_sink_data_9(video_vga_controller_0_avalon_vga_sink_data_9));

cyclone10lp_lcell_comb \ready~0 (
	.dataa(video_vga_controller_0_avalon_vga_sink_valid),
	.datab(\s_mode~q ),
	.datac(video_vga_controller_0_avalon_vga_sink_startofpacket),
	.datad(\VGA_Timing|blanking_pulse~q ),
	.cin(gnd),
	.combout(ready),
	.cout());
defparam \ready~0 .lut_mask = 16'h02CE;
defparam \ready~0 .sum_lutc_input = "datac";

dffeas VGA_HS(
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_h_sync~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_HS1),
	.prn(vcc));
defparam VGA_HS.is_wysiwyg = "true";
defparam VGA_HS.power_up = "low";

dffeas VGA_VS(
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_v_sync~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_VS1),
	.prn(vcc));
defparam VGA_VS.is_wysiwyg = "true";
defparam VGA_VS.power_up = "low";

dffeas VGA_BLANK(
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_blank~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_BLANK1),
	.prn(vcc));
defparam VGA_BLANK.is_wysiwyg = "true";
defparam VGA_BLANK.power_up = "low";

dffeas \VGA_R[0] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_red[0]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_R_0),
	.prn(vcc));
defparam \VGA_R[0] .is_wysiwyg = "true";
defparam \VGA_R[0] .power_up = "low";

dffeas \VGA_R[1] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_red[1]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_R_1),
	.prn(vcc));
defparam \VGA_R[1] .is_wysiwyg = "true";
defparam \VGA_R[1] .power_up = "low";

dffeas \VGA_R[2] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_red[2]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_R_2),
	.prn(vcc));
defparam \VGA_R[2] .is_wysiwyg = "true";
defparam \VGA_R[2] .power_up = "low";

dffeas \VGA_R[3] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_red[3]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_R_3),
	.prn(vcc));
defparam \VGA_R[3] .is_wysiwyg = "true";
defparam \VGA_R[3] .power_up = "low";

dffeas \VGA_G[0] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_green[0]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_G_0),
	.prn(vcc));
defparam \VGA_G[0] .is_wysiwyg = "true";
defparam \VGA_G[0] .power_up = "low";

dffeas \VGA_G[1] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_green[1]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_G_1),
	.prn(vcc));
defparam \VGA_G[1] .is_wysiwyg = "true";
defparam \VGA_G[1] .power_up = "low";

dffeas \VGA_G[2] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_green[2]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_G_2),
	.prn(vcc));
defparam \VGA_G[2] .is_wysiwyg = "true";
defparam \VGA_G[2] .power_up = "low";

dffeas \VGA_G[3] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_green[3]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_G_3),
	.prn(vcc));
defparam \VGA_G[3] .is_wysiwyg = "true";
defparam \VGA_G[3] .power_up = "low";

dffeas \VGA_B[0] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_blue[0]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_B_0),
	.prn(vcc));
defparam \VGA_B[0] .is_wysiwyg = "true";
defparam \VGA_B[0] .power_up = "low";

dffeas \VGA_B[1] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_blue[1]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_B_1),
	.prn(vcc));
defparam \VGA_B[1] .is_wysiwyg = "true";
defparam \VGA_B[1] .power_up = "low";

dffeas \VGA_B[2] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_blue[2]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_B_2),
	.prn(vcc));
defparam \VGA_B[2] .is_wysiwyg = "true";
defparam \VGA_B[2] .power_up = "low";

dffeas \VGA_B[3] (
	.clk(video_vga_controller_0_clk_clk),
	.d(\VGA_Timing|vga_blue[3]~q ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(VGA_B_3),
	.prn(vcc));
defparam \VGA_B[3] .is_wysiwyg = "true";
defparam \VGA_B[3] .power_up = "low";

cyclone10lp_lcell_comb \ns_mode~0 (
	.dataa(video_vga_controller_0_avalon_vga_sink_startofpacket),
	.datab(video_vga_controller_0_avalon_vga_sink_valid),
	.datac(\s_mode~q ),
	.datad(\VGA_Timing|end_of_active_frame~q ),
	.cin(gnd),
	.combout(\ns_mode~0_combout ),
	.cout());
defparam \ns_mode~0 .lut_mask = 16'h08F8;
defparam \ns_mode~0 .sum_lutc_input = "datac";

dffeas s_mode(
	.clk(video_vga_controller_0_clk_clk),
	.d(\ns_mode~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(video_vga_controller_0_reset_reset),
	.sload(gnd),
	.ena(vcc),
	.q(\s_mode~q ),
	.prn(vcc));
defparam s_mode.is_wysiwyg = "true";
defparam s_mode.power_up = "low";

endmodule

module vga_controller_altera_up_avalon_video_vga_timing (
	blanking_pulse1,
	end_of_active_frame1,
	vga_h_sync1,
	vga_v_sync1,
	vga_blank1,
	vga_red_0,
	vga_red_1,
	vga_red_2,
	vga_red_3,
	vga_green_0,
	vga_green_1,
	vga_green_2,
	vga_green_3,
	vga_blue_0,
	vga_blue_1,
	vga_blue_2,
	vga_blue_3,
	GND_port,
	clk,
	video_vga_controller_0_reset_reset,
	video_vga_controller_0_avalon_vga_sink_data_26,
	video_vga_controller_0_avalon_vga_sink_data_27,
	video_vga_controller_0_avalon_vga_sink_data_28,
	video_vga_controller_0_avalon_vga_sink_data_29,
	video_vga_controller_0_avalon_vga_sink_data_16,
	video_vga_controller_0_avalon_vga_sink_data_17,
	video_vga_controller_0_avalon_vga_sink_data_18,
	video_vga_controller_0_avalon_vga_sink_data_19,
	video_vga_controller_0_avalon_vga_sink_data_6,
	video_vga_controller_0_avalon_vga_sink_data_7,
	video_vga_controller_0_avalon_vga_sink_data_8,
	video_vga_controller_0_avalon_vga_sink_data_9)/* synthesis synthesis_greybox=0 */;
output 	blanking_pulse1;
output 	end_of_active_frame1;
output 	vga_h_sync1;
output 	vga_v_sync1;
output 	vga_blank1;
output 	vga_red_0;
output 	vga_red_1;
output 	vga_red_2;
output 	vga_red_3;
output 	vga_green_0;
output 	vga_green_1;
output 	vga_green_2;
output 	vga_green_3;
output 	vga_blue_0;
output 	vga_blue_1;
output 	vga_blue_2;
output 	vga_blue_3;
input 	GND_port;
input 	clk;
input 	video_vga_controller_0_reset_reset;
input 	video_vga_controller_0_avalon_vga_sink_data_26;
input 	video_vga_controller_0_avalon_vga_sink_data_27;
input 	video_vga_controller_0_avalon_vga_sink_data_28;
input 	video_vga_controller_0_avalon_vga_sink_data_29;
input 	video_vga_controller_0_avalon_vga_sink_data_16;
input 	video_vga_controller_0_avalon_vga_sink_data_17;
input 	video_vga_controller_0_avalon_vga_sink_data_18;
input 	video_vga_controller_0_avalon_vga_sink_data_19;
input 	video_vga_controller_0_avalon_vga_sink_data_6;
input 	video_vga_controller_0_avalon_vga_sink_data_7;
input 	video_vga_controller_0_avalon_vga_sink_data_8;
input 	video_vga_controller_0_avalon_vga_sink_data_9;

wire gnd;
wire vcc;
wire unknown;

assign gnd = 1'b0;
assign vcc = 1'b1;
// unknown value (1'bx) is not needed for this tool. Default to 1'b0
assign unknown = 1'b0;

wire \pixel_counter[1]~11_combout ;
wire \pixel_counter[2]~14 ;
wire \pixel_counter[3]~16_combout ;
wire \pixel_counter[3]~q ;
wire \pixel_counter[3]~17 ;
wire \pixel_counter[4]~18_combout ;
wire \pixel_counter[4]~q ;
wire \pixel_counter[4]~19 ;
wire \pixel_counter[5]~20_combout ;
wire \pixel_counter[5]~q ;
wire \pixel_counter[5]~21 ;
wire \pixel_counter[6]~22_combout ;
wire \pixel_counter[6]~q ;
wire \pixel_counter[6]~23 ;
wire \pixel_counter[7]~24_combout ;
wire \pixel_counter[7]~q ;
wire \pixel_counter[7]~25 ;
wire \pixel_counter[8]~26_combout ;
wire \pixel_counter[8]~q ;
wire \pixel_counter[8]~27 ;
wire \pixel_counter[9]~28_combout ;
wire \pixel_counter[9]~q ;
wire \pixel_counter[9]~29 ;
wire \pixel_counter[10]~30_combout ;
wire \pixel_counter[10]~q ;
wire \pixel_counter[10]~31 ;
wire \pixel_counter[11]~32_combout ;
wire \pixel_counter[11]~q ;
wire \Equal4~2_combout ;
wire \Equal4~6_combout ;
wire \Equal4~4_combout ;
wire \pixel_counter[5]~15_combout ;
wire \pixel_counter[1]~q ;
wire \pixel_counter[1]~12 ;
wire \pixel_counter[2]~13_combout ;
wire \pixel_counter[2]~q ;
wire \Equal2~0_combout ;
wire \Equal2~1_combout ;
wire \Equal2~2_combout ;
wire \Equal4~3_combout ;
wire \Equal4~5_combout ;
wire \hblanking_pulse~0_combout ;
wire \hblanking_pulse~q ;
wire \line_counter[1]~11_combout ;
wire \line_counter[2]~14 ;
wire \line_counter[3]~16_combout ;
wire \line_counter[3]~q ;
wire \line_counter[3]~17 ;
wire \line_counter[4]~18_combout ;
wire \line_counter[4]~q ;
wire \line_counter[4]~19 ;
wire \line_counter[5]~20_combout ;
wire \line_counter[5]~q ;
wire \line_counter[5]~21 ;
wire \line_counter[6]~22_combout ;
wire \line_counter[6]~q ;
wire \Equal1~0_combout ;
wire \line_counter[6]~23 ;
wire \line_counter[7]~24_combout ;
wire \line_counter[7]~q ;
wire \line_counter[7]~25 ;
wire \line_counter[8]~26_combout ;
wire \line_counter[8]~q ;
wire \line_counter[8]~27 ;
wire \line_counter[9]~28_combout ;
wire \line_counter[9]~q ;
wire \line_counter[9]~29 ;
wire \line_counter[10]~30_combout ;
wire \line_counter[10]~q ;
wire \line_counter[10]~31 ;
wire \line_counter[11]~32_combout ;
wire \line_counter[11]~q ;
wire \always3~0_combout ;
wire \always3~1_combout ;
wire \line_counter[7]~15_combout ;
wire \line_counter[1]~q ;
wire \line_counter[1]~12 ;
wire \line_counter[2]~13_combout ;
wire \line_counter[2]~q ;
wire \Equal1~1_combout ;
wire \Equal1~2_combout ;
wire \Equal1~3_combout ;
wire \vblanking_pulse~0_combout ;
wire \vblanking_pulse~1_combout ;
wire \vblanking_pulse~q ;
wire \blanking_pulse~0_combout ;
wire \always2~0_combout ;
wire \Equal6~0_combout ;
wire \early_hsync_pulse~0_combout ;
wire \Equal5~0_combout ;
wire \early_hsync_pulse~1_combout ;
wire \early_hsync_pulse~q ;
wire \hsync_pulse~0_combout ;
wire \hsync_pulse~q ;
wire \vga_h_sync~0_combout ;
wire \always3~2_combout ;
wire \early_vsync_pulse~0_combout ;
wire \early_vsync_pulse~q ;
wire \vsync_pulse~0_combout ;
wire \vsync_pulse~q ;
wire \vga_v_sync~0_combout ;
wire \vga_blank~0_combout ;
wire \vga_red~0_combout ;
wire \vga_red~1_combout ;
wire \vga_red~2_combout ;
wire \vga_red~3_combout ;
wire \vga_green~0_combout ;
wire \vga_green~1_combout ;
wire \vga_green~2_combout ;
wire \vga_green~3_combout ;
wire \vga_blue~0_combout ;
wire \vga_blue~1_combout ;
wire \vga_blue~2_combout ;
wire \vga_blue~3_combout ;


dffeas blanking_pulse(
	.clk(clk),
	.d(\blanking_pulse~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(video_vga_controller_0_reset_reset),
	.ena(vcc),
	.q(blanking_pulse1),
	.prn(vcc));
defparam blanking_pulse.is_wysiwyg = "true";
defparam blanking_pulse.power_up = "low";

dffeas end_of_active_frame(
	.clk(clk),
	.d(\always2~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(video_vga_controller_0_reset_reset),
	.sload(gnd),
	.ena(vcc),
	.q(end_of_active_frame1),
	.prn(vcc));
defparam end_of_active_frame.is_wysiwyg = "true";
defparam end_of_active_frame.power_up = "low";

dffeas vga_h_sync(
	.clk(clk),
	.d(\vga_h_sync~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_h_sync1),
	.prn(vcc));
defparam vga_h_sync.is_wysiwyg = "true";
defparam vga_h_sync.power_up = "low";

dffeas vga_v_sync(
	.clk(clk),
	.d(\vga_v_sync~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_v_sync1),
	.prn(vcc));
defparam vga_v_sync.is_wysiwyg = "true";
defparam vga_v_sync.power_up = "low";

dffeas vga_blank(
	.clk(clk),
	.d(\vga_blank~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_blank1),
	.prn(vcc));
defparam vga_blank.is_wysiwyg = "true";
defparam vga_blank.power_up = "low";

dffeas \vga_red[0] (
	.clk(clk),
	.d(\vga_red~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_red_0),
	.prn(vcc));
defparam \vga_red[0] .is_wysiwyg = "true";
defparam \vga_red[0] .power_up = "low";

dffeas \vga_red[1] (
	.clk(clk),
	.d(\vga_red~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_red_1),
	.prn(vcc));
defparam \vga_red[1] .is_wysiwyg = "true";
defparam \vga_red[1] .power_up = "low";

dffeas \vga_red[2] (
	.clk(clk),
	.d(\vga_red~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_red_2),
	.prn(vcc));
defparam \vga_red[2] .is_wysiwyg = "true";
defparam \vga_red[2] .power_up = "low";

dffeas \vga_red[3] (
	.clk(clk),
	.d(\vga_red~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_red_3),
	.prn(vcc));
defparam \vga_red[3] .is_wysiwyg = "true";
defparam \vga_red[3] .power_up = "low";

dffeas \vga_green[0] (
	.clk(clk),
	.d(\vga_green~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_green_0),
	.prn(vcc));
defparam \vga_green[0] .is_wysiwyg = "true";
defparam \vga_green[0] .power_up = "low";

dffeas \vga_green[1] (
	.clk(clk),
	.d(\vga_green~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_green_1),
	.prn(vcc));
defparam \vga_green[1] .is_wysiwyg = "true";
defparam \vga_green[1] .power_up = "low";

dffeas \vga_green[2] (
	.clk(clk),
	.d(\vga_green~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_green_2),
	.prn(vcc));
defparam \vga_green[2] .is_wysiwyg = "true";
defparam \vga_green[2] .power_up = "low";

dffeas \vga_green[3] (
	.clk(clk),
	.d(\vga_green~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_green_3),
	.prn(vcc));
defparam \vga_green[3] .is_wysiwyg = "true";
defparam \vga_green[3] .power_up = "low";

dffeas \vga_blue[0] (
	.clk(clk),
	.d(\vga_blue~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_blue_0),
	.prn(vcc));
defparam \vga_blue[0] .is_wysiwyg = "true";
defparam \vga_blue[0] .power_up = "low";

dffeas \vga_blue[1] (
	.clk(clk),
	.d(\vga_blue~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_blue_1),
	.prn(vcc));
defparam \vga_blue[1] .is_wysiwyg = "true";
defparam \vga_blue[1] .power_up = "low";

dffeas \vga_blue[2] (
	.clk(clk),
	.d(\vga_blue~2_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_blue_2),
	.prn(vcc));
defparam \vga_blue[2] .is_wysiwyg = "true";
defparam \vga_blue[2] .power_up = "low";

dffeas \vga_blue[3] (
	.clk(clk),
	.d(\vga_blue~3_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(vga_blue_3),
	.prn(vcc));
defparam \vga_blue[3] .is_wysiwyg = "true";
defparam \vga_blue[3] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[1]~11 (
	.dataa(\pixel_counter[1]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(\pixel_counter[1]~11_combout ),
	.cout(\pixel_counter[1]~12 ));
defparam \pixel_counter[1]~11 .lut_mask = 16'h55AA;
defparam \pixel_counter[1]~11 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \pixel_counter[2]~13 (
	.dataa(\pixel_counter[2]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[1]~12 ),
	.combout(\pixel_counter[2]~13_combout ),
	.cout(\pixel_counter[2]~14 ));
defparam \pixel_counter[2]~13 .lut_mask = 16'h5A5F;
defparam \pixel_counter[2]~13 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \pixel_counter[3]~16 (
	.dataa(\pixel_counter[3]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[2]~14 ),
	.combout(\pixel_counter[3]~16_combout ),
	.cout(\pixel_counter[3]~17 ));
defparam \pixel_counter[3]~16 .lut_mask = 16'hA50A;
defparam \pixel_counter[3]~16 .sum_lutc_input = "cin";

dffeas \pixel_counter[3] (
	.clk(clk),
	.d(\pixel_counter[3]~16_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[3]~q ),
	.prn(vcc));
defparam \pixel_counter[3] .is_wysiwyg = "true";
defparam \pixel_counter[3] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[4]~18 (
	.dataa(\pixel_counter[4]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[3]~17 ),
	.combout(\pixel_counter[4]~18_combout ),
	.cout(\pixel_counter[4]~19 ));
defparam \pixel_counter[4]~18 .lut_mask = 16'h5A5F;
defparam \pixel_counter[4]~18 .sum_lutc_input = "cin";

dffeas \pixel_counter[4] (
	.clk(clk),
	.d(\pixel_counter[4]~18_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[4]~q ),
	.prn(vcc));
defparam \pixel_counter[4] .is_wysiwyg = "true";
defparam \pixel_counter[4] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[5]~20 (
	.dataa(\pixel_counter[5]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[4]~19 ),
	.combout(\pixel_counter[5]~20_combout ),
	.cout(\pixel_counter[5]~21 ));
defparam \pixel_counter[5]~20 .lut_mask = 16'hA50A;
defparam \pixel_counter[5]~20 .sum_lutc_input = "cin";

dffeas \pixel_counter[5] (
	.clk(clk),
	.d(\pixel_counter[5]~20_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[5]~q ),
	.prn(vcc));
defparam \pixel_counter[5] .is_wysiwyg = "true";
defparam \pixel_counter[5] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[6]~22 (
	.dataa(\pixel_counter[6]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[5]~21 ),
	.combout(\pixel_counter[6]~22_combout ),
	.cout(\pixel_counter[6]~23 ));
defparam \pixel_counter[6]~22 .lut_mask = 16'h5A5F;
defparam \pixel_counter[6]~22 .sum_lutc_input = "cin";

dffeas \pixel_counter[6] (
	.clk(clk),
	.d(\pixel_counter[6]~22_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[6]~q ),
	.prn(vcc));
defparam \pixel_counter[6] .is_wysiwyg = "true";
defparam \pixel_counter[6] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[7]~24 (
	.dataa(\pixel_counter[7]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[6]~23 ),
	.combout(\pixel_counter[7]~24_combout ),
	.cout(\pixel_counter[7]~25 ));
defparam \pixel_counter[7]~24 .lut_mask = 16'hA50A;
defparam \pixel_counter[7]~24 .sum_lutc_input = "cin";

dffeas \pixel_counter[7] (
	.clk(clk),
	.d(\pixel_counter[7]~24_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[7]~q ),
	.prn(vcc));
defparam \pixel_counter[7] .is_wysiwyg = "true";
defparam \pixel_counter[7] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[8]~26 (
	.dataa(\pixel_counter[8]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[7]~25 ),
	.combout(\pixel_counter[8]~26_combout ),
	.cout(\pixel_counter[8]~27 ));
defparam \pixel_counter[8]~26 .lut_mask = 16'h5A5F;
defparam \pixel_counter[8]~26 .sum_lutc_input = "cin";

dffeas \pixel_counter[8] (
	.clk(clk),
	.d(\pixel_counter[8]~26_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[8]~q ),
	.prn(vcc));
defparam \pixel_counter[8] .is_wysiwyg = "true";
defparam \pixel_counter[8] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[9]~28 (
	.dataa(\pixel_counter[9]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[8]~27 ),
	.combout(\pixel_counter[9]~28_combout ),
	.cout(\pixel_counter[9]~29 ));
defparam \pixel_counter[9]~28 .lut_mask = 16'hA50A;
defparam \pixel_counter[9]~28 .sum_lutc_input = "cin";

dffeas \pixel_counter[9] (
	.clk(clk),
	.d(\pixel_counter[9]~28_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[9]~q ),
	.prn(vcc));
defparam \pixel_counter[9] .is_wysiwyg = "true";
defparam \pixel_counter[9] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[10]~30 (
	.dataa(\pixel_counter[10]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\pixel_counter[9]~29 ),
	.combout(\pixel_counter[10]~30_combout ),
	.cout(\pixel_counter[10]~31 ));
defparam \pixel_counter[10]~30 .lut_mask = 16'h5A5F;
defparam \pixel_counter[10]~30 .sum_lutc_input = "cin";

dffeas \pixel_counter[10] (
	.clk(clk),
	.d(\pixel_counter[10]~30_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[10]~q ),
	.prn(vcc));
defparam \pixel_counter[10] .is_wysiwyg = "true";
defparam \pixel_counter[10] .power_up = "low";

cyclone10lp_lcell_comb \pixel_counter[11]~32 (
	.dataa(\pixel_counter[11]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(\pixel_counter[10]~31 ),
	.combout(\pixel_counter[11]~32_combout ),
	.cout());
defparam \pixel_counter[11]~32 .lut_mask = 16'hA5A5;
defparam \pixel_counter[11]~32 .sum_lutc_input = "cin";

dffeas \pixel_counter[11] (
	.clk(clk),
	.d(\pixel_counter[11]~32_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[11]~q ),
	.prn(vcc));
defparam \pixel_counter[11] .is_wysiwyg = "true";
defparam \pixel_counter[11] .power_up = "low";

cyclone10lp_lcell_comb \Equal4~2 (
	.dataa(\pixel_counter[2]~q ),
	.datab(\pixel_counter[3]~q ),
	.datac(\pixel_counter[11]~q ),
	.datad(\pixel_counter[6]~q ),
	.cin(gnd),
	.combout(\Equal4~2_combout ),
	.cout());
defparam \Equal4~2 .lut_mask = 16'h0080;
defparam \Equal4~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal4~6 (
	.dataa(\pixel_counter[5]~q ),
	.datab(\pixel_counter[8]~q ),
	.datac(\pixel_counter[7]~q ),
	.datad(\Equal4~2_combout ),
	.cin(gnd),
	.combout(\Equal4~6_combout ),
	.cout());
defparam \Equal4~6 .lut_mask = 16'h0800;
defparam \Equal4~6 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal4~4 (
	.dataa(\pixel_counter[10]~q ),
	.datab(gnd),
	.datac(\pixel_counter[4]~q ),
	.datad(\pixel_counter[9]~q ),
	.cin(gnd),
	.combout(\Equal4~4_combout ),
	.cout());
defparam \Equal4~4 .lut_mask = 16'h000A;
defparam \Equal4~4 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \pixel_counter[5]~15 (
	.dataa(\pixel_counter[1]~q ),
	.datab(\Equal4~6_combout ),
	.datac(\Equal4~4_combout ),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\pixel_counter[5]~15_combout ),
	.cout());
defparam \pixel_counter[5]~15 .lut_mask = 16'hFF80;
defparam \pixel_counter[5]~15 .sum_lutc_input = "datac";

dffeas \pixel_counter[1] (
	.clk(clk),
	.d(\pixel_counter[1]~11_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[1]~q ),
	.prn(vcc));
defparam \pixel_counter[1] .is_wysiwyg = "true";
defparam \pixel_counter[1] .power_up = "low";

dffeas \pixel_counter[2] (
	.clk(clk),
	.d(\pixel_counter[2]~13_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\pixel_counter[5]~15_combout ),
	.ena(vcc),
	.q(\pixel_counter[2]~q ),
	.prn(vcc));
defparam \pixel_counter[2] .is_wysiwyg = "true";
defparam \pixel_counter[2] .power_up = "low";

cyclone10lp_lcell_comb \Equal2~0 (
	.dataa(\pixel_counter[6]~q ),
	.datab(\pixel_counter[4]~q ),
	.datac(\pixel_counter[1]~q ),
	.datad(\pixel_counter[10]~q ),
	.cin(gnd),
	.combout(\Equal2~0_combout ),
	.cout());
defparam \Equal2~0 .lut_mask = 16'h0008;
defparam \Equal2~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal2~1 (
	.dataa(\pixel_counter[2]~q ),
	.datab(\pixel_counter[3]~q ),
	.datac(\pixel_counter[11]~q ),
	.datad(\Equal2~0_combout ),
	.cin(gnd),
	.combout(\Equal2~1_combout ),
	.cout());
defparam \Equal2~1 .lut_mask = 16'h8000;
defparam \Equal2~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal2~2 (
	.dataa(\pixel_counter[7]~q ),
	.datab(\pixel_counter[5]~q ),
	.datac(\pixel_counter[8]~q ),
	.datad(\pixel_counter[9]~q ),
	.cin(gnd),
	.combout(\Equal2~2_combout ),
	.cout());
defparam \Equal2~2 .lut_mask = 16'h0080;
defparam \Equal2~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal4~3 (
	.dataa(\pixel_counter[5]~q ),
	.datab(\pixel_counter[8]~q ),
	.datac(gnd),
	.datad(\pixel_counter[7]~q ),
	.cin(gnd),
	.combout(\Equal4~3_combout ),
	.cout());
defparam \Equal4~3 .lut_mask = 16'h0088;
defparam \Equal4~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal4~5 (
	.dataa(\Equal4~2_combout ),
	.datab(\Equal4~3_combout ),
	.datac(\Equal4~4_combout ),
	.datad(\pixel_counter[1]~q ),
	.cin(gnd),
	.combout(\Equal4~5_combout ),
	.cout());
defparam \Equal4~5 .lut_mask = 16'h0080;
defparam \Equal4~5 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \hblanking_pulse~0 (
	.dataa(\Equal2~1_combout ),
	.datab(\Equal2~2_combout ),
	.datac(\hblanking_pulse~q ),
	.datad(\Equal4~5_combout ),
	.cin(gnd),
	.combout(\hblanking_pulse~0_combout ),
	.cout());
defparam \hblanking_pulse~0 .lut_mask = 16'h88F8;
defparam \hblanking_pulse~0 .sum_lutc_input = "datac";

dffeas hblanking_pulse(
	.clk(clk),
	.d(\hblanking_pulse~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(video_vga_controller_0_reset_reset),
	.ena(vcc),
	.q(\hblanking_pulse~q ),
	.prn(vcc));
defparam hblanking_pulse.is_wysiwyg = "true";
defparam hblanking_pulse.power_up = "low";

cyclone10lp_lcell_comb \line_counter[1]~11 (
	.dataa(\line_counter[1]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(gnd),
	.combout(\line_counter[1]~11_combout ),
	.cout(\line_counter[1]~12 ));
defparam \line_counter[1]~11 .lut_mask = 16'h55AA;
defparam \line_counter[1]~11 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \line_counter[2]~13 (
	.dataa(\line_counter[2]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[1]~12 ),
	.combout(\line_counter[2]~13_combout ),
	.cout(\line_counter[2]~14 ));
defparam \line_counter[2]~13 .lut_mask = 16'h5A5F;
defparam \line_counter[2]~13 .sum_lutc_input = "cin";

cyclone10lp_lcell_comb \line_counter[3]~16 (
	.dataa(\line_counter[3]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[2]~14 ),
	.combout(\line_counter[3]~16_combout ),
	.cout(\line_counter[3]~17 ));
defparam \line_counter[3]~16 .lut_mask = 16'hA50A;
defparam \line_counter[3]~16 .sum_lutc_input = "cin";

dffeas \line_counter[3] (
	.clk(clk),
	.d(\line_counter[3]~16_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[3]~q ),
	.prn(vcc));
defparam \line_counter[3] .is_wysiwyg = "true";
defparam \line_counter[3] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[4]~18 (
	.dataa(\line_counter[4]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[3]~17 ),
	.combout(\line_counter[4]~18_combout ),
	.cout(\line_counter[4]~19 ));
defparam \line_counter[4]~18 .lut_mask = 16'h5A5F;
defparam \line_counter[4]~18 .sum_lutc_input = "cin";

dffeas \line_counter[4] (
	.clk(clk),
	.d(\line_counter[4]~18_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[4]~q ),
	.prn(vcc));
defparam \line_counter[4] .is_wysiwyg = "true";
defparam \line_counter[4] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[5]~20 (
	.dataa(\line_counter[5]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[4]~19 ),
	.combout(\line_counter[5]~20_combout ),
	.cout(\line_counter[5]~21 ));
defparam \line_counter[5]~20 .lut_mask = 16'hA50A;
defparam \line_counter[5]~20 .sum_lutc_input = "cin";

dffeas \line_counter[5] (
	.clk(clk),
	.d(\line_counter[5]~20_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[5]~q ),
	.prn(vcc));
defparam \line_counter[5] .is_wysiwyg = "true";
defparam \line_counter[5] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[6]~22 (
	.dataa(\line_counter[6]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[5]~21 ),
	.combout(\line_counter[6]~22_combout ),
	.cout(\line_counter[6]~23 ));
defparam \line_counter[6]~22 .lut_mask = 16'h5A5F;
defparam \line_counter[6]~22 .sum_lutc_input = "cin";

dffeas \line_counter[6] (
	.clk(clk),
	.d(\line_counter[6]~22_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[6]~q ),
	.prn(vcc));
defparam \line_counter[6] .is_wysiwyg = "true";
defparam \line_counter[6] .power_up = "low";

cyclone10lp_lcell_comb \Equal1~0 (
	.dataa(\line_counter[1]~q ),
	.datab(\line_counter[4]~q ),
	.datac(\line_counter[6]~q ),
	.datad(gnd),
	.cin(gnd),
	.combout(\Equal1~0_combout ),
	.cout());
defparam \Equal1~0 .lut_mask = 16'h8080;
defparam \Equal1~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \line_counter[7]~24 (
	.dataa(\line_counter[7]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[6]~23 ),
	.combout(\line_counter[7]~24_combout ),
	.cout(\line_counter[7]~25 ));
defparam \line_counter[7]~24 .lut_mask = 16'hA50A;
defparam \line_counter[7]~24 .sum_lutc_input = "cin";

dffeas \line_counter[7] (
	.clk(clk),
	.d(\line_counter[7]~24_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[7]~q ),
	.prn(vcc));
defparam \line_counter[7] .is_wysiwyg = "true";
defparam \line_counter[7] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[8]~26 (
	.dataa(\line_counter[8]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[7]~25 ),
	.combout(\line_counter[8]~26_combout ),
	.cout(\line_counter[8]~27 ));
defparam \line_counter[8]~26 .lut_mask = 16'h5A5F;
defparam \line_counter[8]~26 .sum_lutc_input = "cin";

dffeas \line_counter[8] (
	.clk(clk),
	.d(\line_counter[8]~26_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[8]~q ),
	.prn(vcc));
defparam \line_counter[8] .is_wysiwyg = "true";
defparam \line_counter[8] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[9]~28 (
	.dataa(\line_counter[9]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[8]~27 ),
	.combout(\line_counter[9]~28_combout ),
	.cout(\line_counter[9]~29 ));
defparam \line_counter[9]~28 .lut_mask = 16'hA50A;
defparam \line_counter[9]~28 .sum_lutc_input = "cin";

dffeas \line_counter[9] (
	.clk(clk),
	.d(\line_counter[9]~28_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[9]~q ),
	.prn(vcc));
defparam \line_counter[9] .is_wysiwyg = "true";
defparam \line_counter[9] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[10]~30 (
	.dataa(\line_counter[10]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(vcc),
	.cin(\line_counter[9]~29 ),
	.combout(\line_counter[10]~30_combout ),
	.cout(\line_counter[10]~31 ));
defparam \line_counter[10]~30 .lut_mask = 16'h5A5F;
defparam \line_counter[10]~30 .sum_lutc_input = "cin";

dffeas \line_counter[10] (
	.clk(clk),
	.d(\line_counter[10]~30_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[10]~q ),
	.prn(vcc));
defparam \line_counter[10] .is_wysiwyg = "true";
defparam \line_counter[10] .power_up = "low";

cyclone10lp_lcell_comb \line_counter[11]~32 (
	.dataa(\line_counter[11]~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(gnd),
	.cin(\line_counter[10]~31 ),
	.combout(\line_counter[11]~32_combout ),
	.cout());
defparam \line_counter[11]~32 .lut_mask = 16'hA5A5;
defparam \line_counter[11]~32 .sum_lutc_input = "cin";

dffeas \line_counter[11] (
	.clk(clk),
	.d(\line_counter[11]~32_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[11]~q ),
	.prn(vcc));
defparam \line_counter[11] .is_wysiwyg = "true";
defparam \line_counter[11] .power_up = "low";

cyclone10lp_lcell_comb \always3~0 (
	.dataa(\line_counter[2]~q ),
	.datab(\line_counter[5]~q ),
	.datac(\line_counter[7]~q ),
	.datad(\line_counter[8]~q ),
	.cin(gnd),
	.combout(\always3~0_combout ),
	.cout());
defparam \always3~0 .lut_mask = 16'h0001;
defparam \always3~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \always3~1 (
	.dataa(\line_counter[11]~q ),
	.datab(\always3~0_combout ),
	.datac(\line_counter[9]~q ),
	.datad(\line_counter[10]~q ),
	.cin(gnd),
	.combout(\always3~1_combout ),
	.cout());
defparam \always3~1 .lut_mask = 16'h0008;
defparam \always3~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \line_counter[7]~15 (
	.dataa(\line_counter[3]~q ),
	.datab(\Equal1~0_combout ),
	.datac(\always3~1_combout ),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\line_counter[7]~15_combout ),
	.cout());
defparam \line_counter[7]~15 .lut_mask = 16'hFF40;
defparam \line_counter[7]~15 .sum_lutc_input = "datac";

dffeas \line_counter[1] (
	.clk(clk),
	.d(\line_counter[1]~11_combout ),
	.asdata(video_vga_controller_0_reset_reset),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[1]~q ),
	.prn(vcc));
defparam \line_counter[1] .is_wysiwyg = "true";
defparam \line_counter[1] .power_up = "low";

dffeas \line_counter[2] (
	.clk(clk),
	.d(\line_counter[2]~13_combout ),
	.asdata(GND_port),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(\line_counter[7]~15_combout ),
	.ena(\pixel_counter[5]~15_combout ),
	.q(\line_counter[2]~q ),
	.prn(vcc));
defparam \line_counter[2] .is_wysiwyg = "true";
defparam \line_counter[2] .power_up = "low";

cyclone10lp_lcell_comb \Equal1~1 (
	.dataa(\line_counter[3]~q ),
	.datab(\line_counter[5]~q ),
	.datac(\line_counter[7]~q ),
	.datad(\line_counter[8]~q ),
	.cin(gnd),
	.combout(\Equal1~1_combout ),
	.cout());
defparam \Equal1~1 .lut_mask = 16'h8000;
defparam \Equal1~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal1~2 (
	.dataa(\line_counter[9]~q ),
	.datab(\line_counter[10]~q ),
	.datac(gnd),
	.datad(\line_counter[11]~q ),
	.cin(gnd),
	.combout(\Equal1~2_combout ),
	.cout());
defparam \Equal1~2 .lut_mask = 16'h0088;
defparam \Equal1~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal1~3 (
	.dataa(\line_counter[2]~q ),
	.datab(\Equal1~0_combout ),
	.datac(\Equal1~1_combout ),
	.datad(\Equal1~2_combout ),
	.cin(gnd),
	.combout(\Equal1~3_combout ),
	.cout());
defparam \Equal1~3 .lut_mask = 16'h8000;
defparam \Equal1~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vblanking_pulse~0 (
	.dataa(\line_counter[3]~q ),
	.datab(\Equal4~5_combout ),
	.datac(\Equal1~0_combout ),
	.datad(\always3~1_combout ),
	.cin(gnd),
	.combout(\vblanking_pulse~0_combout ),
	.cout());
defparam \vblanking_pulse~0 .lut_mask = 16'hBFFF;
defparam \vblanking_pulse~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vblanking_pulse~1 (
	.dataa(\vblanking_pulse~q ),
	.datab(\Equal4~5_combout ),
	.datac(\Equal1~3_combout ),
	.datad(\vblanking_pulse~0_combout ),
	.cin(gnd),
	.combout(\vblanking_pulse~1_combout ),
	.cout());
defparam \vblanking_pulse~1 .lut_mask = 16'hEAC0;
defparam \vblanking_pulse~1 .sum_lutc_input = "datac";

dffeas vblanking_pulse(
	.clk(clk),
	.d(\vblanking_pulse~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(video_vga_controller_0_reset_reset),
	.ena(vcc),
	.q(\vblanking_pulse~q ),
	.prn(vcc));
defparam vblanking_pulse.is_wysiwyg = "true";
defparam vblanking_pulse.power_up = "low";

cyclone10lp_lcell_comb \blanking_pulse~0 (
	.dataa(\hblanking_pulse~q ),
	.datab(\vblanking_pulse~q ),
	.datac(gnd),
	.datad(gnd),
	.cin(gnd),
	.combout(\blanking_pulse~0_combout ),
	.cout());
defparam \blanking_pulse~0 .lut_mask = 16'hEEEE;
defparam \blanking_pulse~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \always2~0 (
	.dataa(\Equal2~1_combout ),
	.datab(\Equal2~2_combout ),
	.datac(\Equal1~3_combout ),
	.datad(gnd),
	.cin(gnd),
	.combout(\always2~0_combout ),
	.cout());
defparam \always2~0 .lut_mask = 16'h8080;
defparam \always2~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal6~0 (
	.dataa(\pixel_counter[4]~q ),
	.datab(\pixel_counter[9]~q ),
	.datac(\pixel_counter[1]~q ),
	.datad(\pixel_counter[10]~q ),
	.cin(gnd),
	.combout(\Equal6~0_combout ),
	.cout());
defparam \Equal6~0 .lut_mask = 16'h0008;
defparam \Equal6~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \early_hsync_pulse~0 (
	.dataa(\early_hsync_pulse~q ),
	.datab(\Equal4~2_combout ),
	.datac(\Equal4~3_combout ),
	.datad(\Equal6~0_combout ),
	.cin(gnd),
	.combout(\early_hsync_pulse~0_combout ),
	.cout());
defparam \early_hsync_pulse~0 .lut_mask = 16'h2AAA;
defparam \early_hsync_pulse~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \Equal5~0 (
	.dataa(\pixel_counter[9]~q ),
	.datab(\pixel_counter[7]~q ),
	.datac(\pixel_counter[5]~q ),
	.datad(\pixel_counter[8]~q ),
	.cin(gnd),
	.combout(\Equal5~0_combout ),
	.cout());
defparam \Equal5~0 .lut_mask = 16'h0002;
defparam \Equal5~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \early_hsync_pulse~1 (
	.dataa(\early_hsync_pulse~0_combout ),
	.datab(\Equal2~1_combout ),
	.datac(\Equal5~0_combout ),
	.datad(gnd),
	.cin(gnd),
	.combout(\early_hsync_pulse~1_combout ),
	.cout());
defparam \early_hsync_pulse~1 .lut_mask = 16'hEAEA;
defparam \early_hsync_pulse~1 .sum_lutc_input = "datac";

dffeas early_hsync_pulse(
	.clk(clk),
	.d(\early_hsync_pulse~1_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(video_vga_controller_0_reset_reset),
	.sload(gnd),
	.ena(vcc),
	.q(\early_hsync_pulse~q ),
	.prn(vcc));
defparam early_hsync_pulse.is_wysiwyg = "true";
defparam early_hsync_pulse.power_up = "low";

cyclone10lp_lcell_comb \hsync_pulse~0 (
	.dataa(\early_hsync_pulse~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\hsync_pulse~0_combout ),
	.cout());
defparam \hsync_pulse~0 .lut_mask = 16'h00AA;
defparam \hsync_pulse~0 .sum_lutc_input = "datac";

dffeas hsync_pulse(
	.clk(clk),
	.d(\hsync_pulse~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\hsync_pulse~q ),
	.prn(vcc));
defparam hsync_pulse.is_wysiwyg = "true";
defparam hsync_pulse.power_up = "low";

cyclone10lp_lcell_comb \vga_h_sync~0 (
	.dataa(video_vga_controller_0_reset_reset),
	.datab(gnd),
	.datac(gnd),
	.datad(\hsync_pulse~q ),
	.cin(gnd),
	.combout(\vga_h_sync~0_combout ),
	.cout());
defparam \vga_h_sync~0 .lut_mask = 16'hAAFF;
defparam \vga_h_sync~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \always3~2 (
	.dataa(\Equal4~5_combout ),
	.datab(\always3~1_combout ),
	.datac(\line_counter[4]~q ),
	.datad(\line_counter[6]~q ),
	.cin(gnd),
	.combout(\always3~2_combout ),
	.cout());
defparam \always3~2 .lut_mask = 16'h0008;
defparam \always3~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \early_vsync_pulse~0 (
	.dataa(\early_vsync_pulse~q ),
	.datab(\line_counter[1]~q ),
	.datac(\always3~2_combout ),
	.datad(\line_counter[3]~q ),
	.cin(gnd),
	.combout(\early_vsync_pulse~0_combout ),
	.cout());
defparam \early_vsync_pulse~0 .lut_mask = 16'h8AEA;
defparam \early_vsync_pulse~0 .sum_lutc_input = "datac";

dffeas early_vsync_pulse(
	.clk(clk),
	.d(\early_vsync_pulse~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(video_vga_controller_0_reset_reset),
	.sload(gnd),
	.ena(vcc),
	.q(\early_vsync_pulse~q ),
	.prn(vcc));
defparam early_vsync_pulse.is_wysiwyg = "true";
defparam early_vsync_pulse.power_up = "low";

cyclone10lp_lcell_comb \vsync_pulse~0 (
	.dataa(\early_vsync_pulse~q ),
	.datab(gnd),
	.datac(gnd),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vsync_pulse~0_combout ),
	.cout());
defparam \vsync_pulse~0 .lut_mask = 16'h00AA;
defparam \vsync_pulse~0 .sum_lutc_input = "datac";

dffeas vsync_pulse(
	.clk(clk),
	.d(\vsync_pulse~0_combout ),
	.asdata(vcc),
	.clrn(vcc),
	.aload(gnd),
	.sclr(gnd),
	.sload(gnd),
	.ena(vcc),
	.q(\vsync_pulse~q ),
	.prn(vcc));
defparam vsync_pulse.is_wysiwyg = "true";
defparam vsync_pulse.power_up = "low";

cyclone10lp_lcell_comb \vga_v_sync~0 (
	.dataa(video_vga_controller_0_reset_reset),
	.datab(gnd),
	.datac(gnd),
	.datad(\vsync_pulse~q ),
	.cin(gnd),
	.combout(\vga_v_sync~0_combout ),
	.cout());
defparam \vga_v_sync~0 .lut_mask = 16'hAAFF;
defparam \vga_v_sync~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_blank~0 (
	.dataa(video_vga_controller_0_reset_reset),
	.datab(gnd),
	.datac(gnd),
	.datad(blanking_pulse1),
	.cin(gnd),
	.combout(\vga_blank~0_combout ),
	.cout());
defparam \vga_blank~0 .lut_mask = 16'hAAFF;
defparam \vga_blank~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_red~0 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_26),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_red~0_combout ),
	.cout());
defparam \vga_red~0 .lut_mask = 16'h000A;
defparam \vga_red~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_red~1 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_27),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_red~1_combout ),
	.cout());
defparam \vga_red~1 .lut_mask = 16'h000A;
defparam \vga_red~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_red~2 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_28),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_red~2_combout ),
	.cout());
defparam \vga_red~2 .lut_mask = 16'h000A;
defparam \vga_red~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_red~3 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_29),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_red~3_combout ),
	.cout());
defparam \vga_red~3 .lut_mask = 16'h000A;
defparam \vga_red~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_green~0 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_16),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_green~0_combout ),
	.cout());
defparam \vga_green~0 .lut_mask = 16'h000A;
defparam \vga_green~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_green~1 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_17),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_green~1_combout ),
	.cout());
defparam \vga_green~1 .lut_mask = 16'h000A;
defparam \vga_green~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_green~2 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_18),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_green~2_combout ),
	.cout());
defparam \vga_green~2 .lut_mask = 16'h000A;
defparam \vga_green~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_green~3 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_19),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_green~3_combout ),
	.cout());
defparam \vga_green~3 .lut_mask = 16'h000A;
defparam \vga_green~3 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_blue~0 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_6),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_blue~0_combout ),
	.cout());
defparam \vga_blue~0 .lut_mask = 16'h000A;
defparam \vga_blue~0 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_blue~1 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_7),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_blue~1_combout ),
	.cout());
defparam \vga_blue~1 .lut_mask = 16'h000A;
defparam \vga_blue~1 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_blue~2 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_8),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_blue~2_combout ),
	.cout());
defparam \vga_blue~2 .lut_mask = 16'h000A;
defparam \vga_blue~2 .sum_lutc_input = "datac";

cyclone10lp_lcell_comb \vga_blue~3 (
	.dataa(video_vga_controller_0_avalon_vga_sink_data_9),
	.datab(gnd),
	.datac(blanking_pulse1),
	.datad(video_vga_controller_0_reset_reset),
	.cin(gnd),
	.combout(\vga_blue~3_combout ),
	.cout());
defparam \vga_blue~3 .lut_mask = 16'h000A;
defparam \vga_blue~3 .sum_lutc_input = "datac";

endmodule
