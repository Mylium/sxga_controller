library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
port (
    -- VGA
    clk12      : in std_logic;
    R          : out std_logic_vector(2 downto 0);
    G          : out std_logic_vector(2 downto 0);
    B          : out std_logic_vector(2 downto 0);
    HS         : out std_logic;
    VS         : out std_logic;
    -- SDRAM
    sdram_addr : out   std_logic_vector(11 downto 0);                   
    sdram_ba   : out   std_logic_vector(1 downto 0);                    
    sdram_cas_n: out   std_logic;                                       
    sdram_cke  : out   std_logic;                                       
    sdram_cs_n : out   std_logic;                                       
    sdram_dq   : inout std_logic_vector(15 downto 0) := (others => '0');
    sdram_dqm  : out   std_logic_vector(1 downto 0);                    
    sdram_ras_n: out   std_logic;                                       
    sdram_we_n : out   std_logic;
    sdram_clk  : out   std_logic 
    -- SPI
    --SCLK     : in  std_logic; -- SPI clock
    --CS_N     : in  std_logic; -- SPI chip select, active in low
    --MOSI     : in  std_logic; -- SPI serial data from master to slave
    --MISO     : out std_logic -- SPI serial data from slave to master                                      
  );
end top;

architecture top_arc of top is
  
  type StateType is (IDLE, FILL_MEMORY,WRITE_OP,READ_MEMORY,READ_OP,READ_PAUSE);

  signal state             : StateType := IDLE;
  signal clk108            : std_logic;
  signal locked            : std_logic;
  signal data_red          : std_logic_vector(9 downto 0);
  signal data_green        : std_logic_vector(9 downto 0);
  signal data_blue         : std_logic_vector(9 downto 0);
  signal R_int             : std_logic_vector(3 downto 0);
  signal G_int             : std_logic_vector(3 downto 0);
  signal B_int             : std_logic_vector(3 downto 0);
  signal load              : std_logic;
  signal reset             : std_logic;
  signal color_cnt         : unsigned(11 downto 0) := (others => '0');
  signal total_cnt         : unsigned(23 downto 0) := (others => '0');
  signal sink_sop          : std_logic := '0';
  signal sink_eop          : std_logic := '0';
  signal sink_valid        : std_logic := '0';
  signal sink_ready        : std_logic;
  signal sink_data         : std_logic_vector(29 downto 0);
  signal ctrl_address      : std_logic_vector(21 downto 0) := (others => '0');
  signal ctrl_byteenable_n : std_logic_vector(1 downto 0)  := (others => '0');
  signal ctrl_chipselect   : std_logic                     := '0';            
  signal ctrl_writedata    : std_logic_vector(15 downto 0) := (others => '0');
  signal ctrl_read_n       : std_logic                     := '0';            
  signal ctrl_write_n      : std_logic                     := '0';            
  signal ctrl_readdata     : std_logic_vector(15 downto 0);                   
  signal ctrl_readdatavalid: std_logic;                                       
  signal ctrl_waitrequest  : std_logic;   
  signal data_vga_in       : std_logic_vector(15 downto 0);                                    
  signal din               : std_logic_vector(7 downto 0);
  signal din_valid         : std_logic;
  signal dout              : std_logic_vector(7 downto 0);
  signal dout_valid        : std_logic; 
  signal spi_ready         : std_logic;
  signal data_count        : unsigned(19 downto 0) := (others => '0');
  signal memory_filled_flag: std_logic := '0';
  signal vga_reset         : std_logic := '1';
  signal fifo_wr,fifo_rd   : std_logic := '0';
  signal fifo_empty        : std_logic;
  signal fifo_full         : std_logic;
  signal data_vga          : std_logic_vector(7 downto 0);
  signal HS_int,HS_prev    : std_logic := '0';
  signal VS_int,VS_prev    : std_logic := '0';
  signal line_cnt          : unsigned(10 downto 0) := (others => '0');

  component pll
  port(
    inclk0: in std_logic  := '0';
    c0    : out std_logic := '0';
    locked: out std_logic := '0'
  );
  end component;

  component vga_controller
  port (
    video_vga_controller_0_avalon_vga_sink_data          : in std_logic_vector (29 downto 0) := (others => '0');
    video_vga_controller_0_avalon_vga_sink_startofpacket : in std_logic;
    video_vga_controller_0_avalon_vga_sink_endofpacket   : in std_logic;
    video_vga_controller_0_avalon_vga_sink_valid         : in std_logic;
    video_vga_controller_0_avalon_vga_sink_ready         : out std_logic;
    video_vga_controller_0_clk_clk                       : in std_logic;
    video_vga_controller_0_external_interface_CLK        : out std_logic;
    video_vga_controller_0_external_interface_HS         : out std_logic;
    video_vga_controller_0_external_interface_VS         : out std_logic;
    video_vga_controller_0_external_interface_BLANK      : out std_logic;
    video_vga_controller_0_external_interface_SYNC       : out std_logic;
    video_vga_controller_0_external_interface_R          : out std_logic_vector (3 downto 0);
    video_vga_controller_0_external_interface_G          : out std_logic_vector (3 downto 0);
    video_vga_controller_0_external_interface_B          : out std_logic_vector (3 downto 0);
    video_vga_controller_0_reset_reset                   : in std_logic
    );
  end component;

  component sdram_controller
	port (
		clk_clk                                 : in    std_logic                     := '0';             --                         clk.clk
		new_sdram_controller_0_s1_address       : in    std_logic_vector(21 downto 0) := (others => '0'); --   new_sdram_controller_0_s1.address
		new_sdram_controller_0_s1_byteenable_n  : in    std_logic_vector(1 downto 0)  := (others => '0'); --                            .byteenable_n
		new_sdram_controller_0_s1_chipselect    : in    std_logic                     := '0';             --                            .chipselect
		new_sdram_controller_0_s1_writedata     : in    std_logic_vector(15 downto 0) := (others => '0'); --                            .writedata
		new_sdram_controller_0_s1_read_n        : in    std_logic                     := '0';             --                            .read_n
		new_sdram_controller_0_s1_write_n       : in    std_logic                     := '0';             --                            .write_n
		new_sdram_controller_0_s1_readdata      : out   std_logic_vector(15 downto 0);                    --                            .readdata
		new_sdram_controller_0_s1_readdatavalid : out   std_logic;                                        --                            .readdatavalid
		new_sdram_controller_0_s1_waitrequest   : out   std_logic;                                        --                            .waitrequest
		new_sdram_controller_0_wire_addr        : out   std_logic_vector(11 downto 0);                    -- new_sdram_controller_0_wire.addr
		new_sdram_controller_0_wire_ba          : out   std_logic_vector(1 downto 0);                     --                            .ba
		new_sdram_controller_0_wire_cas_n       : out   std_logic;                                        --                            .cas_n
		new_sdram_controller_0_wire_cke         : out   std_logic;                                        --                            .cke
		new_sdram_controller_0_wire_cs_n        : out   std_logic;                                        --                            .cs_n
		new_sdram_controller_0_wire_dq          : inout std_logic_vector(15 downto 0) := (others => '0'); --                            .dq
		new_sdram_controller_0_wire_dqm         : out   std_logic_vector(1 downto 0);                     --                            .dqm
		new_sdram_controller_0_wire_ras_n       : out   std_logic;                                        --                            .ras_n
		new_sdram_controller_0_wire_we_n        : out   std_logic;                                        --                            .we_n
		reset_reset_n                           : in    std_logic                     := '0'              --                       reset.reset_n
	);
	end component;
	
  component spi
  port (
      CLK      : in  std_logic; -- system clock
      RST      : in  std_logic; -- high active synchronous reset
      -- SPI SLAVE INTERFACE
      SCLK     : in  std_logic; -- SPI clock
      CS_N     : in  std_logic; -- SPI chip select, active in low
      MOSI     : in  std_logic; -- SPI serial data from master to slave
      MISO     : out std_logic; -- SPI serial data from slave to master
      -- USER INTERFACE
      DIN      : in  std_logic_vector(7 downto 0); -- input data for SPI master
      DIN_VLD  : in  std_logic; -- when DIN_VLD = 1, input data are valid
      READY    : out std_logic; -- when READY = 1, valid input data are accept
      DOUT     : out std_logic_vector(7 downto 0); -- output data from SPI master
      DOUT_VLD : out std_logic  -- when DOUT_VLD = 1, output data are valid
  );
  end component;

  component fifo
    port (
      data   : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
      rdclk  : IN STD_LOGIC ;
      rdreq  : IN STD_LOGIC ;
      wrclk  : IN STD_LOGIC ;
      wrreq  : IN STD_LOGIC ;
      q      : OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
      rdempty: OUT STD_LOGIC ;
      wrfull : OUT STD_LOGIC 
    );
  end component;

begin

  sdram_clk <= clk108;
  reset <= not locked;
  R <= R_int(3 downto 1);
  G <= G_int(3 downto 1);
  B <= B_int(3 downto 1);
  HS <= HS_int;
  VS <= VS_int;

  i_pll : entity work.pll
  port map (
    inclk0 => clk12,
    c0     => clk108,
    locked => locked
  );
  
  dut : vga_controller
  port map (
    video_vga_controller_0_avalon_vga_sink_data          => sink_data,
    video_vga_controller_0_avalon_vga_sink_startofpacket => sink_sop,
    video_vga_controller_0_avalon_vga_sink_endofpacket   => sink_eop,
    video_vga_controller_0_avalon_vga_sink_valid         => sink_valid,
    video_vga_controller_0_avalon_vga_sink_ready         => sink_ready,
    video_vga_controller_0_clk_clk                       => clk108,
    video_vga_controller_0_external_interface_CLK        => open,
    video_vga_controller_0_external_interface_HS         => HS_int,
    video_vga_controller_0_external_interface_VS         => VS_int,
    video_vga_controller_0_external_interface_BLANK      => open,
    video_vga_controller_0_external_interface_SYNC       => open,
    video_vga_controller_0_external_interface_R          => R_int,
    video_vga_controller_0_external_interface_G          => G_int,
    video_vga_controller_0_external_interface_B          => B_int,
    video_vga_controller_0_reset_reset                   => vga_reset
    );

  sdram : sdram_controller
  port map(
	 	clk_clk                                 => clk108,                             
    new_sdram_controller_0_s1_address       => ctrl_address      , 
    new_sdram_controller_0_s1_byteenable_n  => ctrl_byteenable_n , 
    new_sdram_controller_0_s1_chipselect    => ctrl_chipselect   , 
    new_sdram_controller_0_s1_writedata     => ctrl_writedata    , 
    new_sdram_controller_0_s1_read_n        => ctrl_read_n       , 
    new_sdram_controller_0_s1_write_n       => ctrl_write_n      , 
    new_sdram_controller_0_s1_readdata      => ctrl_readdata     , 
    new_sdram_controller_0_s1_readdatavalid => ctrl_readdatavalid, 
    new_sdram_controller_0_s1_waitrequest   => ctrl_waitrequest  , 
    new_sdram_controller_0_wire_addr        => sdram_addr   ,
    new_sdram_controller_0_wire_ba          => sdram_ba     ,
    new_sdram_controller_0_wire_cas_n       => sdram_cas_n  ,
    new_sdram_controller_0_wire_cke         => sdram_cke    ,
    new_sdram_controller_0_wire_cs_n        => sdram_cs_n   ,
    new_sdram_controller_0_wire_dq          => sdram_dq     ,
    new_sdram_controller_0_wire_dqm         => sdram_dqm    ,
    new_sdram_controller_0_wire_ras_n       => sdram_ras_n  ,
    new_sdram_controller_0_wire_we_n        => sdram_we_n   ,
    reset_reset_n                           => "not"(reset) 
	 );

	--spi_slave : spi
 -- port map
 -- (
 --   CLK      => clk108,
 --   RST      => reset,
 --   SCLK     => SCLK,
 --   CS_N     => CS_N,
 --   MOSI     => MOSI,
 --   MISO     => MISO,
 --   DIN      => din,
 --   DIN_VLD  => din_valid,
 --   READY    => spi_ready,
 --   DOUT     => dout,
 --   DOUT_VLD => dout_valid
 -- );

  sdram_fifo : fifo
  port map(
    data    => data_vga_in,
    rdclk   => clk108,
    rdreq   => fifo_rd,
    wrclk   => clk108,
    wrreq   => fifo_wr,
    q       => data_vga,
    rdempty => fifo_empty,
    wrfull  => fifo_full
    );

  SM : process (clk108)
  begin
    if (rising_edge(clk108)) then
      if reset = '1' then
        sink_data  <= (others => '0');
        sink_sop   <= '0';
        sink_eop   <= '0';
        sink_valid <= '0';
        vga_reset  <= '1';
      else
        HS_prev <= HS_int;
        VS_prev <= VS_int;
        data_vga_in <= ctrl_readdata;
        ctrl_chipselect <= '1';
        ctrl_write_n <= '1';
        ctrl_read_n <= '1';
        fifo_wr <= '0';
        fifo_rd <= '0';
        if VS_prev = '1' and VS_int = '0' then
          line_cnt <= (others => '0');
        elsif HS_prev = '1' and HS_int = '0' then  
          line_cnt <= line_cnt + 1;
        end if;  
        case(state) is
          when IDLE => 
            if memory_filled_flag = '0' and line_cnt = 0 then
              state <= FILL_MEMORY;
            else
              state <= READ_MEMORY;  
              vga_reset <= '0';
            end if;  
          when FILL_MEMORY =>
            if data_count = 1280*512 then
              memory_filled_flag <= '1';
              data_count <= (others => '0');
              state <= IDLE;
            else
              state <= WRITE_OP;
              ctrl_address <= b"00" & std_logic_vector(data_count);
              ctrl_writedata <= std_logic_vector(data_count(19 downto 12)) &std_logic_vector(data_count(19 downto 12));
              ctrl_write_n <= '0';
            end if; 
          when WRITE_OP =>
            if ctrl_waitrequest = '0' then
              data_count <= data_count + 1;
              ctrl_write_n <= '0';
              state <= FILL_MEMORY; 
            else
              ctrl_write_n <= '0';
            end if;  
          when READ_MEMORY =>
            ctrl_address <= std_logic_vector(line_cnt*512+data_count);
            ctrl_read_n <= '0';
            if fifo_full = '0' then
              state <= READ_OP;
            end if; 
          when READ_PAUSE =>
            if HS_prev = '1' and HS_int = '0' then 
              state <= READ_MEMORY;  
            end if;     
          when others =>
            ctrl_read_n <= '0';
            if ctrl_readdatavalid = '1' then 
              if data_count = 511 then
                data_count <= (others => '0');
                state <= READ_PAUSE;
              else  
                ctrl_address <= std_logic_vector(line_cnt*512+data_count);
                data_count <= (data_count + 1);
  				      state <= READ_OP; 
                fifo_wr <= '1';
              end if;  
            else
              state <= READ_OP;
            end if;    
        end case; 
        -- VGA
        if memory_filled_flag = '1' then
          sink_sop <= '0';
          sink_eop <= '0';
          if fifo_empty = '0' then 
            sink_valid <= '1';
            if sink_ready = '1' then
              color_cnt <= color_cnt + 1;  
              if total_cnt = x"13FFFF" then
                total_cnt <= (others => '0');
                sink_eop <= '1';
              else  
                total_cnt <= total_cnt + 1;
                if total_cnt = x"000000" then
                  sink_sop <= '1';
                end if;  
              end if;  
              if data_vga(1 downto 0) = b"11" then
                sink_data <= std_logic_vector(data_vga(7 downto 5)) & b"000_0000" &
                 std_logic_vector(data_vga(4 downto 2)) & b"000_0000" &
                 std_logic_vector(data_vga(1 downto 0)) & b"1000_0000";   
              else
                sink_data <= std_logic_vector(data_vga(7 downto 5)) & b"000_0000" &
                 std_logic_vector(data_vga(4 downto 2)) & b"000_0000" &
                 std_logic_vector(data_vga(1 downto 0)) & b"0000_0000";   
              end if; 
              fifo_rd <= '1';  
            end if;  
          end if;    
        end if;         
      end if;  
    end if;
  end process SM;

end top_arc;