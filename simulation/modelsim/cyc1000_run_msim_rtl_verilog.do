transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation {/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/vga_controller.v}
vlog -vlog01compat -work work +incdir+/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/submodules {/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/submodules/altera_up_avalon_video_vga_timing.v}
vlog -vlog01compat -work work +incdir+/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/submodules {/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/submodules/vga_controller_video_vga_controller_0.v}

vcom -93 -work work {/home/dominik/intelFPGA_projects/cyc1000/vga_controller/simulation/tb_vga_controller.vhd}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclone10lp_ver -L rtl_work -L work -voptargs="+acc"  vga_controller

add wave *
view structure
view signals
run -all
