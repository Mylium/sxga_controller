library ieee;
use ieee.std_logic_1164.all;

entity tb_top is
end tb_top;

architecture tb of tb_top is

  component top
  port (
    clk12       : in std_logic;
    R           : out std_logic_vector (2 downto 0);
    G           : out std_logic_vector (2 downto 0);
    B           : out std_logic_vector (2 downto 0);
    HS          : out std_logic;
    VS          : out std_logic;
    sdram_addr  : out std_logic_vector (11 downto 0);
    sdram_ba    : out std_logic_vector (1 downto 0);
    sdram_cas_n : out std_logic;
    sdram_cke   : out std_logic;
    sdram_cs_n  : out std_logic;
    sdram_dq    : inout std_logic_vector (15 downto 0) := (others => '0');
    sdram_dqm   : out std_logic_vector (1 downto 0);
    sdram_ras_n : out std_logic;
    sdram_we_n  : out std_logic;
    SCLK        : in std_logic;
    CS_N        : in std_logic;
    MOSI        : in std_logic;
    MISO        : out std_logic);
  end component;

  component sdram_controller_new_sdram_controller_0_test_component
  port (
    clk      : in std_logic;
    zs_addr  : in std_logic_vector (11 downto 0);
    zs_ba    : in std_logic_vector (1 downto 0);
    zs_cas_n : in std_logic;
    zs_cke   : in std_logic;
    zs_cs_n  : in std_logic;
    zs_dq    : inout std_logic_vector (15 downto 0);
    zs_dqm   : in std_logic_vector (1 downto 0);
    zs_ras_n : in std_logic;
    zs_we_n  : in std_logic
  );
  end component; 

    signal clk12,clk108: std_logic;
    signal R           : std_logic_vector (2 downto 0);
    signal G           : std_logic_vector (2 downto 0);
    signal B           : std_logic_vector (2 downto 0);
    signal HS          : std_logic;
    signal VS          : std_logic;
    signal sdram_addr  : std_logic_vector (11 downto 0);
    signal sdram_ba    : std_logic_vector (1 downto 0);
    signal sdram_cas_n : std_logic;
    signal sdram_cke   : std_logic;
    signal sdram_cs_n  : std_logic;
    signal sdram_dq    : std_logic_vector (15 downto 0) := (others => '0');
    signal sdram_dqm   : std_logic_vector (1 downto 0);
    signal sdram_ras_n : std_logic;
    signal sdram_we_n  : std_logic;
    signal SCLK        : std_logic;
    signal CS_N        : std_logic;
    signal MOSI        : std_logic;
    signal MISO        : std_logic;

    constant TbPeriod : time := 83.333 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    constant TbPeriod108 : time := 9.26 ns; -- EDIT Put right period here
    signal TbClock108 : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : top
    port map (clk12       => clk12,
              R           => R,
              G           => G,
              B           => B,
              HS          => HS,
              VS          => VS,
              sdram_addr  => sdram_addr,
              sdram_ba    => sdram_ba,
              sdram_cas_n => sdram_cas_n,
              sdram_cke   => sdram_cke,
              sdram_cs_n  => sdram_cs_n,
              sdram_dq    => sdram_dq,
              sdram_dqm   => sdram_dqm,
              sdram_ras_n => sdram_ras_n,
              sdram_we_n  => sdram_we_n,
              SCLK        => SCLK,
              CS_N        => CS_N,
              MOSI        => MOSI,
              MISO        => MISO);

    memory_model: sdram_controller_new_sdram_controller_0_test_component
    port map (
      clk      => clk108,
      zs_addr  => sdram_addr,
      zs_ba    => sdram_ba,
      zs_cas_n => sdram_cas_n,
      zs_cke   => sdram_cke,
      zs_cs_n  => sdram_cs_n,
      zs_dq    => sdram_dq,
      zs_dqm   => sdram_dqm,
      zs_ras_n => sdram_ras_n,
      zs_we_n  => sdram_we_n
      );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    TbClock108 <= not TbClock108 after TbPeriod108/2 when TbSimEnded /= '1' else '0';
    -- EDIT: Check that clk12 is really your main clock signal
    clk12 <= TbClock;
    clk108 <= TbClock108;
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        SCLK <= '0';
        CS_N <= '0';
        MOSI <= '0';

        -- EDIT Add stimuli here
        wait;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_top of tb_top is
    for tb
    end for;
end cfg_tb_top;
